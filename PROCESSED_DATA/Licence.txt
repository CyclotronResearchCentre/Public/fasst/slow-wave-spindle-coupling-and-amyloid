Licence for all data and scripts shared within the following gitlab repository: 
https://gitlab.uliege.be/CyclotronResearchCentre/Public/fasst/slow-wave-spindle-coupling-and-amyloid

Licence type: CC BY-NC 4.0

In brief this means: 
- You are free to:
    * Share — copy and redistribute the material in any medium or format
    * Adapt — remix, transform, and build upon the material
- Under the following terms:
    * Attribution — You must give appropriate credit, provide a link to the license, and indicate if changes were made. 
	You may do so in any reasonable manner, but not in any way that suggests the licensor endorses you or your use.
    * NonCommercial — You may not use the material for commercial purposes. 
- more information here: https://creativecommons.org/licenses/by-nc/4.0/



The repository was created in relation to the following paper:
Chylinski D, Van Egroo M, Narbutas J, Muto V, Bahri MA, Berthomier C, Salmon E, Bastin C, Phillips C, Collette F, Maquet P, Carrier J, Lina JM, Vandewalle G
Timely sleep coupling: spindle-slow wave synchrony is linked to early amyloid-β burden and predicts memory decline, bioRxiv 2022
hdoi: https://doi.org/10.1101/2022.03.15.484463

