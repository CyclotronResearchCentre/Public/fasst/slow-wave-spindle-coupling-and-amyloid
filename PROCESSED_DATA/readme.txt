TST = total sleep time
flut_mpfc = amyloid burden in the MPFC
cos_o_allSW_N2N3 = spindle onset on the SW phase for all SW (in N2 & N3 combined)
cos_o_slow_N2N3 = spindle onset on the SW phase for slow switcher SW (in N2 & N3 combined)
cos_o_fast_N2N3 = spindle onset on the SW phase for fast switcher SW (in N2 & N3 combined)
meanSW_dens = mean SW density per minute
spdl_dens = spindle density per minute
SWE_subj = cumulated power over the 0.5-4Hz range
prop_slow_fast_delta = proportion of cumulated power 0.5Hz-1Hz over the 1.25-4Hz
rm = MST score of the baseline assessment
rm_d_n = MST change over the 2years (baseline score - follow up score / baseline score)