function MultiParMapCreation_hMRI(p)
%
% Script: MRI multiparameter map creation for data generated with the CRC Prisma
% scanner. This script uses the maps creation function implemented in hMRI
% toolbox under SPM12. Data should be previously reoriented (reorientation
% must be done only one single time to all available data (MPMs,
% functional, or any other data in the subject folder).
%
% M.Bahri: 2019/06/14
% -------------------------------------------------------------------------
% =========================================================================

% spm fmri;
global data;
% root_pth ='home/cofitage/DATA_COF/PET_Analysis';
root_pth ='C:\Cofitage\DATA_COF\PET_Analysis';
% Structure containing subjects names
subjinfo = struct('names',[],'mrinames',[],'sbzero',[],'sbone',[],'spdw',[],'smtw',[],'stonew',[]);
% Please encode your data here

% Old Subjects
% subjinfo.names = {'';''};
% subjinfo.mrinames = {'';''};
% % Indicate the session number for each parameter
% subjinfo.stonew = {'8' ; '2' ; '2' ; '2' ; '8' ; '8' ; '2' ; '8' ; ...
% '8' ; '2' ; '8' ; '8' ; '8' ; '8' ; '2' ; '2' ; '8' ; ...
% '2' ; '5' ; '2' ; '2' ; '2' ; '8' ; '5' ; '2' ; '5' ; ...
% '2' ; '8' ; '8' ; '2' ; '8' ; '2' ; '3' ; '5' ; '2' ; ...
% '5' ; '5' ; '2' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; ...
% '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; ...
% '5' ; '5' ; '5' ; '5' ; '5' ; '2' ; '5' ; '5' ; '5' ; ...
% '5' ; '5' ; '2' ; '5' ; '5' ; '5' ; '6' ; '5' ; '5' ; ...
% '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; ...
% '4' ; '3' ; '5' ; '5' ; '5' ; '5' ; '5'};
% subjinfo.spdw = {'10' ; '4' ; '4' ; '4' ; '10' ; '10' ; '4' ; '10' ; ...
% '10' ; '4' ; '10' ; '10' ; '10' ; '10' ; '4' ; '4' ; '10'; ...
% '4' ; '7' ; '4' ; '4' ; '4' ; '10' ; '7' ; '4' ; '7' ; ...
% '4' ; '10' ; '10' ; '4' ; '10' ; '4' ; '5' ; '7' ; '4' ; ...
% '7' ; '7' ; '4' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; ...
% '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; ...
% '7' ; '7' ; '7' ; '7' ; '7' ; '4' ; '7' ; '7' ; '7' ; ...
% '7' ; '7' ; '4' ; '7' ; '7' ; '7' ; '8' ; '7' ; '7' ; ...
% '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; ...
% '6' ; '5' ; '7' ; '7' ; '7' ; '7' ; '7'};
% subjinfo.smtw = {'12' ; '6' ; '6' ; '6' ; '12' ; '12' ; '6' ; '12' ; ...
% '12' ; '6' ; '12' ; '13' ; '12' ; '12' ; '6' ; '6' ; '12' ; ...
% '6' ; '9' ; '6' ; '6' ; '6' ; '12' ; '9' ; '6' ; '9' ; ...
% '6' ; '12' ; '12' ; '6' ; '12' ; '6' ; '7' ; '9' ; '6' ; ...
% '9' ; '9' ; '6' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; ...
% '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; ...
% '9' ; '9' ; '9' ; '9' ; '9' ; '6' ; '9' ; '9' ; '9' ; ...
% '9' ; '9' ; '6' ; '9' ; '9' ; '9' ; '10' ; '9' ; '9' ; ...
% '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; ...
% '8' ; '7' ; '9' ; '9' ; '9' ; '9' ; '9'};
% subjinfo.sbone = {'14' ; '8' ; '8' ; '8' ; '14' ; '14' ; '8' ; '14' ; ...
% '14' ; '8' ; '14' ; '15' ; '14' ; '14' ; '8' ; '8' ; '14' ; ...
% '8' ; '11' ; '8' ; '8' ; '8' ; '14' ; '11' ; '8' ; '11' ; ...
% '8' ; '14' ; '14' ; '8' ; '14' ; '8' ; '9' ; '11' ; '8' ; ...
% '11' ; '11' ; '8' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; ...
% '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; ...
% '11' ; '11' ; '11' ; '11' ; '11' ; '8' ; '11' ; '11' ; '11' ; ...
% '11' ; '11' ; '8' ; '11' ; '11' ; '11' ; '12' ; '11' ; '11' ; ...
% '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; ...
% '10' ; '9' ; '11' ; '11' ; '11' ; '11' ; '11'};
% subjinfo.sbzero = {'15' ; '9' ; '9' ; '9' ; '15' ; '15' ; '9' ; '15' ; ...
% '15' ; '9' ; '15' ; '16' ; '15' ; '15' ; '9' ; '9' ; '15' ; ...
% '9' ; '12' ; '9' ; '9' ; '9' ; '15' ; '12' ; '9' ; '12' ; ...
% '9' ; '15' ; '15' ; '9' ; '15' ; '9' ; '10' ; '12' ; '9' ; ...
% '12' ; '12' ; '9' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; ...
% '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; ...
% '12' ; '12' ; '12' ; '12' ; '12' ; '9' ; '12' ; '12' ; '12' ; ...
% '12' ; '12' ; '9' ; '12' ; '12' ; '12' ; '13' ; '12' ; '12' ; ...
% '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; ...
% '11' ; '10' ; '12' ; '12' ; '12' ; '15' ; '12'};

% % New Subjects 1
% subjinfo.names = {'';''};
% subjinfo.mrinames = {'';''};
% % Indicate the session number for each parameter
% subjinfo.stonew = {'2' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; ...
% '5' ; '3' ; '5' ; '5' ; '5' ; '5'};
% subjinfo.spdw = {'4' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; ...
% '7' ; '5' ; '7' ; '7' ; '7' ; '9'};
% subjinfo.smtw = {'6' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; ...
% '9' ; '7' ; '9' ; '9' ; '9' ; '11'};
% subjinfo.sbone = {'8' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; '11' ; ...
% '11' ; '9' ; '11' ; '11' ; '11' ; '13'};
% subjinfo.sbzero = {'9' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12' ; ...
% '12' ; '10' ; '12' ; '12' ; '12' ; '14'};

% New Subjects 2
% subjinfo.names = {''};
% subjinfo.mrinames = {''};
% % Indicate the session number for each parameter
% subjinfo.stonew = {'5'};
% subjinfo.spdw = {'7'};
% subjinfo.smtw = {'9'};
% subjinfo.sbone = {'11'};
% subjinfo.sbzero = {'12'};

% New Subjects 3
subjinfo.names = {''};
subjinfo.mrinames = {''};
% Indicate the session number for each parameter
subjinfo.stonew = {'8' ; '2' ; '8' ; '8' ; '2' ; '2' ; '8' ; '8' ; '8' ; '2' ; ...
'8' ; '2' ; '5' ; '5' ; '5' ; '5' ; '2' ; '5' ; '5' ; '5' ; '5' ; '5' ; '5' ; ...
'5' ; '3' ; '5' ; '5' ; '3' ; '5' ; '5' ; '5'};
subjinfo.spdw = {'10' ; '4' ; '10' ; '10' ; '4' ; '4' ; '10' ; '10' ; '10' ; '4' ; ...
'10' ; '4' ; '7' ; '7' ; '7' ; '7' ; '4' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; '7' ; ...
'5' ; '7' ; '7' ; '5' ; '7' ; '7' ; '7'};
subjinfo.smtw = {'12' ; '6' ; '13' ; '12' ; '6' ; '6' ; '12' ; '12' ; '12' ; '6' ; ...
'12' ; '6' ; '9' ; '9' ; '9' ; '9' ; '6' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; '9' ; ...
'7' ; '9' ; '9' ; '7' ; '9' ; '9' ; '9'};
subjinfo.sbone = {'14' ; '8' ; '15' ; '14' ; '8' ; '8' ; '14' ; '14' ; '14' ; '8' ; ...
'14' ; '8' ; '11' ; '11' ; '11' ; '11' ; '8' ; '11' ; '11' ; '11' ; '11' ; '11' ; ...
'11' ; '11' ; '9' ; '11' ; '11' ; '9' ; '11' ; '11' ; '11'};
subjinfo.sbzero = {'15' ; '9' ; '16' ; '15' ; '9' ; '9' ; '15' ; '15' ; '15' ; '9' ; ...
'15' ; '9' ; '12' ; '12' ; '12' ; '12'; '9' ; '12' ; '12' ; '12' ; '12' ; '12' ; '12'	; ...
'12' ; '10' ; '15' ; '12' ; '10' ; '12' ; '12' ; '12'};

% Note: data is supposed to be organized as follow
% /root_pth/subject-name/MRI/nii

% Get data
data = get_data(root_pth,subjinfo);

for isub = 1:size(data,2)
    fprintf(1,'PROCESSING SUBJECT %i : %s\n',isub,data(isub).id)

    % load data
    bzo = get_Bzoimg(isub);
    bone = get_Boneimg(isub);
    mtw = get_mtwimg(isub);
    pdw = get_pdwimg(isub);
    tonew = get_tonewimg(isub);

    % batch initialization
    matlabbatch=[];
    % Get subject information for matlabbatch
    matlabbatch{1}.spm.tools.hmri.hmri_config.hmri_setdef.customised = cellstr([fullfile(spm('Dir'),'toolbox','hMRI','config','hmri_defaults.m')]);
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.output.indir = 'yes';
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.sensitivity.RF_us = '-';
    %%
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1input = cellstr(bone);
    %%
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b0input = cellstr(bzo);
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.b1_type.i3D_EPI.b1parameters.b1metadata = 'yes';
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.MT = cellstr(mtw);
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.PD = cellstr(pdw);
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.raw_mpm.T1 = cellstr(tonew);
    matlabbatch{2}.spm.tools.hmri.create_mpm.subj.popup = true;
    % Saving temporary batch
    dirindex = findstr(filesep,data(isub).dir);
    strdir = fullfile(data(isub).dir(1:dirindex(end)),'JOBS');
    str_ssname = data(isub).id;

    if ~exist(strdir)
        mkdir(strdir)
    end
    cd(strdir);

    eval(['save Job_MPM_creation_' str_ssname '_' datestr(now,30) ' matlabbatch'])
    spm_jobman('run',matlabbatch)

end
end

% =========================================================================
%                              Functions
% =========================================================================

function [bzo] = get_Bzoimg(isub);
    global data
    souname = data(isub).idmri;
    bzero = str2num(data(isub).bzero);
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'MRI','nii');

    if(length(bzero) < 1 & bzero < 9)
        [bzo]=spm_select('FPList',dirstruc,strcat('^s',souname,'-000',num2str(bzero),'-.+\.nii$'));
        [bzo]=[bzo;spm_select('FPList',dirstruc,strcat('^s',souname,'-000', num2str(bzero + 1),'-.+\.nii$'))];
    elseif(bzero == 9)
        [bzo]=spm_select('FPList',dirstruc,strcat('^s',souname, '-000', num2str(bzero),'-.+\.nii$'));
        [bzo]=[bzo;spm_select('FPList',dirstruc,strcat('^s',souname,'-00', num2str(bzero + 1),'-.+\.nii$'))];
    else
        [bzo]=spm_select('FPList',dirstruc,strcat('^s',souname,'-00',num2str(bzero),'-.+\.nii$'));
        [bzo]=[bzo;spm_select('FPList',dirstruc,strcat('^s',souname,'-00', num2str(bzero + 1),'-.+\.nii$'))];
    end
    return
end

function [bone] = get_Boneimg(isub);
    global data
    souname = data(isub).idmri;
    sbone = data(isub).bone;
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'MRI','nii');
    if(length(sbone) < 2)
        [bone]=spm_select('FPList',dirstruc,strcat('^s',souname,'-000',num2str(sbone), '-.+\.nii$'));
    else
        [bone]=spm_select('FPList',dirstruc,strcat('^s',souname,'-00',num2str(sbone), '-.+\.nii$'));
    end

    return
end

function [mtw] = get_mtwimg(isub);
    global data
    souname = data(isub).idmri;
    dirstruc = [];
    smtw = data(isub).mtw;
    dirstruc = fullfile(data(isub).dir,'MRI','nii');
    if(length(smtw) < 2)
        [mtw]=spm_select('FPList',dirstruc,strcat('^s',souname,'-000',num2str(smtw),'-.+\.nii$'));
    else
        [mtw]=spm_select('FPList',dirstruc,strcat('^s',souname,'-00',num2str(smtw),'-.+\.nii$'));
    end

    return
end

function [pdw] = get_pdwimg(isub);
    global data
    souname = data(isub).idmri;
    dirstruc = [];
    spdw = data(isub).pdw;
    dirstruc = fullfile(data(isub).dir,'MRI','nii');
    if(length(spdw) < 2)
        [pdw]=spm_select('FPList',dirstruc,strcat('^s',souname,'-000',num2str(spdw),'-.+\.nii$'));
    else
        [pdw]=spm_select('FPList',dirstruc,strcat('^s',souname,'-00',num2str(spdw),'-.+\.nii$'));
    end
    return
end


function [tonew] = get_tonewimg(isub);
    global data
    souname = data(isub).idmri;
    dirstruc = [];
    stone = data(isub).tone;
    dirstruc = fullfile(data(isub).dir,'MRI','nii');
    if(length(stone) < 2)
        [tonew]=spm_select('FPList',dirstruc,strcat('^s',souname,'-000',num2str(stone),'-.+\.nii$'));
    else
        [tonew]=spm_select('FPList',dirstruc,strcat('^s',souname,'-00',num2str(stone),'-.+\.nii$'));
    end
    return
end


% =========================================================================
% ======================= Put your data here ==============================

function [data] = get_data(root_pth,subjinfo)
data = struct('DIR',[root_pth], ...
'id',[], ...
'idmri',[], ...
'dir',[], ...
'bzero',[], ...
'bone',[], ...
'pdw',[], ...
'mtw',[], ...
'tone',[]);
            for isubj = 1:size(subjinfo.names,1)
                data(isubj).id      = subjinfo.names{isubj};
                data(isubj).idmri   = subjinfo.mrinames{isubj};
                data(isubj).dir     = fullfile(root_pth,subjinfo.names{isubj});
                data(isubj).bzero   = subjinfo.sbzero{isubj};
                data(isubj).bone    = subjinfo.sbone{isubj};
                data(isubj).pdw     = subjinfo.spdw{isubj};
                data(isubj).mtw     = subjinfo.smtw{isubj};
                data(isubj).tone    = subjinfo.stonew{isubj};

            end
end
