function Spatial_Processing_MPM_Dartel_hMRI()

% Script description: The script gets all the data (MT, PD, R1, and R2
% maps) and send them to segmentation function (under hMRI toolbox). The
% segmented rc1, rc2, rc3 are sent to DARTEL module, which creates the
% study specific template and provides flowfield for each subject. The script
% normalizes the GM, WM and MPM using flow-field and study
% specific template.
%
% The study specific template is saved in directory of the 1st subject.
% This script requires the batch 'Batch_Spatial_processing_MPM_hMRI.mat'
%
% M.Bahri CRC, University of Liege: 2019/06/14
% -------------------------------------------------------------------------
% =========================================================================

%spm fmri;
global data;
%root_pth = '/home/bahri/Documents/Projects/Justinas/Cofitage';
%batch_pth = '/home/bahri/Documents/Projects/Justinas/Centiloid/Scripts';
root_pth = 'C:\Cofitage\DATA_COF\PET_Analysis';
batch_pth = 'C:\Cofitage\Analyse\PET';
% Structure containing subjects' names
subjinfo = struct('names',[],'mrinames',[]);
% Please encode you data here

% Old Template
% subjinfo.names = {''};
% subjinfo.mrinames = {''};

% New Template
subjinfo.names = {''};
subjinfo.mrinames = {''};

% Note: data is supposed to be organized as follow:
% /root_pth/subject-name/PET (PET data)
% /root_pth/subject-name/MRI/nii (MRI files)

% Get data
data = get_data(root_pth,subjinfo);
% Get MT,PD,R1, and R2 files.
for isub = 1:length(data)
    souname = data(isub).idmri;
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'MRI','nii','Results');
%     MT_files{isub,:} = [spm_select('FPList',dirstruc,strcat('^s*',souname,'-.+_MTsat.nii$'))];
    MT_files{isub,:} = [spm_select('FPList',dirstruc,strcat('^s*',souname,'-.+_MT.nii$'))];
    PD_files{isub,:} = [spm_select('FPList',dirstruc,strcat('^s*',souname,'-.+_PD.nii$'))];;
    R1_files{isub,:} = [spm_select('FPList',dirstruc,strcat('^s*',souname,'-.+_R1.nii$'))];
    R2_files{isub,:} = [spm_select('FPList',dirstruc,strcat('^s*',souname,'-.+_R2s_OLS.nii$'))];
end


% Load batch
load(fullfile(batch_pth,'Batch_Spatial_processing_MPM_hMRI.mat'));

matlabbatch{1}.cfg_basicio.file_dir.file_ops.cfg_named_file.name = 'MT_maps';
matlabbatch{1}.cfg_basicio.file_dir.file_ops.cfg_named_file.files = {MT_files};
matlabbatch{2}.cfg_basicio.file_dir.file_ops.cfg_named_file.name = 'PD_maps';
matlabbatch{2}.cfg_basicio.file_dir.file_ops.cfg_named_file.files = {PD_files};
matlabbatch{3}.cfg_basicio.file_dir.file_ops.cfg_named_file.name = 'R1_maps';
matlabbatch{3}.cfg_basicio.file_dir.file_ops.cfg_named_file.files = {R1_files};
matlabbatch{4}.cfg_basicio.file_dir.file_ops.cfg_named_file.name = 'R2_maps';
matlabbatch{4}.cfg_basicio.file_dir.file_ops.cfg_named_file.files = {R2_files};

% Tissue probabiliy maps
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(1).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,1'));
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(2).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,2'));;
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(3).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,3'));
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(4).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,4'));
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(5).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,5'));
matlabbatch{5}.spm.tools.hmri.proc.proc_modul.proc_us.tissue(6).tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,6'));


matlabbatch{8}.spm.tools.hmri.proc.proc_modul.proc_smooth.tpm = cellstr(fullfile(spm('dir'),'toolbox','hMRI','etpm','eTPM.nii,1'));
matlabbatch{8}.spm.tools.hmri.proc.proc_modul.proc_smooth.fwhm = [4 4 4];

% Save the temporary batch
strdir = fullfile(root_pth,'JOBS');
if ~exist(strdir)
    mkdir(strdir)
end
cd(strdir);
eval(['save job_Spatial_process_MPM_DARTEL_hMRI_' datestr(now,30) ' matlabbatch']);
spm_jobman('run',matlabbatch)

end

% =========================================================================
%                              Functions
% =========================================================================
function [data] = get_data(root_pth,subjinfo)
data = struct('DIR',[root_pth], ...
'id',[], ...
'idmri',[], ...
'dir',[]);
            for isubj = 1:size(subjinfo.names,1)
            	data(isubj).id      = subjinfo.names{isubj};
	            data(isubj).idmri   = subjinfo.mrinames{isubj};
                data(isubj).dir     = fullfile(root_pth,subjinfo.names{isubj});
	         end
end
% =========================================================================
