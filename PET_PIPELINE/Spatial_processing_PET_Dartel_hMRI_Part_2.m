function Spatial_processing_PET_Dartel_hMRI_Part_2(p)
%
% Script Part II: spatial processing of PET data of two radiotracers
% (THK5351 and flutemetamol). The script contains: loading of data (PET
% sumframe, MT structural MRI image and AAL atlas), coregistration of the
% PET to the MT image, normalisation of the PET using the study_specific
% template and the flow field obtained with DARTEL, extraction of the
% regional uptake and creation of RSUV image.
% THK5351: RSUV image was obtained by dividing the normalised pve corrected
% PET image by the mean uptake of the cerebelum GM.
% Text file containing the cerebelum GM mean uptake values.
% Flutemetamol: text file containing all AAL rois uptake values.
% Note: aal2_atlas is not available on spm12 toolbox, this atlas could be.
%       found on the SPM website.
%
% Attention! Adapt PET path ("FLUT" or "THK") according to radiotracer.
%
% M.Bahri: 2017/18/10
% -------------------------------------------------------------------------
% =========================================================================


%spm pet;
global data;
%root_pth = '/home/bahri/Documents/Projects/Justinas/Centiloid/Klunk_Data';
%batch_pth = '/home/bahri/Documents/Projects/Justinas/Centiloid/Scripts/Batchs';
root_pth = 'C:\Cofitage\DATA_COF\PET_Analysis';
batch_pth = 'C:\Cofitage\Analyse\PET\Batch';
% Structure containing subjects' ID and MRI ID
subjinfo = struct('names',[],'mrinames',[]);

% Please encode your data here
% % FLUT Subjects
% subjinfo.names = {''}; % Only current FLUT subjects 
% subjinfo.mrinames = {''}; % Only current FLUT subjects 

% % THK Subjects
% subjinfo.names = {''}; % Only THK subjects
% subjinfo.mrinames = {''};  % Only THK subjects

% subjinfo.names = {'';''};
% subjinfo.mrinames = {'';''};

subjinfo.names = {''}; %
subjinfo.mrinames = {''}; % 

% Note: data is supposed to be organized as follow:
% /root_pth/subject-id/PET (PET data)
% /root_pth/subject-id/MRI/nii (MRI files)

% Get data
data = get_data(root_pth,subjinfo);

% Load Centiloid Cortical mask
Introi_Cort = fullfile(root_pth,'voi_ctx_2mm_bbch.nii');
ma1 = spm_vol(Introi_Cort);
mask_Cort = spm_read_vols(ma1);
indCort = find(mask_Cort > 0);

% Load Centiloid Cerebelum mask
Refroi_Cereb = fullfile(root_pth,'voi_WhlCbl_2mm_bbch.nii');
ma2 = spm_vol(Refroi_Cereb);
mask_Cereb = spm_read_vols(ma2);
indCereb = find(mask_Cereb > 0);

% Load AAL Atlas (image and labels)
aal2_atlas = fullfile(spm('Dir'),'toolbox','aal','ROI_MNI_V5_bbch.nii');
aal2_rois = fullfile(spm('Dir'),'toolbox','aal','ROI_MNI_V5.txt');
% Read atlas file
atlas_img = spm_vol(aal2_atlas);
Vol_atlas = spm_read_vols(atlas_img);
mask_values = round(unique(Vol_atlas));
Vol_atlasRounded = round(Vol_atlas);
% get rois names
roisfile = readcoglog(aal2_rois);

% Output: open a text file
fid = fopen(fullfile(root_pth,'PET_COF_RAW.txt'), 'w');
fidsuvr = fopen(fullfile(root_pth,'PET_COF_SUVR.txt'), 'w');

fprintf(fid,'Subject_id\t')
fprintf(fidsuvr,'Subject_id\t')
for roi=1:length(roisfile)
    fprintf(fid,'%s\t',char(roisfile{roi}(2)));
    fprintf(fidsuvr,'%s\t',char(roisfile{roi}(2)));

end
fprintf(fid,'%s\t%s','Cortex','WholeCereb');
fprintf(fidsuvr,'%s\t%s','Cortex','WholeCereb');



for isub = 1:size(data,2)
    fprintf(1,'PROCESSING SUBJECT %i : %s\n',isub,data(isub).id)

    % Load images
    MTw = get_MTw(isub);
    PETfile = get_PETfile(isub);
    flowfield = get_flowfield(isub);

    % Coregister PET into MRI MTw image using SPM
    matlabbatch{1}.spm.spatial.coreg.estimate.ref = cellstr(MTw);
    matlabbatch{1}.spm.spatial.coreg.estimate.source = cellstr(PETfile);
    matlabbatch{1}.spm.spatial.coreg.estimate.other = {''};
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.cost_fun = 'nmi';
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.sep = [4 2];
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.tol = [0.02 0.02 0.02 0.001 0.001 0.001 0.01 0.01 0.01 0.001 0.001 0.001];
    matlabbatch{1}.spm.spatial.coreg.estimate.eoptions.fwhm = [7 7];
    % Normalize PET image using Template and flowfield
    % matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.template = {'/home/bahri/Documents/Projects/Justinas/Centiloid/Klunk_Data/Elder_Subjects/AD001/MRI/Template_6.nii'};
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.template = {'C:\Cofitage\DATA_COF\PET_Analysis\COF002\MRI\nii\Results\Template_6.nii'};
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.multsdata.vols_tc = cell(1, 0);
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.multsdata.vols_pm{1}(1) = cfg_dep('Coregister: Estimate: Coregistered Images', substruct('.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}, '.','val', '{}',{1}), substruct('.','cfiles'));
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.multsdata.vols_field = cellstr(flowfield);
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.output.indir = 1;
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.vox = [2 2 2];
    matlabbatch{2}.spm.tools.hmri.proc.proc_modul.proc_dart.mni_norm.bb = [NaN NaN NaN
                                                                           NaN NaN NaN];

    % Save the temporary batch
    strdir = fullfile(root_pth,'JOBS');
    if ~exist(strdir)
        mkdir(strdir)
    end
    cd(strdir);    eval(['save job_Spatial_process_PET_DARTEL_hMRI_' data(isub).id '_' datestr(now,30) ' matlabbatch']);
    spm_jobman('run',matlabbatch)

    clear matlabbatch;

    % Extract Cerebelum cortical value for each subject
    [pth,nam,ext] = fileparts(PETfile);
    Ma = spm_vol(fullfile(pth,['w' nam ext]));
    subj_vol = spm_read_vols(Ma);

    % Calculate mean value of Cortical region
    mean_Cort = mean(subj_vol(indCort));
    mean_Cereb = mean(subj_vol(indCereb));

    % Normalized PET division by value in ROI Cerebelum GM
    f = strcat('i1 ./ ',num2str(mean_Cereb));
    value1 = round(mean_Cereb);
    matlabbatch{1}.spm.util.imcalc.input = {fullfile(pth,['w' nam ext])};
    matlabbatch{1}.spm.util.imcalc.output = fullfile(pth,['w' nam strcat('_divROI_',num2str(value1)) ext]);
    matlabbatch{1}.spm.util.imcalc.outdir = {pth};
    matlabbatch{1}.spm.util.imcalc.expression = f;
    spm_jobman('run',matlabbatch);
    clear matlabbatch;

    fprintf(fid,'\n%s\t',data(isub).id);
    fprintf(fidsuvr,'\n%s\t',data(isub).id);
    % Extract AAL ROIs mean values for each subject
    for i = 1:length(roisfile)
        region_mean(i) = mean(subj_vol(ind2sub(size(Vol_atlas),find(Vol_atlasRounded == str2num(char(roisfile{i}(3)))))));
        fprintf(fid,'%d\t', region_mean(i));
        fprintf(fidsuvr,'%d\t', region_mean(i)/mean_Cereb);
    end
    % write in the text file Cortical and cerebelum mean values
    fprintf(fid, '%d\t%d',mean_Cort,mean_Cereb);
    fprintf(fidsuvr, '%d\t%d',mean_Cort/mean_Cereb,mean_Cereb/mean_Cereb);
    print('Done');
end
status = fclose(fid);
status = fclose(fidsuvr);
end


% =========================================================================
%                              Functions
% =========================================================================

function [MTw] = get_MTw(isub);
    global data
    souname = data(isub).idmri;
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'MRI','nii','Results');
    [MTw]=spm_select('FPList',dirstruc,strcat('^s',souname,'-.+_MT.nii$'));
    return
end

function [PETfile] = get_PETfile(isub);
    global data
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'PET','FLUT');
    [PETfile]=spm_select('FPList',dirstruc,strcat('^*.+.nii$'));
    return
end

function [flowfield] =  get_flowfield(isub);
    global data
    souname = data(isub).idmri;
    dirstruc = [];
    dirstruc = fullfile(data(isub).dir,'MRI','nii','Results');
    [flowfield]=spm_select('FPList',dirstruc,strcat('^u_rc1s',souname,'.+_Template.nii$'));
    return
end


% =========================================================================
% ======================= Put your data here ==============================

function [data] = get_data(root_pth,subjinfo)
    data = struct('DIR',[root_pth], ...
    'id',[], ...
    'idmri',[], ...
    'dir',[]);

    for isubj = 1:size(subjinfo.names,1)
        data(isubj).id      = subjinfo.names{isubj};
        data(isubj).idmri   = subjinfo.mrinames{isubj};
        data(isubj).dir     = fullfile(root_pth,subjinfo.names{isubj});
    end
end
% =========================================================================
