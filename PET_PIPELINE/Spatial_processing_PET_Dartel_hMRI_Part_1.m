function Spatial_processing_PET_Dartel_hMRI_Part_1()
%
% % Script Part I: spatial processing of PET data of two radiotracers
% (THK5351 and flutemetamol). The script contains : Loading data (PET raw
% data "Ecat format"), conversion of the data into .nii, rename of images,
% realignment of the dynamic data and creation of the sumframe image.
%
% THK5351: 40 to 60min corresponding to 23-26 frames
% Flutemetamol: all frames
% 
% Attention! Adapt PET path ("FLUT" or "THK") according to radiotracer.
% 
% M.Bahri: 2019/06/14
% -------------------------------------------------------------------------
% =========================================================================

% Radiotracers: THK5153 = 1; Flutemetamol = 2;
radiotracer = 2;
% spm pet;
global data;
% root_pth ='/home/justinas/cofitage/DATA_COF/PET_Analysis';
root_pth = 'C:\Cofitage\DATA_COF\PET_Analysis';
% Structure containing subjects names
subjinfo = struct('names',[],'mrinames',[]);
% Please encode your data here

% FLUT Subjects
% subjinfo.names = {''}; % Only current FLUT subjects 
% subjinfo.mrinames = {''};  % Only current FLUT subjects

% % THK Subjects
% subjinfo.names = {''}; % Only THK subjects
% subjinfo.mrinames = {''};  % Only THK subjects

% subjinfo.names = {''};
% subjinfo.mrinames = {''};

% subjinfo.names = {'';''};
% subjinfo.mrinames = {'';''};

% subjinfo.names = {''};
% subjinfo.mrinames = {''};

% subjinfo.names = {''};
% subjinfo.mrinames = {''};

subjinfo.names = {''};
subjinfo.mrinames = {''};
    
% Note: data is supposed to be organized as follow:
% /root_pth/subject-name/PET (PET data)
% /root_pth/subject-name/MRI/nii (MRI files)

% Get data
data = get_data(root_pth,subjinfo);

for isub = 2:size(data,2) % Attention! Remove comments for full processing
    fprintf(1,'PROCESSING SUBJECT %i : %s\n',isub,data(isub).id)
    % Dynamic PET: Loading ".v"
    PET_dir = fullfile(data(isub).dir,'PET\FLUT');
%     [rawPET] = spm_select('FPList',PET_dir,strcat('^',data(isub).id,'_.+\.v$'));
    % Conversion to ".nii"
    PETDyn_dir = fullfile(PET_dir,'Dynamic')
%     mkdir(PETDyn_dir);
%     cd(PETDyn_dir);
%     matlabbatch = [];
%     matlabbatch{1}.spm.util.import.ecat.data = cellstr(rawPET);
%     matlabbatch{1}.spm.util.import.ecat.opts.ext = 'nii';
%     spm_jobman('run',matlabbatch);
%     clear matlabbatch;
    % Rename converted files
%     [ncfiles]=spm_select('FPList',PETDyn_dir,strcat('^',data(isub).id,'*.+\.nii$'));
%     for i = 1:size(ncfiles,1)
%         [pthref,namref,extref] = fileparts(ncfiles(i,:));
%         if(i < 10)
%             movefile(ncfiles(i,:),fullfile(pthref,[namref(1:end-2) '-0' num2str(i) extref]));
%         else
%             movefile(ncfiles(i,:),fullfile(pthref,[namref(1:end-2) '-' num2str(i) extref]));
%         end
%     end
    % Create average image (THK5153: 40-60 minutes coresponding to 23-26
    % frames
    [niifiles]=spm_select('FPList',PETDyn_dir,strcat('^*',data(isub).id,'*.+\.nii$'));
    [pth,nam,ext] = fileparts(niifiles(1,:));
    cd(PET_dir);
    % THK5153: 40-60 minutes coresponding to 23-26 frames
    if(radiotracer == 1)
        tpmfiles=[];
        for i=23:26
            tpmfiles = [tpmfiles;niifiles(i,:)];
        end
        matlabbatch{1}.spm.util.imcalc.input = cellstr(tpmfiles);
        matlabbatch{1}.spm.util.imcalc.output = fullfile(PET_dir,[nam '_40-60' ext]);
        matlabbatch{1}.spm.util.imcalc.expression = 'mean(X)';
        matlabbatch{1}.spm.util.imcalc.options.dmtx = 1;
        spm_jobman('run',matlabbatch);
        clear matlabbatch;
    end
    % Flutemetamol: average of all frames
    if(radiotracer == 2)
        matlabbatch{1}.spm.util.imcalc.input = cellstr(niifiles);
        matlabbatch{1}.spm.util.imcalc.output = fullfile(PET_dir,[nam '_avg' ext]);
        matlabbatch{1}.spm.util.imcalc.expression = 'mean(X)';
        matlabbatch{1}.spm.util.imcalc.options.dmtx = 1;
        spm_jobman('run',matlabbatch);
        clear matlabbatch;

    end
end
fprintf(1,'Attention: since the PET image will be coregistered to structural image,\n. It is necessary to reorient the PET *_avg.nii or/and PET *_40-60.nii\n into the T1/MT structural MRI image before running PART II\n')
end

% =========================================================================
% ======================= Put your data here ==============================


function [data] = get_data(root_pth,subjinfo)
data = struct('DIR',[root_pth], ...
'id',[], ...
'idmri',[], ...
'dir',[]);
            for isubj = 1:size(subjinfo.names,1)
                data(isubj).id      = subjinfo.names{isubj};
                data(isubj).idmri   = subjinfo.mrinames{isubj};
                data(isubj).dir     = fullfile(root_pth,subjinfo.names{isubj});
            end
end
