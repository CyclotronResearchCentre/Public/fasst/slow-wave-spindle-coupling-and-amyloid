1. MultiParData_autoreorient_hMRI.m: Autoreorientation of MPM sequences (B0, B1, T1w, MTw, PDw) using canonical avg152T1.nii as a template. The reference image is the 1st volume of T1w sequence.

2. MultiParMapCreation_hMRI.m: Creates multiparametric maps from these MPM sequences.

3. Spatial_Processing_MPM_Dartel_hMRI.m: The script gets all the data (MT, PD, R1, and R2 maps) and sends them to segmentation function (under hMRI toolbox). The segmented rc1, rc2, rc3 are sent to DARTEL module, which creates the study specific template and provides flow-field for each subject. The script ormalizes the GM, WM and MPM using flow-field and study specific template.

4. Spatial_processing_PET_Dartel_hMRI_Part_1.m: Converts PET ECAT format to .nii, realignes the dynamic data and creates average PET image

5. Spatial_processing_PET_Dartel_hMRI_Part_2.m: Coregisters of the PET to the MT image, normalizes PET using the study specific template and the flow-field obtained with DARTEL, estimates regional uptake (SUVRs)

Batch_Spatial_processing_MPM_hMRI.mat: SPM batch for the spatial processing of MPM images

ROI_MNI_V5_bbch.nii: AAL atlas

voi_WhlCbl_2mm_bbch.nii: Whole cerebelum mask from Centiloid project.

