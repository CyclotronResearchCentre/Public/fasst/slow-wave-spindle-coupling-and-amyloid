function newO = set_up_Detector(O, P2Pthreshold, Negthreshold)
newO = O;
fprintf('\n');
% Utilise-t-on des seuils adaptes?
% Seuils standard de Carrier

for i_gr = 1:numel(O.DBase.groups)
    newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 75;
    newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 40;
end
% DC commented
%         answ = 'n'; answ = input(' --- Adapted SSW thresholds (Thaina) ? ([n]/y) ','s');
% DC added

% if strcmpi(answ,'y')
    ok_change = 0;
    for i_gr = 1:numel(O.DBase.groups)
        if strcmpi(O.DBase.groups{i_gr},'middle')
            newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 65;
            newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 34.5;
            fprintf('\n     New thresholds for SSW detection in %s : PaP = %3.2f and Neg = %3.2f', ...
                newO.DBase.groups{i_gr},newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP, ...
                newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg);
            ok_change = 1;
        end
        
        if strcmpi(O.DBase.groups{i_gr},'young')
            newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 75.5;
            newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 40;
            fprintf('\n     New thresholds for SSW detection in %s : PaP = %3.2f and Neg = %3.2f', ...
                newO.DBase.groups{i_gr},newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP, ...
                newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg);
            ok_change = 1;
        end
        %DC commented this
        %                 if ~ok_change
        %                  asw = input(' ---> new PaP threshold (standard 75) ? ','s');
        %                  newO.Sleep.SSW.Carrier.PaP = str2num(asw);
        %                  asw = input(' ---> new Neg threshold (standard 40) ? ','s');
        %                  newO.Sleep.SSW.Carrier.Neg = str2num(asw);
        %                  fprintf('\n     New thresholds for SSW detection:\n     PaP = %3.2f and Neg = %3.2f\n', ...
        %                  newO.Sleep.SSW.Carrier.PaP, ...
        %                  newO.Sleep.SSW.Carrier.Neg);
        %                 end
        % DC added this
        if ~ok_change
            newO.Sleep.SSW.Carrier.PaP = P2Pthreshold;
            newO.Sleep.SSW.Carrier.Neg = Negthreshold;
            fprintf('\n     New thresholds for SSW detection:\n     PaP = %3.2f and Neg = %3.2f\n', ...
                newO.Sleep.SSW.Carrier.PaP, ...
                newO.Sleep.SSW.Carrier.Neg);
        end
        %
    end
% else
%     fprintf('\n     Standard thresholds for SSW detection');
% end
fprintf('\n');pause(1);
end