function set_up_Data(O)

SCALE = 10^-6;
fprintf('\n--- Inventaire et archivage des parametres:\n');
for gr = 1:numel(O.DBase.Inventaire.relative_files_mat)
    liste = '';
    DBase_gr = O.DBase.Inventaire.relative_files_mat{gr};
    DBase_gr_liste = O.DBase.listes{gr};
    for suj = 1:size(DBase_gr,1)
        name = DBase_gr_liste{suj};
        for cond = 1:size(DBase_gr,2)
            load(DBase_gr{suj,cond});
            Ntr = numel(trials.trials.data);
            condition = trials.type;
            trials_suj = trials.trials;
            Nele = numel(trials_suj.data{1}.LUT);
            fs   = trials_suj.data{1}.fs;
            fprintf('\t--> %s (%s OK): condition %s, %d trials (%d electrodes)\n',...
                name,trials_suj.sujet_file,condition,Ntr,Nele);
                for ele = 1:Nele
                D{cond}{ele}.data      = [];
                D{cond}{ele}.fs        = fs;
                D{cond}{ele}.electrode = trials_suj.data{1}.LUT{ele};
                    for itr = 1:Ntr
                    D{cond}{ele}.data(itr,:) = trials_suj.data{itr}.data(ele,:)*SCALE;
                    end
                D{cond}{ele}.Time = (0:size(trials_suj.data{itr}.data,2)-1)/fs;
                end
        end
        % ARCHIVAGE:
        Comments = 'D{condition}{electrode}';
        TR.Id   = [gr suj];
        TR.name = name;
        TR.D = D;
        TR.Comments = Comments;
        save_path = [O.Results.out_dbase '/Dgr_suj_' num2str(gr) '_' num2str(suj) '__' name];
        save(save_path,'TR','O');
        liste = sprintf('%s\n%s',liste,name);
    end
    
    save_liste = [O.Results.out_dbase '/liste_gr_' num2str(gr)];
    save(save_liste,'liste');
    
end
