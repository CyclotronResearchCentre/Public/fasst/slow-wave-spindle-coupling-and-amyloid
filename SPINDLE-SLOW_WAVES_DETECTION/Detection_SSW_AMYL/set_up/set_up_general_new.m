function OPTIONS = set_up_general_new(OPTIONS)

% set-up of the computations

fsep = filesep;


% ======== SPeCIFIC CHOICE
% OPTIONS.Sleep.stages = [2 3];
% OPTIONS.Sleep.cycles = [1];
% OPTIONS.Sleep.eeg_electrodes = 'fz';


% ======== Parameters (a priori, not to be changed)
% Carrier Standard:
OPTIONS.Sleep.SSW.method = 'Carrier0.3_4Hz'; % can be Molle
OPTIONS.Sleep.SSW.Carrier.fmin_max = [0.3 4]; % 4]; %1.3]; % Hz  (initialement 0.16 - 4 Hz)
OPTIONS.Sleep.SSW.Carrier.duree_min_max_Neg = [125 1500]; % msec
OPTIONS.Sleep.SSW.Carrier.duree_max_Pos = 1000; % msec
OPTIONS.Sleep.SSW.Carrier.PaP = 75; % uV
OPTIONS.Sleep.SSW.Carrier.Neg = 40; % uV

OPTIONS.Sleep.SSW.Molle.fmin_max = [0.16 1.25]; % Hz
OPTIONS.Sleep.SSW.Molle.duree_min_max = [600 2500]; % msec
OPTIONS.Sleep.SSW.Molle.percentile = 75; % in %

% Fuseaux
OPTIONS.Sleep.SPL.method = 'Walker';
OPTIONS.Sleep.SPL.fmin_max = [10 16]; % Hz
OPTIONS.Sleep.SPL.duree_min_max = [500 3000]; % msec
OPTIONS.Sleep.SPL.percentile = 75; % in %
OPTIONS.Sleep.SPL.smoothing = 200;


OPTIONS.Results.relative_path = ['..' fsep 'Results' fsep];
pp = strsplit(OPTIONS.DBase.path,fsep); pp = pp{end}; pp = pp(1:end-5);
datename = date;
OPTIONS.Results.out_dir = ['..' fsep 'Results' fsep 'Resultats_' pp datename];
OPTIONS.Results.out_DBase = [OPTIONS.Results.out_dir  fsep 'DBase_SSW' fsep];%corrected from DB_matlab GV 27/07/21
OPTIONS.Results.out_figures = [OPTIONS.Results.out_dir fsep 'Figures'];
if ~exist(OPTIONS.Results.out_dir,'dir')
    mkdir(OPTIONS.Results.out_DBase);    
    mkdir(OPTIONS.Results.out_figures);
end

% DC commented and added
% OPTIONS.Results.out_dbase = [OPTIONS.DBase.path fsep 'Dgroup_mat'];
% if ~exist(OPTIONS.Results.out_dbase,'dir')
%     mkdir(OPTIONS.Results.out_dbase);
% end
OPTIONS.Results.out_dbase = [OPTIONS.Results.out_dir fsep 'Dgroup_mat'];
if ~exist(OPTIONS.Results.out_dbase,'dir')
    mkdir(OPTIONS.Results.out_dbase);
end
%

% Verification / changement
revision_methode_Carrier(OPTIONS.Sleep.SSW);



end


function revision_methode_Carrier(SSW)

% affichage:
fprintf('\n     SSW Detector: %s\n',SSW.method);
fprintf('     frequence: %3.2f - %3.2f (Hz)\n',SSW.Carrier.fmin_max(1),SSW.Carrier.fmin_max(2));
fprintf('     duree (Hyp) > %3.2f (msec)\n',SSW.Carrier.duree_min_max_Neg(1));
fprintf('     duree (Hyp) < %3.2f (msec)\n',SSW.Carrier.duree_min_max_Neg(2));
fprintf('     duree (Dep) < %3.2f (msec)\n',SSW.Carrier.duree_max_Pos);
fprintf('     Amplitude PaP > %3.2f (uV)\n',SSW.Carrier.PaP);
fprintf('     Amplitude Neg > %3.2f (uV)\n',SSW.Carrier.Neg);

% answer = input('\n --- Change for slow SSW: 0.16 - 1.3 (Hz) ? [n]/y ','s');
% if ischar(answer) && strcmp(answer,'y')
%     SSW.Carrier.fmin_max(1) = 0.16;
%     SSW.Carrier.fmin_max(2) = 1.3;
%     
%     SSW.method = 'Carrier0.16_4Hz_lentes';
%     fprintf('    Detection: %s\n',SSW.method);
%     fprintf('    frequences: %3.2f - %3.2f (Hz)\n',SSW.Carrier.fmin_max(1),SSW.Carrier.fmin_max(2));
%     fprintf('    (autres parametres identiques)\n');
% end
% fprintf('\n');

% SSWn = SSW;

end


