function [ SSW_SIG ] = SSW_SIG_correspondance( SSW, signals )
Nbre_trials = numel(SSW);

for it = 1:Nbre_trials
    SSW_SIG{it}.comment = 'amplitude de sigma dans ssw';

        ssw_interv = SSW{it}.n_t;
        Nbre_ssw   = size(ssw_interv,1);
        SSW_SIG{it}.sigma_in_ssw = zeros(Nbre_ssw,1);
        
            for j = 1:1:Nbre_ssw
                d = SSW{it}.n_t(j,1);
                f = SSW{it}.n_t(j,2);
                SSW_SIG{it}.sigma_in_ssw(j) = ...
                    max(signals.data_f_sigma_Hilbert(it,d:f));
            end
            
end
end

