function [marker,signals,N_SSW] = detect_SSW_Molle(signals,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% marker------------ .n_zc_critere_temporel : table N1(start) N2(end) 
%                    .PaP : PaP amplitude of each SSW

% SSW detector of Molle et al. Sleep,2011

% data from the input
fs   = signals.fs;
data = signals.data;

% registres
data_f_sw = zeros(size(data));
[Ntrials,~] = size(data);
% parametres du filtre (SSW)
wn = OPTIONS.Sleep.SSW.Molle.fmin_max / (fs/2);
ssw_filter = fir1(150,wn);
PaP_all = [];
N_SSW   = 0;
Duree   = 0;

% on passe en revue les trials:
for it = 1:Ntrials
    sig = data(it,:); % the signal
    Duree = Duree + length(sig) / fs / 60; % minutes
    % filtrage dans la bande SSW
    sigf = filtfilt(ssw_filter,1,sig);
    data_f_sw(it,:) = sigf; % ONLY FOR VISU (facultatif)
    % zero crossings (from positives to negatives) 
    f1 = (sigf(2:end).*sigf(1:end-1))<0;
    f2 = sigf(1:end-1)>0;
    n_zc = find(f1.*f2);
    % critere temporel
    delay = abs(n_zc(2:end)-n_zc(1:end-1)) / fs *1000; % msec
    duree_min = delay >= OPTIONS.Sleep.SSW.Molle.duree_min_max(1);
    duree_max = delay <= OPTIONS.Sleep.SSW.Molle.duree_min_max(2);
    duree_critere = find(duree_min .* duree_max);
    % we keep the markers here (zc for zero crossing)
    marker{it}.n_t(:,1) = n_zc(duree_critere)';
    marker{it}.n_t(:,2) = n_zc(duree_critere+1)';
    % amplitude PaP
    PaP = zeros(1,numel(n_zc)-1);
    PaPraw = zeros(1,numel(n_zc)-1);
    for i = 1:length(n_zc)-1
        segment = sigf(n_zc(i):n_zc(i+1));
        PaP(i)  = abs(max(segment)-min(segment));
        segment = sig(n_zc(i):n_zc(i+1));
        PaPraw(i)  = abs(max(segment)-min(segment));
    end
    marker{it}.PaP     = PaP(duree_critere);
    marker{it}.PaP_raw = PaPraw(duree_critere);
    PaP_all = cat(2,PaP_all,marker{it}.PaP);
end

signals.data_f_sw = data_f_sw;

% PERCENTILE (over all the trial we have here)
PaP_all = sort(PaP_all,'ascend');
n_thr   = ceil(OPTIONS.Sleep.SSW.Molle.percentile * numel(PaP_all) / 100);
PaP_thr = PaP_all(n_thr);

% we filter the markers here for keeping only those above the threshold:
for it = 1:Ntrials
    sub_thr            = find(marker{it}.PaP>PaP_thr);
    marker{it}.PaP     = marker{it}.PaP(sub_thr)';
    marker{it}.PaP_raw = marker{it}.PaP_raw(sub_thr)';
    marker{it}.n_t     = marker{it}.n_t(sub_thr,:);
    N_SSW              = N_SSW + numel(sub_thr);
end

fprintf('SSW Molle detector: we found\t %d over %d trials\t (%3.2f SSW/minute)\n',N_SSW,Ntrials,N_SSW/Duree);
end
    


