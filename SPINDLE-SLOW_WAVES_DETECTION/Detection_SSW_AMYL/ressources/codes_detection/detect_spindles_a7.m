function [marker,signals,N_spindle] = detect_spindles_a7(signals,OPTIONS)

% path to the Warby'a et al.  detector
addpath(genpath('./A7'));
% defaul parameters
initA7_DEF;
% A7 thresholds
    % Sigma based thresholds
    DEF_a7.absSigPow_Th = 1.25; % absSigPow threshold (sigma power log10 transformed)
    DEF_a7.relSigPow_Th = 1.6;  % relSigPow (z-score of relative sigma power from a clean 30 sec around the current window)
    % Correlation and covariance thresholds
    DEF_a7.sigCov_Th    = 1.3;  % sigmaCov (z-score of the covariance of sigma from a clean 30 sec around the current window)
    DEF_a7.sigCorr_Th   = 0.69; % sigmaCorr (correlation of sigma signal)
    
% Spindle definition
    DEF_a7.minDurSpindleSec = 0.3; % minimum duration of spindle
    DEF_a7.maxDurSpindleSec = 2.5; % maximum duration of spindle

% Slow ratio definition (Context Classifier)   
    % Slow ratio definition
    DEF_a7.lowFreqLow   = 0.5; % frequency band of delta + theta
    DEF_a7.lowFreqHigh  = 8;   % frequency band of delta + theta
    DEF_a7.highFreqLow  = 16;  % frequency band of beta
    DEF_a7.highFreqHigh = 30;  % frequency band of beta.
    % Slow ratio threshold
    DEF_a7.slowRat_Th   = 0.9; % slow ratio threshold for the spindle spectral context
    
% Sigma filter definition
    DEF_a7.sigmaFreqLow  = 11.0;   % sigma frequency band
    DEF_a7.sigmaFreqHigh = 16.0;   % sigma frequency band
    DEF_a7.fOrder        = 20.0;   % filter order for the sigma band
    
% Frequency band definitions
    DEF_a7.sigmaPSDFreqLow  = 11;  % frequency band of the sigma to be sure that the previous value of 11 Hz is taken
    DEF_a7.sigmaPSDFreqHigh = 16;  % frequency band of the sigma to be sure that the next value of 16 Hz is taken
    DEF_a7.totalFreqLow     = 4.5; % frequency band of the broad band
    DEF_a7.totalFreqHigh    = 30;  % frequency band of the broad band
    
% Sliding window definitions
    % A7 threshold window
    DEF_a7.absWindLength    = 0.3;  % window length in sample for absSigPow and sigmaCov   <---- ??
    DEF_a7.absWindStep      = 0.1;  % window step in sample for absSigPow and sigmaCov
    DEF_a7.relWindLength    = 0.3;  % window length in sec for sigmaCorr
    DEF_a7.relWindStep      = 0.1;  % window step in sec for sigmaCorr
    % PSA window
    DEF_a7.PSAWindLength    = 0.3;  % window length in sec for PSA
    DEF_a7.PSAZWindLength   = 2;    % window length with zero pad in sec for PSA <---- ?
    DEF_a7.PSAWindStep      = 0.1;  % window step in sec for PSA
    DEF_a7.BSLLengthSec     = 10;   % length of the baseline in sec  <---- we reduce it 
    % Spindle detection window
    DEF_a7.winLengthSec     = 0.3;  % window length in second
    DEF_a7.WinStepSec       = 0.1;  % window step in second
    DEF_a7.ZeroPadSec       = 2;    % zero padding length in second  
    DEF_a7.minSecBwtSpindle = 0.0;  % Minimum distance between spindle
    DEF_a7.bslLengthSec     = 10;   % baseline length to compute the z-score of rSigPow and sigmaCov <--- line 114 ??
    
% Parameter settings
    % Setting used in a7subAbsPowValues.m
    DEF_a7.eventNameAbsPowValue       = 'a7AbsPowValue'; % event name for warnings
    % Settings used in a7subRelSigPow.m
    DEF_a7.eventNameRelSigPow         = 'a7RelSigPow';  % event name for warnings
    DEF_a7.lowPerctRelSigPow          = 10;             % low percentile to compute the STD and median of both thresholds
    DEF_a7.highPerctRelSigPow         = 90;             % high percentile to compute the STD and median of both thresholds
    % 1 = On / 0 = Off
    DEF_a7.useLimPercRelSigPow        = 1;              % Consider only the baseline included in the percentile selected
    DEF_a7.useMedianPSAWindRelSigPow  = 0;              % To use the median instead of the mean to compute the threshold. 
    % Settings used in a7subSigmaCov.m
    DEF_a7.eventNameSigmaCov          = 'a7SigmaCov';   % event name for warnings
    DEF_a7.lowPerctSigmaCov           = 10;             % low percentile to compute the STD and median of both thresholds
    DEF_a7.highPerctSigmaCov          = 90;             % high percentile to compute the STD and median of both thresholds
    DEF_a7.filterOrderSigmaCov        = 20;             % Define the filter order
    % 1 = On / 0 = Off
    DEF_a7.useLimPercSigmaCov         = 1;              % Consider only the baseline included in the percentile selected
    DEF_a7.removeDeltaFromRawSigmaCov = 0;              % To filter out the delta signal from the raw signal to compute the covariance or the correlation
    DEF_a7.useMedianWindSigmaCov      = 0;              % On: Use the median to the bsl normlization, Off: Use the mean value
    DEF_a7.useLog10ValNoNegSigmaCov   = 1;              % On: Use log10 distribution (It is more similar to normal distribution)
    % Settings used in a7subSigmaCorr.m 
    DEF_a7.filterOrderSigCorr         = 20;             % The filter order
    % 1 = On / 0 = Off
    DEF_a7.removeDeltaFromRawSigCorr  = 0;              % On: Use the median to the bsl normlization, Off: Use the mean value

    % Settings used in a7subTurnOffDetSlowRatio.m
    DEF_a7.eventNameSlowRatio         = 'a7SlowRatio';  % event name for warnings
    % 1 = On / 0 = Off 
    DEF_a7.useMedianWindSlowRatio     = 0;              % On: Use the median to the bsl normlization, Off: Use the mean value
    DEF_a7.useLog10ValNoNegSlowRatio  = 1;              % On: Use log10 distribution (It is more similar to normal distribution)
    
    warning('off'); % not to be bothered by warnings...

% ====== Main loop (over the trials) ========

DEF_a7.standard_sampleRate = signals.fs;
N_spindle = 0;
marker = struct;

fs = signals.fs;
data = signals.data;
[Ntrials,~]  = size(data);
% parametres du filtre (SSW)
wn = OPTIONS.Sleep.SPL.fmin_max / (fs/2);
spl_filter = fir1(150,wn);
signals.data_f_sigma = zeros(size(data));
signals.data_f_sigma_Hilbert = zeros(size(data));

        for tr = 1:Ntrials
            
            eeg = data(tr,:);
            signals.data_f_sigma(tr,:) = filtfilt(spl_filter,1,eeg);
            sig_Hilbert = hilbert(signals.data_f_sigma(tr,:));
            signals.data_f_sigma_Hilbert(tr,:) = smooth(abs(sig_Hilbert),fs,OPTIONS);
            eeg = eeg' * 10^6; % A7 travaille en uV
            sleepStageVect = 2*ones(length(eeg),1); % on est en stade 2
            artifactVect   = zeros(length(eeg),1);  % on est artefact-free
            marker.delimiters{tr} = [];
            marker.frequence{tr}  = [];
            % appel du detecteur
               [detVect, ~ , ~ , outputFile] = ...
                    a7SpindleDetection(eeg, ...
                    sleepStageVect, ...
                    artifactVect, ...
                    DEF_a7);
             % stockage des markers:
             if sum(detVect)
                 Ns = size(outputFile,1)-1;
                 for k = 1:Ns
                        marker.delimiters{tr}(k,:) = [outputFile{1+k,1} outputFile{1+k,2}];
                        marker.frequence{tr}(k) = avg_freq_of_spindle(signals.data_f_sigma(tr,:),...
                            fs,...
                            marker.delimiters{tr}(k,1), ...
                            marker.delimiters{tr}(k,2));
                 end
                 N_spindle = N_spindle + Ns; 
             end
        end
        marker.spindle_thr = 0;
end
       


% a function that smooths the signal (sliding window)
function sig_out = smooth(sig_in,fs,OPTIONS)
tau = ceil((OPTIONS.Sleep.SPL.smoothing/2/1000)*fs);
n   = length(sig_in);
sig_out = zeros(1,n);
    for i=1:n, 
        sig_out(i) = mean(sig_in(i-min(tau,i-1):i+min(tau,n-i)));
    end
end

function freq = avg_freq_of_spindle(sig_Hilb,fs,debut,fin)
    segment = sig_Hilb(debut:fin);
    zeros_c = numel(find(segment(1:end-1).*segment(2:end)<0));
    duree   = (fin-debut)/fs;
    freq    = 0.5*zeros_c / duree;
end
    
    
    
    
    

