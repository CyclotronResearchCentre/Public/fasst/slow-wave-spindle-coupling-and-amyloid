
function [marker,signals,Stat_SSW] = detect_SSW_Carrier(signals,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% marker------------ .n_zc_critere_temporel : table N1(start) N2(end) 
%                    .PaP : PaP amplitude of each SSW

    % CRITERES (Carrier et al. EJN, 2011)
    % critere temporel
    % il doit porter sur la partie < 0 et sur la partie > de l'onde
    % separement 
    % on separe la partie negative de la partie positive:
    % la duree de la partie < 0 doit etre entre 125 et 1500 msec
    % la duree de la partie > 0 doit etre < 1000 msec
    % l'amplitude PaP > 75 uV
    % l'amplitude Neg > 40 uV
    th_PaP  = OPTIONS.Sleep.SSW.Carrier.PaP;  % uV
    th_Neg  = OPTIONS.Sleep.SSW.Carrier.Neg;  % uV
    min_tNe = OPTIONS.Sleep.SSW.Carrier.duree_min_max_Neg(1);
    max_tNe = OPTIONS.Sleep.SSW.Carrier.duree_min_max_Neg(2);
    max_tPo = OPTIONS.Sleep.SSW.Carrier.duree_max_Pos;

% Nouveau: le sexe (old)
if isfield(signals,'old_sex')
    if strcmpi(num2str(signals.old_sex),'0')||strcmpi(signals.old_sex,'f')
        th_PaP =  70;
        th_Neg =  37;
    elseif strcmpi(num2str(signals.old_sex),'1')||strcmpi(signals.old_sex,'m')
        th_PaP =  60.5;
        th_Neg =  32;
    end
end
    
fs = signals.fs;
data = signals.data;
data_f_sw = zeros(size(data));
[Ntrials,~] = size(data);
% parametres du filtre (SSW)
wn = OPTIONS.Sleep.SSW.Carrier.fmin_max / (fs/2);
% filtre FIR : l'ordre pourrait avoir un role a jouer dans la densite des
% OL detectees.
ssw_filter = fir1(500,wn);

N_SSW   = 0;
Duree = 0;

% on passe en revue les trials:
for it = 1:Ntrials
    sig = double(data(it,:));
    Duree = Duree + length(sig) / fs / 60; % minutes
    % filtrage dans la bande SSW
    sigf = filtfilt(ssw_filter,1,sig);
    data_f_sw(it,:) = sigf; % ONLY FOR VISU (facultatif)
    % zero crossings (from positives to negatives) 
    f1 = (sigf(2:end).*sigf(1:end-1))<0;
    f2 = sigf(1:end-1)>0;
    n_zc = find(f1.*f2);
    
    % amplitude PaP and other properties
    n_t = zeros(numel(n_zc)-1,2);
    PaP = zeros(1,numel(n_zc)-1);
    Neg = zeros(1,numel(n_zc)-1);
    tNe = zeros(1,numel(n_zc)-1);
    tPo = zeros(1,numel(n_zc)-1);
    PaP_raw = zeros(1,numel(n_zc)-1);
    Neg_raw = zeros(1,numel(n_zc)-1);
    keep = [];
    
    for i = 1:length(n_zc)-1
        n_t(i,:) = [n_zc(i),n_zc(i+1)];
        segment = sigf(n_zc(i)+1:n_zc(i+1)-1); % on exclut les bords
        segNeg  = find(segment<0);
        segPos  = find(segment>=0);
        PaP(i)  = abs(max(segment)-min(segment))*10^6; % in microV
        Neg(i)  = max(abs(segment(segNeg)))*10^6;
        tNe(i)  = (length(segNeg)-1) / fs * 1000; % msec
        tPo(i)  = (length(segPos)-1) / fs * 1000; % msec
        % frequence de transition:
        [~,u]   = min(segment);
        [~,v]   = max(segment);
        tfr(i)  = fs/(v-u)/2;
        mfr(i)  = fs/(n_zc(i+1)-n_zc(i));
        
        % raw data:
        segment = sig(n_zc(i)+1:n_zc(i+1)-1); % on exclut les bords
        PaP_raw(i) = abs(max(segment)-min(segment))*10^6; % in microV
        Neg_raw(i) = max(abs(segment(segNeg)))*10^6;
        
        % Carrier Criterion:
        if      (PaP(i) > th_PaP)  && ...
                (Neg(i) > th_Neg)  && ...
                (tNe(i) > min_tNe) && ...
                (tNe(i) < max_tNe) && ...
                (tPo(i) < max_tPo)
            keep = cat(2,keep,i);
        end
        
    end
     marker{it}.Thresholds_PaP_Neg = [th_PaP th_Neg]; 
     marker{it}.n_t = n_t(keep,:);
     marker{it}.PaP = PaP(keep);
     marker{it}.Neg = Neg(keep);
     marker{it}.tNe = tNe(keep);
     marker{it}.tPo = tPo(keep);
     marker{it}.PaP_raw = PaP_raw(keep);
     marker{it}.Neg_raw = Neg_raw(keep);
     marker{it}.mfr = mfr(keep);
     marker{it}.tfr = tfr(keep);

     N_SSW = N_SSW + numel(keep);
end

Stat_SSW.N_SSW = N_SSW;
Stat_SSW.d_SSW = N_SSW/Duree;
Stat_SSW.duree = Duree;

signals.data_f_sw = data_f_sw;
mess = sprintf(' SSW Carrier detector(%3.2f-%3.2f Hz): we found\t%d over %d trials\t (%3.2f SSW/minute)\n',OPTIONS.Sleep.SSW.Carrier.fmin_max(1),OPTIONS.Sleep.SSW.Carrier.fmin_max(2),N_SSW,Ntrials,N_SSW/Duree);
fprintf(mess);
end
    


