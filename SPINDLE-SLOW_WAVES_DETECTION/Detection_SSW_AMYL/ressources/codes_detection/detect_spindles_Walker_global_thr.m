function [marker,signals,N_spindle] = detect_spindles_Walker_global_thr(signals,OPTIONS,nom)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% marker------------ .delimiters : table N1(start) N2(end) 

fprintf('\nSpindle detection on %s: ',nom);
fs = signals.fs;
data = signals.data;
data_f_spind = zeros(size(data));
data_f_sp_ah = zeros(size(data));
[Ntrials,~] = size(data);
% parametres du filtre (SSW)
wn = OPTIONS.Sleep.SPL.fmin_max / (fs/2);
spl_filter = fir1(150,wn);
N_spindle  = 0;
amplitude_all = [];
fprintf('%d trials\n',Ntrials);

% Critere de duree (en echantillons):
duree_min = ceil(OPTIONS.Sleep.SPL.duree_min_max(1)/1000*signals.fs);
duree_max = ceil(OPTIONS.Sleep.SPL.duree_min_max(2)/1000*signals.fs);

% on passe en revue les trials:
for it = 1:Ntrials
    if mod(it,50)==0, fprintf('\n'); end
    if mod(it,5)==0, fprintf('%d%%, ',ceil(it/Ntrials * 100)); end
    sig = double(data(it,:)); 
    % filtrage dans la bande sigma
    sigf = filtfilt(spl_filter,1,sig);
    data_f_spind(it,:) = sigf;
    sigh = hilbert(sigf);
    siga = smooth(abs(sigh),fs,OPTIONS);
    data_f_sp_ah(it,:) = siga;
    data_f_sp_an(it,:) = sigh;
    amplitude_all = cat(1,amplitude_all,siga);
end
fprintf('\nmaintenant recherche des fuseaux (criteres de duree).\n');

    % PERCENTILE (over all the trial we have here)
    siga_s = sort(reshape(amplitude_all,1,numel(amplitude_all)),'ascend');
    n_thr   = ceil(OPTIONS.Sleep.SPL.percentile * numel(siga_s) / 100);
    marker.spindle_thr = siga_s(n_thr);
    clear('siga_s','amplitude_all');
    
    for it = 1:Ntrials
        
        sig =  data_f_sp_ah(it,:);
        thr = marker.spindle_thr;
        uu = sig>=thr;
        vv = diff(uu);
        deb_s = find(vv==1);
        fin_s = find(vv==-1);
        
        marker.delimiters{it}  = [];
        marker.frequence{it}   = [];
        marker.maxPower{it}    = [];
        marker.signal_anal{it} = {};
        
        % Criteres temporels
        if numel(deb_s)&&(numel(fin_s))
%             deb_s = deb_s(deb_s<fin_s(end));
%             fin_s = fin_s(fin_s>deb_s(1));
%         
%             marker.delimiters{it}  = [deb_s ; fin_s]';
            % les lignes precedentes peuvent poser un probleme si on a des
            % effets de bord...
            deb_s = deb_s(deb_s<fin_s(end));
            if ~isempty(deb_s)
                fin_s = fin_s(fin_s>deb_s(1));
            else
                fin_s = [];
            end
        
            marker.delimiters{it}  = [deb_s ; fin_s]';



        
            if ~isempty(marker.delimiters{it})
            durees = marker.delimiters{it}(:,2)-marker.delimiters{it}(:,1);
            liste  = find((durees>duree_min)&(durees<duree_max));
            marker.delimiters{it} = marker.delimiters{it}(liste,:);
            Ns = numel(liste);
                 for k = 1:Ns
                 marker.frequence{it}(k) = avg_freq_of_spindle(...
                            data_f_spind(it,:),...
                            fs,...
                            marker.delimiters{it}(k,1), ...
                            marker.delimiters{it}(k,2));
                 marker.maxPower{it}(k,:) = maxPowerSpdl(...
                            data_f_sp_ah(it,:),...
                            marker.delimiters{it}(k,1), ...
                            marker.delimiters{it}(k,2)); 
                 marker.signal_anal{it}{k} = data_f_sp_an(it,...
                            marker.delimiters{it}(k,1): ...
                            marker.delimiters{it}(k,2));
                 end
            N_spindle = N_spindle + Ns;
            end
        end
    end
    
     fprintf('\n--> %d spindles found (%d trials)\n',N_spindle,Ntrials);
     signals.data_f_sigma         = data_f_spind;
     signals.data_f_sigma_Hilbert = data_f_sp_ah;  
end
    
    


% ======================================================================

% a function that smooths the signal (sliding window)
function sig_out = smooth(sig_in,fs,OPTIONS)
tau = ceil((OPTIONS.Sleep.SPL.smoothing/2/1000)*fs);
n   = length(sig_in);
sig_out = zeros(1,n);
    for i=1:n, 
        sig_out(i) = mean(sig_in(i-min(tau,i-1):i+min(tau,n-i)));
    end
end


function freq = avg_freq_of_spindle(sig_Hilb,fs,debut,fin)
    segment = sig_Hilb(debut:fin);
    zeros_c = numel(find(segment(1:end-1).*segment(2:end)<0));
    duree   = (fin-debut)/fs;
    freq    = 0.5*zeros_c / duree;
end

function pow = maxPowerSpdl(amp_Hilb,debut,fin)
    segment = amp_Hilb(debut:fin);
    [maximum,when] = max(segment);
    pow(1) = debut+when-1;
    pow(2) = maximum;
end