


addpath(genpath('./ressources'));
%function script_principal_Detection()
addpath(genpath('./set_up'));
clear
clc
% Chemin
% addpath(genpath('./set_up'));
% addpath(genpath('../codes/codes_detection'));
% addpath(genpath('../codes/CircStat'));
% addpath(genpath('../codes/codes_stat'));

% ====== THE DataBASE (either we build it or we provide it)
fprintf('\n\n ========= SSW - Spindles project (%s) ========= \n\n',date);
fprintf('\tlook in Amyloide_Qu-Be the directory of the matlab data\n');
fprintf('\tfiles (extracted from the original database)\n');

dd = uigetdir('../');
if ~ischar(dd), disp('== Bye!'); return; end
% pour Windows:
% repertoire lu
fsep = filesep;
uu = strsplit(dd,fsep);
fprintf('\n\tRepertoire lu: %s\n',uu{end});
% ============

% creation des OPTIONS de base: la liste des sujets et les donnees de base
% utilisee. Aussi inventaire

    O = set_up_local_cognap(dd);
    O = set_up_general_new(O);

    Dpathmath = dir([O.Results.out_dbase fsep 'D*']);
    if ~isempty(Dpathmath)
        O = set_up_Detector(O);
        fprintf('\n --- We read files in\n     %s\n',O.Results.out_dbase);
    else
        O = set_up_Detector(O);
        set_up_Data(O);
        Dpathmath = dir([O.Results.out_dbase fsep 'D*']);
    end
    dgr = dir([O.Results.out_dbase filesep 'Dgr*']);    
    
    
    for i = 1:numel(dgr) % sujets
        load([O.Results.out_dbase filesep dgr(i).name],'TR');
        fprintf('\n     %s:\n',TR.name);
        MARKERS = {};
        for j = 1:numel(TR.D) % condition
            for k = 1:numel(TR.D{j}) % electrode               
            [marker,signals,stat_SSW] = detect_SSW(TR.D{j}{k},O);
            MARKERS{k}{j}.markers = marker;
            MARKERS{k}{j}.signals = signals;
            MARKERS{k}{j}.electro = signals.electrode;
            MARKERS{k}{j}.stage   = O.DBase.Conditions{j};
            MARKERS{k}{j}.identif = O.DBase.listes{1}{i};
            MARKERS{k}{j}.densite = stat_SSW.d_SSW;
            MARKERS{k}{j}.duree   = stat_SSW.duree;
            end
        end
        SSW.name_Id = TR.name;
        SSW.markers = MARKERS;
        save([O.Results.out_dir '/DBaseMatlab/SW_' TR.name],'SSW','O'); 
    end
    
    
    clear('SSW');
    % Ecriture du fichier Densites_OL.txt ---------------------------------
    
    fprintf('\n --- Ecriture de Densites_OL.txt ... ')
    fId = fopen([O.Results.out_dir '/Densites_OL.txt'],'w');
    fprintf(fId,'Densites OL/mn: %s (PaP = %3.1f muV et Neg = %3.1f muV)\n',...
        O.Sleep.SSW.method, ...
        O.Sleep.SSW.Carrier.PaP, ...
        O.Sleep.SSW.Carrier.Neg);
    ligne1 = sprintf('Id_sujet, Stade (N2,N3 et N2+N3)');
    
    % Inventaire des SW
    ddg = dir([O.Results.out_dir fsep 'DBaseMatlab' fsep 'SW_*']);
    
         for k = 1:numel(ddg)
         load([O.Results.out_dir filesep 'DBaseMatlab' filesep ddg(k).name]);
             if k==1, for r = 1:numel(SSW.markers)
                         ligne1 = [ligne1 ', ' SSW.markers{r}{1}.electro];
                      end
             fprintf(fId,'%s\n',ligne1);
             end
%             % SSW.markers{electrode}{N2/N3}
                 nombre_total   = zeros(1,numel(SSW.markers));
                 duree_totale   = zeros(1,numel(SSW.markers));
                 for j = 1:numel(O.DBase.Conditions) % condition
                     fprintf(fId,'\n%s, %s', SSW.name_Id, SSW.markers{1}{j}.stage);
                     for r = 1:numel(SSW.markers)
                         fprintf(fId,', %3.2f',SSW.markers{r}{j}.densite);
                         nombre_total(r) = nombre_total(r) +  SSW.markers{r}{j}.densite * SSW.markers{r}{j}.duree;
                         duree_totale(r) = duree_totale(r) +  SSW.markers{r}{j}.duree;
                     end
                 end
                 fprintf(fId,'\n%s, All', SSW.name_Id);
                 for r = 1:numel(SSW.markers)
                         fprintf(fId,', %3.2f',nombre_total(r)/duree_totale(r));
                 end
         end
    fclose(fId);
    fprintf(' done. Bye\n\n');
    
    