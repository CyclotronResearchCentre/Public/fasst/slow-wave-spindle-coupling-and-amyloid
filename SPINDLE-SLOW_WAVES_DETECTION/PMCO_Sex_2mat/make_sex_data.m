
%clear; clc

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On range les donnees (epoques) en foncton du sexe des individus
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% recherche de la base de donnees
dtb_mat = ['..' filesep 'PMCO_2mat' filesep];

% les fichiers
file_mat = dir([dtb_mat 'PM*.mat']);   % 'PMCOF002_TEST_140616_T232800_20160615T003032-2.mat';
n_file = numel(file_mat);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('--- On a trouve %d donnees mat\n',n_file);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On classe les sujets par sexe
% Lecture des donnees demographiques: (1 == homme)
load(['.' filesep 'COF_DEMOGRAPHICS.mat']);

HOMMES.Liste = {}; n_H = 1;
FEMMES.Liste = {}; n_F = 1;
for i = 1:size(COFDEMOGRAPHICS,1)
    index = '000';
    iiiii = num2str(COFDEMOGRAPHICS(i,1)); o = numel(iiiii)-1;
    index(end-o:end) = iiiii;
    if COFDEMOGRAPHICS(i,2) == 1
        HOMMES.Liste{n_H} = index;
        HOMMES.Age(n_H) = COFDEMOGRAPHICS(i,3); 
        n_H = n_H +1;
    elseif COFDEMOGRAPHICS(i,2) == 2
        FEMMES.Liste{n_F} = index;
        FEMMES.Age(n_F) = COFDEMOGRAPHICS(i,3);
        n_F = n_F +1;
    end
end
fprintf('--- On a trouve %d HOMMES\n',numel(HOMMES.Liste));
fprintf('--- On a trouve %d FEMMES\n',numel(FEMMES.Liste));

if exist(['.' filesep 'PMCO_H_2mat'],'dir'); rmdir(['.' filesep 'PMCO_H_2mat'],'s'); end
   repH = ['.' filesep 'PMCO_H_2mat']; mkdir(repH);
   fprintf('\n--- On definit le repertoire ''H'': %s',repH);
   
if exist(['.' filesep 'PMCO_F_2mat'],'dir'); rmdir(['.' filesep 'PMCO_F_2mat'],'s'); end
   repF = ['.' filesep 'PMCO_F_2mat']; mkdir(repF);
   fprintf('\n--- On definit le repertoire ''F'': %s',repF);
   
   % ==== Hommes
   fid = fopen([repH filesep date],'w'); fclose(fid);
   for i = 1:numel(HOMMES.Liste)
       uu = dir([dtb_mat 'PMCOF' HOMMES.Liste{i} '*.mat']);
       if numel(uu)
           for j = 1:numel(uu)
               copyfile([dtb_mat uu(j).name],repH);
           end
       end
   end
               
   % ==== Femmes
   fid = fopen([repF filesep date],'w'); fclose(fid);
   for i = 1:numel(FEMMES.Liste)
       uu = dir([dtb_mat 'PMCOF' FEMMES.Liste{i} '*.mat']);
       if numel(uu)
           for j = 1:numel(uu)
               copyfile([dtb_mat uu(j).name],repF);
           end
       end
   end

fprintf('\n--> sauvegarde terminee\n');

