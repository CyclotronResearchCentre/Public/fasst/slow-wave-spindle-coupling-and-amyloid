function script_Analyses_inventaire_04()
addpath('./ressources')

fprintf('\n\n\n =========== Slow Wave and Switchers Analyses -03- =========== \n\t\t(Statistiques des OL)\n\t\t(On cherche un repertoire dans Results)\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
repertoire_in = uigetdir(['.' filesep]);
AA = strsplit(repertoire_in,'_'); AA = AA{end};
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n\nOn regarde: \n%s\n',repertoire_in);
pause(0.3);
repertoire_in = [repertoire_in filesep 'DBaseMatlab' filesep];

f_sw = dir([repertoire_in filesep '*.mat']);
if numel(f_sw), load([repertoire_in f_sw(1).name],'O'); else disp('probleme, bye.'); return; end
% ============================================================
% CHARGEMENT DES DONNEES
%   O.DBase.listes
%   SSW.markers{ele}{stade}.markers{epoque}
%   creation de l'inventaire
repertoire_out = ['.' filesep 'Inventaire_AMYL2020' filesep 'Inventaire_' AA filesep];
if ~exist(repertoire_out,'dir'), mkdir(repertoire_out); end;
% ============================================================
% fichiers de sorties
    elec = O.DBase.channel;
    std = 'N2N3';
    grp = O.DBase.groups;
    for i_gr = 1:numel(grp)
        for i_ele = 1:numel(elec) 
            csv_fic_OnLe{i_gr}{i_ele} = sprintf('%s%s_OnLe_%s_%s.csv',repertoire_out,std,grp{i_gr},elec{i_ele}); 
            csv_fic_fast{i_gr}{i_ele} = sprintf('%s%s_Fast_%s_%s.csv',repertoire_out,std,grp{i_gr},elec{i_ele});
            csv_fic_slow{i_gr}{i_ele} = sprintf('%s%s_Slow_%s_%s.csv',repertoire_out,std,grp{i_gr},elec{i_ele});
        end
    end
    
    
% ========== 
% REGROUPEMENT D'ELCTRODES
% FRONTAL  ==> GR_ELE == 1;
% CENTRAL  ==> GR_ELE == 2;
% PARIETAL ==> GR_ELE == 3;
GR_ELE = {'Frontal' 'Central' 'Parietal'};
for i_ele = 1:numel(elec)
    if strcmpi(elec{i_ele}(1),'F'),     ELE_ELE(i_ele) = 1;
    elseif strcmpi(elec{i_ele}(1),'C'), ELE_ELE(i_ele) = 2;
    elseif strcmpi(elec{i_ele}(1),'P'), ELE_ELE(i_ele) = 3;
    end   
end

    for i_gr = 1:numel(grp)
        for i_reg = 1:numel(GR_ELE) 
            csv_reg_OnLe{i_gr}{i_reg} = sprintf('%s%s_OnLe_%s_%s.csv',repertoire_out,std,grp{i_gr},GR_ELE{i_reg}); 
            csv_reg_fast{i_gr}{i_reg} = sprintf('%s%s_Fast_%s_%s.csv',repertoire_out,std,grp{i_gr},GR_ELE{i_reg});
            csv_reg_slow{i_gr}{i_reg} = sprintf('%s%s_Slow_%s_%s.csv',repertoire_out,std,grp{i_gr},GR_ELE{i_reg});
        end
    end


% ============================================================
% ============================================================
% ============================================================


for i_gr = 1:numel(O.DBase.groups)
    SUJ = [];
    gr_name = O.DBase.groups{i_gr};
    gr_list = O.DBase.listes{i_gr};
    gr_nbr  = numel(O.DBase.listes{i_gr});


    for i_ele = 1:numel(elec)
        LIGNE_TABLE_DENSITE{i_ele} = [];
        LIGNE_TABLE_SLOW{i_ele}    = [];
        LIGNE_TABLE_FAST{i_ele}    = [];
    end
    for i_reg = 1:numel(GR_ELE)
        TABLE_DENSITE{i_reg} = [];
        TABLE_SLOW{i_reg}    = [];
        TABLE_FAST{i_reg}    = [];
    end
    
    

    All_fr_transition  = [];
    
    Liste_sujets_inventaire = '';
    
     i_suj = 1; k_suj = 1;
    while k_suj <= gr_nbr
        fprintf('\n %s: %s',gr_name,gr_list{k_suj});
        ff_sw = dir([repertoire_in 'SW*' gr_list{k_suj} '*.mat']);
        if ~numel(ff_sw), fprintf(' NOT FOUND'); else
            fprintf(' found');
            load([repertoire_in ff_sw(1).name],'SSW');
            if ~isempty(SSW.markers)
                Liste_sujets_inventaire = sprintf('%s \n %s',Liste_sujets_inventaire,gr_list{k_suj});
                for i_reg = 1:numel(GR_ELE), LITTLE_TABLE_DENSITE{i_reg} = []; end
                for i_ele = 1:numel(elec)
                    % On cumule N2 et N3
                    markers = SSW.markers{i_ele};
                    [All{i_ele}, ligne_table] = inventaire_ssw(markers);
                    LIGNE_TABLE_DENSITE{i_ele} = cat(1,LIGNE_TABLE_DENSITE{i_ele},ligne_table);
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % les frequences de transition: on cumule a travers tous
                    % les sujets de la cohorte
                    All_fr_transition = cat(1,All_fr_transition,All{i_ele}.trans_freq');
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    % On gere ici le regroupement:
                    i_reg = ELE_ELE(i_ele);
                    LITTLE_TABLE_DENSITE{i_reg} = cat(1,LITTLE_TABLE_DENSITE{i_reg},ligne_table);
                end
                for i_reg = 1:numel(GR_ELE)
                    TABLE_DENSITE{i_reg} = cat(1,TABLE_DENSITE{i_reg},...
                                                 calcul_regroupement_electrode_densite(LITTLE_TABLE_DENSITE{i_reg})); 
                end
                
                
                

            SUJ(i_suj).group = gr_name;
            SUJ(i_suj).name  = gr_list{i_suj};
            SUJ(i_suj).All   = All;
            i_suj = i_suj+1; k_suj = k_suj+1;
            
            else fprintf(' but empty.'); k_suj = k_suj+1;
            end
        end
    end % END sur les sujets
    
    % RECHERCHE DE LA FREQUENCE DE SEPARATION SLOW-FAST
     f_sep = frequence_separation_slow_fast(All_fr_transition);
    % RECHERCHE DES SWITCHERS:
    for i_suj = 1:numel(SUJ)%gr_nbr
        for i_reg = 1:numel(GR_ELE), LITTLE_TABLE_SLOW{i_reg} = []; end
        for i_reg = 1:numel(GR_ELE), LITTLE_TABLE_FAST{i_reg} = []; end
      [newSUJ(i_suj), ligne_table_slow, ligne_table_fast] = inventaire_switchers(SUJ(i_suj),f_sep);
        for i_ele = 1:numel(elec)
            LIGNE_TABLE_SLOW{i_ele} = cat(1,LIGNE_TABLE_SLOW{i_ele},ligne_table_slow{i_ele});
            LIGNE_TABLE_FAST{i_ele} = cat(1,LIGNE_TABLE_FAST{i_ele},ligne_table_fast{i_ele});
            
            % On gere ici le regroupement:
            i_reg = ELE_ELE(i_ele);
            LITTLE_TABLE_SLOW{i_reg} = cat(1,LITTLE_TABLE_SLOW{i_reg},ligne_table_slow{i_ele});
            LITTLE_TABLE_FAST{i_reg} = cat(1,LITTLE_TABLE_FAST{i_reg},ligne_table_fast{i_ele});
            
        end
        
       for i_reg = 1:numel(GR_ELE)
       	TABLE_SLOW{i_reg} = cat(1,TABLE_SLOW{i_reg},...
                               calcul_regroupement_electrode_switchers(LITTLE_TABLE_SLOW{i_reg}));
        TABLE_FAST{i_reg} = cat(1,TABLE_FAST{i_reg},...
                               calcul_regroupement_electrode_switchers(LITTLE_TABLE_FAST{i_reg})); 
       end
        
    end
    
    % ecriture des tables
    for i_ele = 1:numel(elec)
            csvwrite(csv_fic_OnLe{i_gr}{i_ele},LIGNE_TABLE_DENSITE{i_ele});
            csvwrite(csv_fic_fast{i_gr}{i_ele},LIGNE_TABLE_FAST{i_ele});
            csvwrite(csv_fic_slow{i_gr}{i_ele},LIGNE_TABLE_SLOW{i_ele});
    end
    % tables par regroupement:
    for i_reg = 1:numel(GR_ELE)    
            csvwrite(csv_reg_OnLe{i_gr}{i_reg},TABLE_DENSITE{i_reg});
            csvwrite(csv_reg_fast{i_gr}{i_reg},TABLE_FAST{i_reg});
            csvwrite(csv_reg_slow{i_gr}{i_reg},TABLE_SLOW{i_reg});
    end
    
    % ecriture des listes
    fic_liste = sprintf('%s %s _liste_sujets.txt',repertoire_out,gr_name);
    fid = fopen(fic_liste,'w');
    fprintf(fid,'%s',Liste_sujets_inventaire);
    fclose(fid);
    
%end

            
    fprintf('\n\n VARIABLES ECRITES DANS FICHIERS OnLe:\n');
    fprintf('\t nombre d''epoques\n');
    fprintf('\t nombre d''OL\n');
    fprintf('\t densite (/mn)\n');
    fprintf('\t Ampl PaP (moyenne)\n');
    fprintf('\t Ampl PaP (std var.)\n');
    fprintf('\t freq. moyenne (moyenne)\n');
    fprintf('\t freq. moyenne (std var.)\n');
    fprintf('\t freq. transition (moyenne)\n');
    fprintf('\t freq. transition (std var.)\n');
    fprintf('\t Duree Down (moyenne)\n');
    fprintf('\t Duree Down (std var.)\n');
    fprintf('\t Duree Up (moyenne)\n');
    fprintf('\t Duree Up (std var.)\n');
    
    fprintf('\n\n VARIABLES ECRITES DANS FICHIERS Slow (ou Fast):\n');
    fprintf('\t nombre d''epoques\n');
    fprintf('\t nombre de switchers\n');
    fprintf('\t pourcentage d''OL (switchers)  <---- Attention\n');
    fprintf('\t Ampl PaP (moyenne)\n');
    fprintf('\t Ampl PaP (std var.)\n');
    fprintf('\t freq. moyenne (moyenne)\n');
    fprintf('\t freq. moyenne (std var.)\n');
    fprintf('\t freq. transition (moyenne)\n');
    fprintf('\t freq. transition (std var.)\n');
    
    
end
    
    
    
fprintf('\n\n -- Fin. Bye.\n\n')
end


function ligne_table = calcul_regroupement_electrode_densite(table_densite)
n = size(table_densite,1);
    % le nombre d'epoque (identique)
    ligne_table(1) = table_densite(1,1);
    % le nombre d'OL: moyenne 
    ligne_table(2) = mean(table_densite(:,2));
    % densite d'OL: moyenne
    ligne_table(3) = mean(table_densite(:,3));
    % Amplitude moyenne: moyenne sur les trois?
    ligne_table(4) = sum(table_densite(:,4).*table_densite(:,2))/sum(table_densite(:,2));
    % sigma Amplitude: Sum( (n_i-1)*sigma_i^2) / (sum(n_i) - 2) ?
    ligne_table(5) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,5).^2)/(sum(table_densite(:,2)) - 2));
    % freq moyenne moyenne: moyenne ou total?
    ligne_table(6) = sum(table_densite(:,6).*table_densite(:,2))/sum(table_densite(:,2));
    % freq moyenne (sigma): moyenne ou total?
    ligne_table(7) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,7).^2)/(sum(table_densite(:,2)) - 2));
    % freq transition (moyenne): moyenne ou total?
    ligne_table(8) = sum(table_densite(:,8).*table_densite(:,2))/sum(table_densite(:,2));
    % freq moyenne moyenne: moyenne ou total?
    ligne_table(9) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,9).^2)/(sum(table_densite(:,2)) - 2));
    % freq moyenne (sigma): moyenne ou total?
    ligne_table(10) = sum(table_densite(:,10).*table_densite(:,2))/sum(table_densite(:,2));
    % freq transition (moyenne): moyenne ou total?
    ligne_table(11) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,11).^2)/(sum(table_densite(:,2)) - 2));
    % freq moyenne moyenne: moyenne ou total?
    ligne_table(12) = sum(table_densite(:,12).*table_densite(:,2))/sum(table_densite(:,2));
    % freq moyenne (sigma): moyenne ou total?
    ligne_table(13) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,13).^2)/(sum(table_densite(:,2)) - 2));

end



function ligne_table = calcul_regroupement_electrode_switchers(table_densite)
n = size(table_densite,1);
    % le nombre d'epoque (identique)
    ligne_table(1) = table_densite(1,1);
    % le nombre d'OL: moyenne 
    ligne_table(2) = mean(table_densite(:,2));
    % densite d'OL: moyenne
    ligne_table(3) = sum(table_densite(:,2)) / sum(table_densite(:,2)./table_densite(:,3));
    % Amplitude moyenne: moyenne sur les trois?
    ligne_table(4) = sum(table_densite(:,4).*table_densite(:,2))/sum(table_densite(:,2));
    % sigma Amplitude: Sum( (n_i-1)*sigma_i^2) / (sum(n_i) - 2) ?
    ligne_table(5) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,5).^2)/(sum(table_densite(:,2)) - 2));
    % freq moyenne moyenne: moyenne ou total?
    ligne_table(6) = sum(table_densite(:,6).*table_densite(:,2))/sum(table_densite(:,2));
    % freq moyenne (sigma): moyenne ou total?
    ligne_table(7) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,7).^2)/(sum(table_densite(:,2)) - 2));
    % freq transition (moyenne): moyenne ou total?
    ligne_table(8) = sum(table_densite(:,8).*table_densite(:,2))/sum(table_densite(:,2));
    % freq moyenne moyenne: moyenne ou total?
    ligne_table(9) = sqrt(sum((table_densite(:,2)-ones(n,1)).*table_densite(:,9).^2)/(sum(table_densite(:,2)) - 2));

end
