function Analyses_inventaire_SSW04(dd)
% clear
% clc

addpath('./ressources');

fprintf('\n\n\n ====== Slow Wave Analyses 001 -v.03- ====== %s \n\t\t(Statistiques des OL)\n',date);
fprintf('\n\n On cherche un repertoire d''une cohorte ''_mat'': Ce repertoire devra \n');
fprintf(' contenir un repertoire DBase_SSW (obtenu par le detection Slow Wave)\n');
% dd = uigetdir('.');

ddSSW = [dd filesep 'DBase_SSW'];
if ~isfolder(ddSSW), fprintf('-- !!! La cohorte ne contient pas le repertoire d''OL (DBase_SSW)...\n Bye!\n'); return; end

ddRes = [dd filesep 'Results'];
if ~isfolder(ddRes), fprintf(' -- On cree le repertoire Results\n');mkdir(ddRes); end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
uu = dir([ddSSW filesep 'SW*.mat']);                         %                    
Ns = numel(uu);                                              %
fprintf(' -- we found %d subject in this cohort\n',Ns);      %
% Each file will contain SSW.markers{ele}{stade}.markers{ep} %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Recherche d'un fichier de sujets (facultatif) %%%%%%%%%%%%%%
% On lit les sujets dans l'ordre indique par ce              %
% fichier. Si absent, on prend l'ordre arbitraire            %
% indique dans OPTION.DBase.listes.sujets                    %
 [LISTEO,LISTE,LISTE_file] = construire_LISTE(dd,uu);        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for i = 1:numel(LISTE_file)
    load([ddSSW filesep LISTE_file{i}]);
    fprintf(' ---> %s: ',SSW.name_Id);
    O.DBase.channels = O.DBase.channel; % <--- PATCH
    
    % sequence d'electrodes: %%%%%%%%%%%%%%%%%%%%%
    Nele = numel(SSW.markers);                   %
    for j = 1:Nele                               %
        electrode{j} = SSW.markers{j}{1}.electro;%
        fprintf('%s ',electrode{j});             %
    end; fprintf('\n');                          %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    fprintf('      on dispose des stades: ');    %
    Nstage = numel(SSW.markers{1});              %
    for j = 1:Nstage                             %
        stage = SSW.markers{1}{j}.stage;         %
        fprintf('%s ',stage);                    %
    end; fprintf('\n');                          %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    
    % STADE par STADE, ELECTRODE PAR ELECTRODE %%%%%%%%%%%%%%%%%%%%%
    % on collecte les parametres suivants des OL:                  %
    % [identif P2P NEG frMOY fTRA dureeDOWN dureeUP]               %
    % a travers la cohorte                                         %
    
    % SSW.markers{1}{2}: 
    %     markers: {1x76 cell}
    %     signals: [1x1 struct]
    %     electro: 'f3'
    %       stage: 'Stade3'
    %     identif: [1 1]
    %     densite: 17.5789
    %       duree: 38

    % markers{trial}: 
    %     Thresholds_PaP_Neg: [61 33]
    %                    n_t: [5x2 double]
    %                    PaP: [79.2222 104.4761 ...]
    %                    Neg: [51.6069 51.9487 ....]
    %                    tNe: [339.8438 320.3125 ...]
    %                    tPo: [562.5000 453.1250 ...]
    %                PaP_raw: [118.7822 121.4741 ...]
    %                Neg_raw: [63.2122 60.5202 ...]
    %                    mfr: [1.0940 1.2736 ...]
    %                    tfr: [1.2190 2.0984 ...]

    
    
    if i==1  % initialisation : 
    for ele = 1:Nele
        for stg = 1:Nstage
            INVENTAIRE{ele}{stg} = [];
        end
    end
    end
    for ele = 1:Nele
        for stg = 1:Nstage
            markers = SSW.markers{ele}{stg}.markers;
            nbepocs = numel(markers);
            densSSW = SSW.markers{ele}{stg}.densite;
            for epo = 1:numel(markers)
                if numel(markers{epo}.PaP)
                    for j = 1:numel(markers{epo}.PaP)
                        INVENTAIRE{ele}{stg} = ...
                            cat(1, INVENTAIRE{ele}{stg}, [i ...
                            nbepocs ...
                            densSSW ...
                            markers{epo}.PaP(j) ...
                            markers{epo}.Neg(j) ...
                            markers{epo}.mfr(j) ...
                            markers{epo}.tfr(j) ...
                            markers{epo}.tNe(j) ...
                            markers{epo}.tPo(j) ...
                            ele ...
                            stg ...
                            epo]);
                    end
                end
            end
        end
    end 
end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 1:                                         %
figure_Nscatter_frequencies(INVENTAIRE,electrode,O);%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%% TABLES DE SORTIE
% On construit une table pour la cohorte

varNames1 = {'NbrEpoc' 'Dens'}; NVar1 = numel(varNames1);
varNames2 = {'P2P' 'Neg' 'fre' 'tra' 'Hyp' 'Dep'}; NVar2 = numel(varNames2);

varTypes = strings(1,14);
varTypes(:,:) = 'double';
              % Colonne de gauche
              for stg = 1:numel(O.DBase.Conditions)
              TABLE(stg).full_Table = table('Size',[numel(LISTE) 1], ...
                  'VariableTypes',"string", ...
                  'VariableNames',{'SUJETS'}, ...
                  'RowNames',LISTE);
              TABLE(stg).full_Table.SUJETS = LISTE';
              end
              

    for ele = 1:numel(O.DBase.channels)
        for stg = 1:numel(O.DBase.Conditions)
        El = O.DBase.channels{ele};
        St = O.DBase.Conditions{stg};
        for k = 1:numel(varNames1), varNames_k{k} = [El '_' St '_' varNames1{k}]; end
        for k = 1:numel(varNames2), varNames_k{k+NVar1} = [El '_' St '_' varNames2{k} '_M']; end
        for k = 1:numel(varNames2), varNames_k{k+NVar1+NVar2} = [El '_' St '_' varNames2{k} '_S']; end

        for k = 1:numel(LISTE_file)
            petite_table = INVENTAIRE{ele}{stg}(INVENTAIRE{ele}{stg}(:,1)==k,1:9);
            % test si petite table vide %%%%%%%%%%%%%%%%%%%%%%%
            if size(petite_table,1)<2                         %
                ligne_sujetsM = NaN(1,9);                     %
                ligne_sujetsS = NaN(1,9);                     %
            else                                              %
                ligne_sujetsM = mean(petite_table);           %
                ligne_sujetsS = sqrt(var(petite_table,[]));   %
            end                                               %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            numtable(k,:) = [ligne_sujetsM(2:end) ligne_sujetsS(4:end)];
        end
        T = table('Size',[numel(LISTE) 14], ...
                  'VariableTypes',varTypes, ...
                  'VariableNames',varNames_k, ...
                  'RowNames',LISTE);
        for k = 1:numel(varNames_k)
        T.(varNames_k{k}) = numtable(:,k);
        end
        disp(T);
        TABLE(stg).full_Table = [TABLE(stg).full_Table T];
        end
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Ecriture du fichier EXCEL                                                             %
for stg = 1:numel(O.DBase.Conditions)                                                   %
    fileXLS = ['TABLE_SSW_' O.DBase.Conditions{stg} '.xls'];                            %
    fprintf('--> Ecriture de %s\n',fileXLS)                                             %
    fprintf('    Ce fichier contient les stat des SSW (%s)\n',O.DBase.Conditions{stg}); %
    fileXLS = [ddRes filesep fileXLS];                                                  %
    Tstag = TABLE(stg).full_Table;                                                      %
    writetable(Tstag,fileXLS);                                                          %
end                                                                                     %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('***********************************************************************\n')
fprintf('VARIABLES ARCHIVES DANS LES XLS:\n');
fprintf('\tNbr d''epoques\n');
fprintf('\tDensite (/minute)\n');
fprintf('\tAmplitude PaP (moyenne)\n');
fprintf('\tfrequence (moyenne)\n');
fprintf('\tfrequence transition (moyenne)\n');
fprintf('\tAmplitude DOWN (moyenne)\n');
fprintf('\tAmplitude UP (moyenne)\n');
fprintf('\tsuivi de :Amplitude PaP, frequences, DOMN et UP (eccart-type)\n');
fprintf('***********************************************************************\n\n')

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Comments = 'INVENTAIRE{ele}{stage}: sujet nb_epoc densite PaP Neg mfr tfr tNe tPo ele stage epo'; %
save([ddRes filesep '001_inventaire_SSW_04'],'INVENTAIRE','LISTE','LISTE_file','O','Comments');   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% ON MELANGE ICI LES STADES 2 et 3:  CAS PARTICULIER (on melange les deux
% dernieres conditions
% TABLES DE SORTIE
% On construit une table pour la cohorte a condition de trouver les deux 
% conditions '2' et '3'
k2 = find(contains(O.DBase.Conditions,'2'));
k3 = find(contains(O.DBase.Conditions,'3'));
if numel(k2)~=1 || numel(k3)~=1
    fprintf('\n\n -- Pas de stade 2 et 3 a fusionner\n    Fin. Bye.\n\n');
    return; 
else
    fprintf('\n -- Fusion des stades 2 et 3\n');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
varNames1 = {'NbrEpoc' 'Dens'}; NVar1 = numel(varNames1);
varNames2 = {'P2P' 'Neg' 'fre' 'tra' 'Hyp' 'Dep'}; NVar2 = numel(varNames2);

varTypes = strings(1,14);
varTypes(:,:) = 'double';
              % Colonne de gauche
              TABLE23.full_Table = table('Size',[numel(LISTE) 1], ...
                  'VariableTypes',"string", ...
                  'VariableNames',{'SUJETS'}, ...
                  'RowNames',LISTE);
              TABLE23.full_Table.SUJETS = LISTE';             

    for ele = 1:numel(O.DBase.channels)
        El = O.DBase.channels{ele};
        for k = 1:numel(varNames1), varNames_k{k} = [El '_23_' varNames1{k}]; end;
        for k = 1:numel(varNames2), varNames_k{k+NVar1} = [El '_23_' varNames2{k} '_M']; end;
        for k = 1:numel(varNames2), varNames_k{k+NVar1+NVar2} = [El '_23_' varNames2{k} '_S']; end;

        for k = 1:numel(LISTE_file)
            INV2_k = INVENTAIRE{ele}{k2}(INVENTAIRE{ele}{k2}(:,1)==k,1:9);
            INV3_k = INVENTAIRE{ele}{k3}(INVENTAIRE{ele}{k3}(:,1)==k,1:9);
            petite_table = [INV2_k; INV3_k];
            % test si petite table vide %%%%%%%%%%%%%%%%%%%%%%%%
            if size(petite_table,1)<2                          %
                ligne_sujetsM = NaN(1,9);                      %
                ligne_sujetsS = NaN(1,9);                      %
            else                                               %
                ligne_sujetsM = mean(petite_table);            %
                ligne_sujetsS = sqrt(var(petite_table,[]));    %
                % Ajustement du nombre d'epoques et densite:   %
                Nb2 = INV2_k(1,2);                             %
                Nb3 = INV3_k(1,2);                             %
                De2 = INV2_k(1,3);                             %
                De3 = INV3_k(1,3);                             %
                ligne_sujetsM(2) = Nb2+Nb3;                    %
                ligne_sujetsM(3) = (De2*Nb2+De3*Nb3)/(Nb2+Nb3);%
            end                                                %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            numtable(k,:) = [ligne_sujetsM(2:end) ligne_sujetsS(4:end)];
        end
        T = table('Size',[numel(LISTE) 14], ...
                  'VariableTypes',varTypes, ...
                  'VariableNames',varNames_k, ...
                  'RowNames',LISTE);
        for k = 1:numel(varNames_k)
        T.(varNames_k{k}) = numtable(:,k);
        end
        disp(T);
        TABLE23.full_Table = [TABLE23.full_Table T];
    end
    
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
% Ecriture du fichier EXCEL                                             %
    fileXLS = ['TABLE_SSW_Stade23.xls'];                                %
    fprintf('--> Ecriture de %s\n',fileXLS)                             %
    fprintf('    Ce fichier contient les stat des SSW (Stade 2+3)\n');  %
    fileXLS = [ddRes filesep fileXLS];                                  %
    Tstag = TABLE23.full_Table;                                         %
    writetable(Tstag,fileXLS);                                          %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% Fusion des INVENTAIRE{}{k2} et INVENTAIRE{}{k3} %%%%%%%%%%%%%%%%%%%%%%%
[INVENTAIRE23] = fusion23(INVENTAIRE,k2,k3);                            %
save([ddRes filesep '001_inventaire_SSW_04'],'INVENTAIRE23','-append'); %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Figure 2:                                           %
% On fusionne dans les OPTIONS                        %
O.DBase.Conditions(k3) = '';                          %
O.DBase.Conditions{k2} = 'NREM 2+3';                  %
figure_Nscatter_frequencies(INVENTAIRE23,electrode,O);%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n\n -- Fin. Bye.\n\n')
%end