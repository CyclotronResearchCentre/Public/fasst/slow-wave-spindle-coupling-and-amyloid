
clear; clc
addpath('./ressources/');
% recherche de la base de donnees
dtb = './PMCO/';
dtb_mat = './PMCO_2mat/';

% les fichiers
file_mat = dir([dtb '*.mat']);   % 'PMCOF002_TEST_140616_T232800_20160615T003032-2.mat';
n_file = numel(file_mat);

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('--- On a trouve %d sujet(s)\n',n_file);

for i_file = 1%:n_file

    OPTIONS.name = [dtb file_mat(i_file).name];
    load(OPTIONS.name);
    % Definitions
    OPTIONS = struct;
    OPTIONS.Canaux.derivations = {'C3','C4','Cz','F3','F4','Fz'};%'P3','P4','Pz'

    % name
    fname = strsplit(D.fname,'_');
    OPTIONS.name = fname{1};
    fprintf('--- Analyse de %s\n',fname{1});
    OPTIONS = inventaire_DC(D,data,OPTIONS);
    % Data N2
    [ trials ] = make_trials( data, 'N2' , OPTIONS);
    save([dtb_mat OPTIONS.name '_N2_Cycle0'],'OPTIONS','trials');
    [ trials ] = make_trials( data, 'N3' , OPTIONS);
    save([dtb_mat OPTIONS.name '_N3_Cycle0'],'OPTIONS','trials');
    
    fprintf(' --> sauvegarde de %s terminee\n',fname{1});
end
