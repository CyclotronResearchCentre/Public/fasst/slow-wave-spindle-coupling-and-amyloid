THIS PACKAGE CAN BE RUN FROM "SW_SPDL_all.mat" WHICH CALLS UPON ALL THE FUNCTIONS



This package runs the detection of slow waves and their association with sigma/spindles.
The scripts need to be executed in the following order:

1) lecture_data
  takes the .mat (PMCO) from the PMCO folder, and gives  2 files per subject in the PMCO_2mat folder (PMCO_N2_Cycle0 et PMCO_N3_Cycle0)


2) make_sex_data
  uses COF_DEMOGRAPHICS to redistribute the PMCO_N2/N3_Cycle0 files by classifying them according to gender into PMCO_F_2mat et PMCO_H_2mat


3) script_principal_Detection (requires additional codes in set_up and ressources)
  proper SW detection, creates a Dgroup_mat folder with a Dgr_suj_XX_PMCOXX for each subject (containing initial data, organised by epochs and not electrodes), possibility to adapt detection thresholds;
also creates a SW_PMCOXX file per subject in Results\Resultats_PMCO_F/H_date\DBaseMatlab


4) Analyses_inventaire_SSW04 (v. 20/12/20 20h27)
  input = Results\Resultats_PMCO_F/H_date, output = 3 fichiers excel TABLE_SSW_N2/N3/Stade23 


5) Analyses_inventaire_Slow_Fast01 (v. 20/12/20 20h31)
  computes transition frequency which separates the two SW groups (slow/fast switchers)
 outputs = graphs with transition frequencies and thresholds for N2, N3 and N2+N3, as well as 6 excel files TABLE_FAST/SLOW_N2/N3/Stade23 with SW parameters according to SW type


6) Analyses_PAC_Slow_Fast_sigma01 (v. 5/03/21 16h50)
  computes the SW-sigma coupling strength, creates in Results\Resultats_PMCO_H/F_date\Results 4 excel files(TABLE_N2/N3_FAST/SLOW_zPAC)


7) script_principal_Detection [in Detection_SPDL_AMYL]
Spindle detection, creates DBase_SPIN with a file for each subject with spindle detection info


8) analyse_SSW_SPIN_onset_coupling.m [in 6_SSW_SPIN_Archive03]
Output = DBase_SSW_SPIN folder with one SSW_SPIN_XXX.mat file per subject containing co-occurence data + SSW_SPDL_Phases_N2/N3 in results folder
with the spindle onset on SW phase (elecline_o_slow/fast) and their maximum (elecline_d_slow/fast)