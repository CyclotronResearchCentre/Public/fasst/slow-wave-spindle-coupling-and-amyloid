function revision_methode_Carrier(SSW)

% affichage:
fprintf('\n     SSW Detector: %s\n',SSW.method);
fprintf('     frequence: %3.2f - %3.2f (Hz)\n',SSW.Carrier.fmin_max(1),SSW.Carrier.fmin_max(2));
fprintf('     duree (Hyp) > %3.2f (msec)\n',SSW.Carrier.duree_min_max_Neg(1));
fprintf('     duree (Hyp) < %3.2f (msec)\n',SSW.Carrier.duree_min_max_Neg(2));
fprintf('     duree (Dep) < %3.2f (msec)\n',SSW.Carrier.duree_max_Pos);
fprintf('     Amplitude PaP > %3.2f (uV)\n',SSW.Carrier.PaP);
fprintf('     Amplitude Neg > %3.2f (uV)\n',SSW.Carrier.Neg);

% answer = input('\n --- Change for slow SSW: 0.16 - 1.3 (Hz) ? [n]/y ','s');
% if ischar(answer) && strcmp(answer,'y')
%     SSW.Carrier.fmin_max(1) = 0.16;
%     SSW.Carrier.fmin_max(2) = 1.3;
%     
%     SSW.method = 'Carrier0.16_4Hz_lentes';
%     fprintf('    Detection: %s\n',SSW.method);
%     fprintf('    frequences: %3.2f - %3.2f (Hz)\n',SSW.Carrier.fmin_max(1),SSW.Carrier.fmin_max(2));
%     fprintf('    (autres parametres identiques)\n');
% end
% fprintf('\n');

% SSWn = SSW;

end
