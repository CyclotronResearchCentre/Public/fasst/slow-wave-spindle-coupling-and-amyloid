function revision_methode_Walker(SPL)

% affichage:
fprintf('\n     Spindle Detector: %s\n',SPL.method);
fprintf('     frequence: %3.2f - %3.2f (Hz)\n',SPL.fmin_max(1),SPL.fmin_max(2));
fprintf('     duree > %3.2f (msec)\n',SPL.duree_min_max(1));
fprintf('     duree < %3.2f (msec)\n',SPL.duree_min_max(2));
fprintf('     Amplitude > %3.2f percentile\n',SPL.percentile);
fprintf('     Smoothing %3.2f (msec)\n',SPL.smoothing);

% answer = input('\n --- Change for slow SSW: 0.16 - 1.3 (Hz) ? [n]/y ','s');
% if ischar(answer) && strcmp(answer,'y')
%     SSW.Carrier.fmin_max(1) = 0.16;
%     SSW.Carrier.fmin_max(2) = 1.3;
%     
%     SSW.method = 'Carrier0.16_4Hz_lentes';
%     fprintf('    Detection: %s\n',SSW.method);
%     fprintf('    frequences: %3.2f - %3.2f (Hz)\n',SSW.Carrier.fmin_max(1),SSW.Carrier.fmin_max(2));
%     fprintf('    (autres parametres identiques)\n');
% end
% fprintf('\n');

% SSWn = SSW;

end
