function [marker,signals,N_spindle] = detect_spindles_Walker_global(signals,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% marker------------ .delimiters : table N1(start) N2(end) 


fs = signals.fs;
data = signals.data;
data_f_spind = zeros(size(data));
data_f_sp_ah = zeros(size(data));
[Ntrials,~] = size(data);
% parametres du filtre (SSW)
wn = OPTIONS.Sleep.SPL.fmin_max / (fs/2);
spl_filter = fir1(150,wn);
N_spindle   = 0;
amplitude_all = [];

% on passe en revue les trials:
for it = 1:Ntrials
    sig = data(it,:); 
    % filtrage dans la bande SSW
    sigf = filtfilt(spl_filter,1,sig);
    data_f_spind(it,:) = sigf;
    sigh = hilbert(sigf);
    siga = smooth(abs(sigh),fs,OPTIONS);
    data_f_sp_ah(it,:) = siga;
    amplitude_all = cat(1,amplitude_all,siga);
end

signals.data_f_sigma    = data_f_spind;
signals.data_f_sigma_Hilbert = data_f_sp_ah;

% PERCENTILE (over all the trial we have here)
amplitude_all = sort(amplitude_all,'ascend');
n_thr   = ceil(OPTIONS.Sleep.SPL.percentile * numel(amplitude_all) / 100);
marker.spindle_thr = amplitude_all(n_thr);

% Critere de duree:
duree_min = ceil(OPTIONS.Sleep.SPL.duree_min_max(1)/1000*signals.fs);
duree_max = ceil(OPTIONS.Sleep.SPL.duree_min_max(2)/1000*signals.fs);

for it = 1:Ntrials
    marker.delimiters{it} = find_segment(signals.data_f_sigma_Hilbert(it,:),marker.spindle_thr);
    if ~isempty(marker.delimiters{it})
        durees = marker.delimiters{it}(:,2)-marker.delimiters{it}(:,1);
        liste = find((durees>duree_min)&(durees<duree_max));
        marker.delimiters{it} = marker.delimiters{it}(liste,:);
        Ns = size(marker.delimiters{it},1);
                 for k = 1:Ns
                 marker.frequence{it}(k) = avg_freq_of_spindle(...
                            signals.data_f_sigma(it,:),...
                            fs,...
                            marker.delimiters{it}(k,1), ...
                            marker.delimiters{it}(k,2));
                 marker.maxPower{it}(k,:) = maxPowerSpdl(...
                            signals.data_f_sigma_Hilbert(it,:),...
                            fs,...
                            marker.delimiters{it}(k,1), ...
                            marker.delimiters{it}(k,2));  
                 end
        N_spindle = N_spindle + numel(liste);
    end
end

end

% ======================================================================

% a function that smooths the signal (sliding window)
function sig_out = smooth(sig_in,fs,OPTIONS)
tau = ceil((OPTIONS.Sleep.SPL.smoothing/2/1000)*fs);
n   = length(sig_in);
sig_out = zeros(1,n);
    for i=1:n, 
        sig_out(i) = mean(sig_in(i-min(tau,i-1):i+min(tau,n-i)));
    end
end

% a function that finds the begining and end of the event defined above a
% threshold (it works but may be improved to save time)
function seg = find_segment(sig,thr)

uu = sig>=thr;
s = 0; seg = []; j = 1; 
for i = 1:numel(uu), 
    if uu(i)&&(~s)
        seg(j,1) = i;   s = 1; 
    elseif ~uu(i)&&s
        seg(j,2) = i-1; s = 0; j = j+1; 
    end;
end
if s, seg(j,2) = numel(uu); end % effet de bord (le spindle est sur le bord)
end

function freq = avg_freq_of_spindle(sig_Hilb,fs,debut,fin)
    segment = sig_Hilb(debut:fin);
    zeros_c = numel(find(segment(1:end-1).*segment(2:end)<0));
    duree   = (fin-debut)/fs;
    freq    = 0.5*zeros_c / duree;
end

function pow = maxPowerSpdl(amp_Hilb,fs,debut,fin)
    segment = amp_Hilb(debut:fin);
    [maximum,when] = max(segment);
    pow(1) = debut+when-1;
    pow(2) = maximum;
end