function [Spindles] = detect_SPIN_Walker(signals,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% marker------------ .delimiters : table N1(start) N2(end) 

fprintf('\n--> Spindle detector (Walker): (epoch) nbr_of_spindles found\n');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Initialisation:                  %
fs = signals.fs;                   %
data = signals.data;               %
[Ntrials,Nechant] = size(data);    %
data_f_spind = zeros(size(data));  %
data_f_sp_ah = zeros(size(data));  %
data_f_sp_ph = zeros(size(data));  %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% parametres du filtre (SSW) %%%%%%%%%%%%%
wn = OPTIONS.Sleep.SPL.fmin_max / (fs/2);%
% or = max(150,3*fs/8);                  %
or = max(150,round(3*fs/8))%  modif by JM%
spl_filter = fir1(or,wn);                %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% Compteur: %%%%%
N_spindle   = 0;%
%%%%%%%%%%%%%%%%%

% Critere de duree (en echantillons): %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
duree_min = ceil(OPTIONS.Sleep.SPL.duree_min_max(1)/1000*signals.fs);%
duree_max = ceil(OPTIONS.Sleep.SPL.duree_min_max(2)/1000*signals.fs);%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% on passe en revue les trials: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
markers = cell(1,Ntrials);                                                %
inventaire = zeros(1,Ntrials);                                            %
for it = 1:Ntrials                                                        %
    sig = double(data(it,:));                                             %
    % filtrage dans la bande des fuseaux %%%                              %
    sigf = filtfilt(spl_filter,1,sig);     %                              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                              %
    data_f_spind(it,:) = sigf;                                            %
    sigh = hilbert(sigf);                                                 %
    siga = smooth(abs(sigh),fs,OPTIONS);                                  %
    data_f_sp_ah(it,:) = siga;                                            %
    data_f_sp_ph(it,:) = unwrap(angle(sigh));                             %
                                                                          %
                                                                          %
    % PERCENTILE (over all the trial we have here)                        %
    siga_s = sort(siga,'ascend');                                         %
    n_thr   = ceil(OPTIONS.Sleep.SPL.percentile * numel(siga_s) / 100);   %
    markers{it}.spindle_thr = siga_s(n_thr);                              %
                                                                          %
    % MARKERS                                                             %
    markers{it}.filter = [or wn];                                         %
    markers{it}.maxPower = [];                                            %
    markers{it}.fr = [];                                                  %
    markers{it}.n_t = find_segment(siga,markers{it}.spindle_thr);         %
                                                                          %
    if ~isempty(markers{it}.n_t)                                          %
        durees = markers{it}.n_t(:,2)-markers{it}.n_t(:,1);               %
        liste = find((durees>duree_min)&(durees<duree_max));              %
                                                                          %
        markers{it}.n_t = markers{it}.n_t(liste,:);                       %
        markers{it}.duree = durees(liste,:) / fs * 1000; % msec           %
        Ns = numel(liste);                                                %
        inventaire(it) = Ns;                                              %
                 for k = 1:Ns                                             %
                 markers{it}.fr(k) = avg_freq_of_spindle(...              %
                            data_f_spind(it,:),...                        %
                            fs,...                                        %
                            markers{it}.n_t(k,1), ...                     %
                            markers{it}.n_t(k,2));                        %
                 markers{it}.maxPower(k,:) = maxPowerSpdl(...             %
                            data_f_sp_ah(it,:),...                        %
                            markers{it}.n_t(k,1), ...                     %
                            markers{it}.n_t(k,2));                        %
                 end                                                      %
        N_spindle = N_spindle + Ns;                                       %
    end                                                                   %
    if mod(it,12)==0, fprintf('\n'); end                                  %
    fprintf('(%d)%d ',it,numel(liste));                                   %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('\n--> we found %d spindles (%d trials) --> ',N_spindle,Ntrials);

    % Output: %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Spindles.Comments = '';                         %
    Spindles.signals = signals;                     %
    Spindles.signals.data_f_sigma = data_f_spind;   %
    Spindles.signals.data_f_sp_ah = data_f_sp_ah;   %
    Spindles.signals.data_f_sp_ph = data_f_sp_ph;   %
    Spindles.markers = markers;                     %
    Spindles.inventaire = inventaire;               %
    Spindles.duree = Ntrials * Nechant / fs /60; %mn%
    Spindles.densite = N_spindle / Spindles.duree;  %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fprintf('density: %4.3f spindles/mn\n',Spindles.densite);
    
    Spindles.Comments = sprintf('\nOutput of the function\n%s',...
    ['Spindles.marker : One marker field per epoch with the following fields,\n\t' ...
     'nsamp is a table of [start end], the number of line is the number of spindles\n\t' ...
     'frequency is an aray of the mean frequency of the spindles\n\t' ...
     'Nspindles is the number of detected spindles\n\t']);
    
end









% ======================================================================

% a function that smooths the signal (sliding window)
function sig_out = smooth(sig_in,fs,OPTIONS)
tau = ceil((OPTIONS.Sleep.SPL.smoothing/2/1000)*fs);
n   = length(sig_in);
sig_out = zeros(1,n);
    for i=1:n
        sig_out(i) = mean(sig_in(i-min(tau,i-1):i+min(tau,n-i)));
    end
end

% a function that finds the begining and end of the event defined above a
% threshold (it works but may be improved to save time)
function seg = find_segment(sig,thr)

uu = sig>=thr;
s = 0; seg = []; j = 1; 
for i = 1:numel(uu) 
    if uu(i)&&(~s)
        seg(j,1) = i;   s = 1; 
    elseif ~uu(i)&&s
        seg(j,2) = i-1; s = 0; j = j+1; 
    end
end
if s, seg(j,2) = numel(uu); end % effet de bord (le spindle est sur le bord)
end

function freq = avg_freq_of_spindle(sig_Hilb,fs,debut,fin)
    segment = sig_Hilb(debut:fin);
    zeros_c = numel(find(segment(1:end-1).*segment(2:end)<0));
    duree   = (fin-debut)/fs;
    freq    = 0.5*zeros_c / duree;
end

function pow = maxPowerSpdl(amp_Hilb,debut,fin)
    segment = amp_Hilb(debut:fin);
    [maximum,when] = max(segment);
    pow(1) = debut+when-1;
    pow(2) = maximum;
end