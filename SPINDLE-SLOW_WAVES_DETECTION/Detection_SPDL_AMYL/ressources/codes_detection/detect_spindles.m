function [marker,signals,N_spindle] = detect_spindles(signals,OPTIONS,nom)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS.Sleep contains the parameters for the SPINDLE detection
% in particular, the OPTIONS.Sleep.SPL gives the paprameters of the
% detectors. TWO DETECTORS AVAILABLE: 
% OPTIONS.Sleep.SPL.method = 'A7' (the detector in Lacourse et al. 2018)
% OPTIONS.Sleep.SPL.method = 'Walker' (the detector in Walker et al. 2017)

% output:
% marker------------ .delimiters

if     strcmpi(OPTIONS.Sleep.SPL.method,'a7')
       [marker,signals,N_spindle] = detect_spindles_a7(signals,OPTIONS);
elseif strcmpi(OPTIONS.Sleep.SPL.method,'walker')
       [marker,signals,N_spindle] = detect_spindles_Walker_global_thr(signals,OPTIONS,nom);
else   fprintf('\n wrong call of the spindles detector, we quit... bye\n'); 
       return;
end
end