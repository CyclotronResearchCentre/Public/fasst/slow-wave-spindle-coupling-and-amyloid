    function spin_new = few_stats_spindles(spin)
    
    spin_new = spin;
    TABLE    = [];
    
    for j = 1:length(spin_new.markers) % electrode
        for k = 1:numel(spin_new.markers{j}) % condition 
            ELE{j} = spin_new.markers{j}{k}.electro;
            CON{k} = spin_new.markers{j}{k}.stage;
            
            for ep = 1:numel(spin_new.markers{j}{k}.markers)
                K = spin_new.markers{j}{k}.inventaire(ep);
                if K~=0
                    table = [ ...
                    j*ones(K,1) , ...
                    k*ones(K,1) , ...
                    ep*ones(K,1) , ...
                    (1:K)' , ...
                    spin_new.markers{j}{k}.markers{ep}.n_t , ...
                    spin_new.markers{j}{k}.markers{ep}.maxPower , ...
                    spin_new.markers{j}{k}.markers{ep}.fr' , ...
                    spin_new.markers{j}{k}.markers{ep}.duree ];
                    TABLE = [TABLE ; table];
                end
            end
        end
    end
    
    spin_new.INVENTAIRE.TABLE = TABLE;
    spin_new.INVENTAIRE.ELE   = ELE;
    spin_new.INVENTAIRE.CON   = CON;
    spin_new.INVENTAIRE.Comment = ...
        sprintf('Table: ele, stade, epoque, spindle debut fin po_were power fr duree');
    
    
    end
    