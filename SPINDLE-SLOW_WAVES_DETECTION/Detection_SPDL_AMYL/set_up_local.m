function OPTIONS = set_up_local(dd)

% O.DBase.Inventaire.relative_files_mat

fsep = filesep;
ll = dir([dd fsep '*.mat']);
liste = {};
for i = 1:numel(ll)
    a = strsplit(ll(i).name,'_');
    liste{i} = a{1};
end
liste = unique(liste);
fprintf('\n --- set_up: we have %d subjects:\n',numel(liste));

OPTIONS.DBase.path       = dd;
OPTIONS.DBase.groups     = {'Amyl'};
OPTIONS.DBase.listes{1}  = liste; %<---------- 1 groupe
OPTIONS.DBase.Conditions = {'N2','N3'};
% OPTIONS.DBase.channel    = {'C3'  'C4'  'Cz'  'F3'  'F4'  'Fz'  'P3'  'P4'  'Pz'};
OPTIONS.DBase.channel    = {'C3'  'C4'  'Cz'  'F3'  'F4'  'Fz' };% 'P3'  'P4'  'Pz'

for i = 1:numel(liste)
    
    ll =  dir([dd fsep liste{i} '_' OPTIONS.DBase.Conditions{1} '*.mat']);
    OPTIONS.DBase.Inventaire.relative_files_mat{1}{i,1} = '';
    if numel(ll), fprintf('\t %s found, ',[liste{i} '_' OPTIONS.DBase.Conditions{1}]);
        OPTIONS.DBase.Inventaire.relative_files_mat{1}{i,1} = [dd fsep ll(1).name];
    else fprintf('\t %s NOT found, ',[liste{i} '_' OPTIONS.DBase.Conditions{1}]);
    end
    
    ll =  dir([dd fsep liste{i} '_' OPTIONS.DBase.Conditions{2} '*.mat']);
    OPTIONS.DBase.Inventaire.relative_files_mat{1}{i,2} = '';
    if numel(ll), fprintf(' %s found\n ',[liste{i} '_' OPTIONS.DBase.Conditions{2}]);
        OPTIONS.DBase.Inventaire.relative_files_mat{1}{i,2} = [dd fsep ll(1).name];
    else fprintf(' %s NOT found\n ',[liste{i} '_' OPTIONS.DBase.Conditions{2}]);
    end
end
end
