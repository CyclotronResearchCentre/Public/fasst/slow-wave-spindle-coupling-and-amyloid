function script_principal_Detection(dd)


addpath(genpath('./ressources'));
%function script_principal_Detection()
addpath(genpath('./set_up'));
% clear
% clc
% Chemin
% addpath(genpath('./set_up'));
% addpath(genpath('../codes/codes_detection'));
% addpath(genpath('../codes/CircStat'));
% addpath(genpath('../codes/codes_stat'));

% ====== THE DataBASE (either we build it or we provide it)
fprintf('\n\n ========= SSW - Spindles project (%s) ========= \n\n',date);
fprintf('\tlook in Amyloide_Qu-Be the directory of the matlab data\n');
fprintf('\tfiles (extracted from the original database)\n');

% dd = uigetdir('../');
if ~ischar(dd), disp('== Bye!'); return; end
% pour Windows:
% repertoire lu
fsep = filesep;
uu = strsplit(dd,fsep);
fprintf('\n\tRepertoire lu: %s\n',uu{end});
% ============

% creation des OPTIONS de base: la liste des sujets et les donnees de base
% utilisee. Aussi inventaire

    O = set_up_local(dd);
    O = set_up_general_new(O); 
    revision_methode_Walker(O.Sleep.SPL);
    
    
    Dpathmath = dir([O.Results.out_dbase fsep 'D*']);
    if ~isempty(Dpathmath)
        %O = set_up_Detector(O);
        fprintf('\n --- We read files in\n     %s\n',O.Results.out_dbase);
    else
%         O = set_up_Detector(O);
        set_up_Data(O);
        Dpathmath = dir([O.Results.out_dbase fsep 'D*']);
    end
    dgr = dir([O.Results.out_dbase filesep 'Dgr*']);    

    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Repertoire de sortie:                       %
    out_dir = [dd filesep 'DBase_SPIN'];          %
    if isfolder(out_dir), rmdir(out_dir,'s'); end %
    mkdir(out_dir);                               %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    for i = 1:numel(dgr) % sujets
        load([O.Results.out_dbase filesep dgr(i).name],'TR');
        fprintf('\n     %s:\n',TR.name);
        MARKERS = {};
        for j = 1:numel(TR.D) % condition
            for k = 1:numel(TR.D{j}) % electrode   
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
            [marker{k}{j}] = detect_SPIN_Walker(TR.D{j}{k},O); %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

            marker{k}{j}.electro = TR.D{j}{k}.electrode;
            marker{k}{j}.stage   = O.DBase.Conditions{j};
            marker{k}{j}.identif = TR.name;
 
            end
        end
        SPIN.name_Id = TR.name;
        SPIN.markers = marker;
        
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            SPIN = few_stats_spindles(SPIN); %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            fprintf('--> Ecriture de %s',['/SPIN_' TR.name]);  
            save([out_dir '/SPIN_' TR.name],'SPIN','O');
            fprintf(' done\n'); 
    end    

    
    
    