%% Script calling all the functions extracting slow wave parameters,
% as well as slow-waves/spindles coupling 
mainpath = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean');
sexpath = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\PMCO_Sex_2mat');
sw_detectpath = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\Detection_SSW_AMYL');
resultspath = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\Results');

% 1. Lecture_data [in main folder]
% This function takes raw files from the PMCO folder and produces 2 files: 
% XXX_N2_cycle0 and XXX_N3_cycle0 for each subject, which are placed in PMCO_2mat
% Input = PMCO folder ; Ouput = 
cd(mainpath); lecture_data_DC({'C3','C4','Cz','F3','F4','Fz'});

% 2. Make_sex_data [in PMCO_Sex_2mat folder]
% Sorts N2/N3_cycle0 files according to sex for further processing
% according to sex
% Requires a demographics mat file with 3 colums (subj number, sex (1=M; 2=F), optionally age)
% Input = PMCO_2mat folder; Output = PMCO_F/H_2mat folders with cycle0
% files
cd(sexpath);make_sex_data;

% 3. Script_principal_Detection [in Detection_SSW_AMYL]
% SW detection, possible to adapt detection thresholds for SW (original = 75�V peak
% to peak amplitude, and -40�V negative deflection)
% Adapted from Ronsinvil (2021): F 70 -37; M 60.6 -32; or averaged (F/M) 65 -35
% Input = PMCO_F/H_2mat folder ; Output = Dgroup_mat folder (with initial data reorganised
% by electrode and not by epoch), with a Dgr_suj_XX_PMCOXX file in Results/Resultats_PMCO_F/H_date/Dgroup_mat
% and a SW_PMCOXX per subject in Results/Resultats_PMCO_F/H_date/DBase_SSW
% + a txt file with densities per stage
cd(sw_detectpath); 
path_men = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\PMCO_Sex_2mat\PMCO_H_2mat');
P2P_men = 60.6 ; neg_men = 32;
path_women = ('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\PMCO_Sex_2mat\PMCO_F_2mat');
P2P_women = 70 ; neg_women = 37;
script_principal_Detection(path_men, P2P_men, neg_men); 
script_principal_Detection(path_women, P2P_women, neg_women); 

% 4. Analyses_inventaire_SSW04 [in main folder]
% Computes SW parameters per electrodes
% Input = Resultats_PMCO_F/H_date folder in Results; Output = 3 excel files
% (TABLE_SSW_N2/N3/Stade23) + one figure per electrode line with global SW
% frequency against transition frequency
cd(resultspath); 
% To improve
test = dir; female_dir = [pwd filesep test(3).name]; male_dir = [pwd filesep test(4).name];
Analyses_inventaire_SSW04(male_dir); cd([male_dir filesep 'Figures']);
FolderName = ([male_dir filesep 'Figures']);   
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number'));
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all

Analyses_inventaire_SSW04(female_dir);
FolderName = ([female_dir filesep 'Figures']);  
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number'));
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all

% 5. Analyses_inventaire_Slow_Fast01 [in main folder]
% Computes transition frequency
% Input = Results/Resultats_PMCO_F/H_date folder; Output = transition
% frequency graphs for SS/FS and N2/N3/N23 + 6 excel files
% (TABLE_FAST/SLOW_N2/N3/Stade23)
cd(mainpath);
Analyses_inventaire_Slow_Fast01(male_dir); cd([male_dir filesep 'Figures']);
FolderName = ([male_dir filesep 'Figures']);   
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number')+10);
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all

Analyses_inventaire_Slow_Fast01(female_dir); cd([female_dir filesep 'Figures']);
FolderName = ([female_dir filesep 'Figures']);   
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number')+10);
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all

% 6. Analyses_PAC_Slow_Fast_sigma01 [in main folder]
% Computes SW-sigma coupling strength
% Input = Results/Resultats_PMCO_F/H_date folder; Output = 4 excel files (TABLE_N2/N3_FAST/SLOW_zPAC)
% n�cessite de rechanger dBase_SSW en dBaseMatlab??
cd(mainpath); 
Analyses_PAC_Slow_Fast_sigma01(male_dir); Analyses_PAC_Slow_Fast_sigma01(female_dir);

% 7. script_principal_Detection [in Detection_SPDL_AMYL]
% Spindle detection
% Output = DBase_SPIN folder with one SPIN_XXX.mat spindle detection file
% per subject
cd('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\Detection_SPDL_AMYL');
script_principal_Detection(male_dir); script_principal_Detection(female_dir);
% creates a new folder Resultats_Resultats_PMCO_F12-Aug16-Aug-2021 ??

% 8.  analyse_SSW_SPIN_onset_coupling.m [in 6_SSW_SPIN_Archive03]
% SW/spindles coupling analysis
% Output = DBase_SSW_SPIN folder with one SSW_SPIN_XXX.mat file per subject
% containing co-occurence data + SSW_SPDL_Phases_N2/N3 in results folder
% with the spindle onset on SW phase (elecline_o_slow/fast) and their maximum (elecline_d_slow/fast)
cd('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\6_SSW_SPIN_Archive03');
analyse_SSW_SPIN_onset_coupling(male_dir);cd([male_dir filesep 'Figures']);
FolderName = ([male_dir filesep 'Figures']);   
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number')+20);
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all

cd('D:\DATA_COF\SCRIPTS\Sleep\MTL_clean\6_SSW_SPIN_Archive03');
analyse_SSW_SPIN_onset_coupling(female_dir); cd([female_dir filesep 'Figures']);
FolderName = ([female_dir filesep 'Figures']);   
FigList = findobj(allchild(0), 'flat', 'Type', 'figure');
for iFig = 1:length(FigList)
  FigHandle = FigList(iFig);
  FigName   = num2str(get(FigHandle, 'Number')+20);
  set(0, 'CurrentFigure', FigHandle);
  savefig(fullfile(FolderName, [FigName '.fig']));
end
close all
