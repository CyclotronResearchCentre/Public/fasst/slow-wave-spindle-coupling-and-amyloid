function f_sep = frequence_separation_slow_fast(fi)
    fM = min(4,max(fi));
    fm = min(fi);
    f_samp = linspace(fm,fM,150);
    GG = fitgmdist(fi,2,'options',statset('MaxIter',1500','TolFun',1e-5));
    % gauss1
    mu1 = GG.mu(1);
    si1 = sqrt(GG.Sigma(1));
    p1 = round(100*GG.ComponentProportion(1));
    A1 = GG.ComponentProportion(1)/sqrt(2*pi)/si1;
    g1 = A1*exp(-0.5*(f_samp-mu1).^2/si1^2);
    %gauss2
    mu2 = GG.mu(2);
    si2 = sqrt(GG.Sigma(2));
    p2 = round(100*GG.ComponentProportion(2));
    A2 = GG.ComponentProportion(2)/sqrt(2*pi)/si2;
    g2 = A2*exp(-0.5*(f_samp-mu2).^2/si2^2);

    [~,m1] = find(g1>g2,1,'first');
    [~,M1] = find(g1>g2,1,'last');
    [~,m2] = find(g2>g1,1,'first');
    [~,M2] = find(g2>g1,1,'last');
    sep_n = min(max(m1,M1),max(m2,M2));
    f_sep = f_samp(sep_n);
end

