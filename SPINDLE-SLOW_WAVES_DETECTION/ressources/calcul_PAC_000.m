function [PAC] = calcul_PAC_000(TRD,MKR)
% TDR = TR.D{std}{ele};
% MKR = SSW.markers{ele}{std};
%
% On utilisse:
%   MKR.markers{epo}
%   TDR.data(epo,:);

% Inventaire des OL:
for i = 1:numel(MKR.markers)
    liste_ssw(i) = numel(MKR.markers{i}.PaP);
end
liste = find(liste_ssw);

    % fs et filtres
    SW.fs = TRD.fs;
    filters.H = [10 16];
    filters.L = [0.3 4];

    
% epoques munies de SW:    
data = TRD.data(liste,:);
k = 1;
% boucle sur la liste d'OL:
for i = 1:numel(liste)
    epo = liste(i);
    % boucle sur les OL
    for j = 1:liste_ssw(epo)
        SW.mkr = MKR.markers{epo}.n_t(j,:);
        SW.signal = data(i,:);
        
        [PACj] = calcul_PAC_003(SW,filters);
        
        PAC.dPAC(k) = PACj.dPAC;
        PAC.zPAC(k) = PACj.zPAC;
        PAC.ftr(k)  =  MKR.markers{epo}.tfr(j);
        k = k+1;
    end
end

end



