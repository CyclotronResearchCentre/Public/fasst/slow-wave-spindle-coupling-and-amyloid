function [ trials ] = make_trials( d, stade , OPTIONS)
% Mise en forme des donnes sous la forme
% trials = 
% 
%       type: 'N2'
%     trials: [1x1 struct]
% 
% trials.trials =
% 
%         sujet_file: '/media/jmlina/Seagate Backup Plus Drive/Projet_SSW_Spindles/d...'
%     bad_electrodes: {}
%               data: {1x395 cell}
% 
% trials.trials.data{1} =
% 
%           data: [9x7680 double]
%     intervalle: [376321 384000]
%       fullpath: '/media/jmlina/Seagate Backup Plus Drive/Projet_SSW_Spindles/data_...'
%       Comments: 'Light SW Sleep'
%            LUT: {'f3'  'f4'  'c3'  'c4'  'p3'  'p4'  'fz'  'cz'  'pz'}
%             fs: 256
%           Time: [1x7680 double]

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Selection du stade
if  strcmpi(stade,'N2')
    std = 2;
    I_pages = find(OPTIONS.Data.pages(:,1)==std);
    trials.type = 'N2';
    trials.trials.sujet_file = OPTIONS.name;
    fprintf(' --- lecture de %d pages du stade %s \n',length(I_pages),stade);
elseif strcmpi(stade,'N3')
    std = 3;
    I_pages = find(OPTIONS.Data.pages(:,1)==std);
    trials.type = 'N3';
    trials.trials.sujet_file = OPTIONS.name;
    fprintf(' --- lecture de %d pages du stade %s \n',length(I_pages),stade);
else 
    fprintf(' --- stade %s non reconnu dans la definition des trials\n',stade);
    trials = [];
    return;
end

% Selection des canaux
cnx = OPTIONS.Canaux.derivations_ok;
% Donnees trials
for i_tr = 1:length(I_pages)
    pg1 = OPTIONS.Data.pages(I_pages(i_tr),2);
    pg2 = OPTIONS.Data.pages(I_pages(i_tr),3);
    trials.trials.data{i_tr}.intervalle = [pg1 pg2];
    trials.trials.data{i_tr}.fullpath = '';
    trials.trials.data{i_tr}.fs = OPTIONS.Signal.fs;
    trials.trials.data{i_tr}.Time = (0:(pg2-pg1-1)) / OPTIONS.Signal.fs;
    trials.trials.data{i_tr}.data = d(cnx,pg1:pg2);
    trials.trials.data{i_tr}.LUT = OPTIONS.Canaux.derivations;
end
end

