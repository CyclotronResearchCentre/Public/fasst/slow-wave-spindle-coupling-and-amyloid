

function [newSUJ, LIGNE_SLOW , LIGNE_FAST] = inventaire_switchers(SUJ,freq_sep)
newSUJ = SUJ;
newSUJ.f_sep = freq_sep;
for i_ele = 1:numel(newSUJ.All)
    for i =1:2

        if i == 1, % slow
            selection = find(newSUJ.All{i_ele}.trans_freq<freq_sep);
        else % fast
            selection = find(newSUJ.All{i_ele}.trans_freq>=freq_sep);
        end
        X.Ap2p = newSUJ.All{i_ele}.Ap2p(selection);
        X.trans_freq = newSUJ.All{i_ele}.trans_freq(selection);
        X.moy_freq = newSUJ.All{i_ele}.moy_freq(selection);
        X.mean_Ap2p = mean(X.Ap2p);
        X.sigm_Ap2p = std(X.Ap2p);
        X.mean_trfr = mean(X.trans_freq);
        X.sigm_trfr = std(X.trans_freq);
        X.mean_mofr = mean(X.moy_freq);
        X.sigm_mofr = std(X.moy_freq);
        X.electrode = newSUJ.All{i_ele}.electrode;
        X.OL_densite = numel(X.Ap2p) / newSUJ.All{i_ele}.duree;
        X.percentage_of_OL = 100*numel(X.Ap2p)/numel(newSUJ.All{i_ele}.Ap2p);
        X.stade = 'N2N3';
        
        ligne = [newSUJ.All{i_ele}.Nbr_trials , ...
                 numel(X.Ap2p) , ...
                 X.percentage_of_OL , ...
                 X.mean_Ap2p , ...
                 X.sigm_Ap2p , ...
                 X.mean_mofr , ...
                 X.sigm_mofr , ...
                 X.mean_trfr , ...
                 X.sigm_trfr ];
        
        if i == 1, % slow
            newSUJ.Slow{i_ele} = X;
            LIGNE_SLOW{i_ele} = ligne;
        else % fast
            newSUJ.Fast{i_ele} = X;
            LIGNE_FAST{i_ele} = ligne;
        end
    end
end
              
end
