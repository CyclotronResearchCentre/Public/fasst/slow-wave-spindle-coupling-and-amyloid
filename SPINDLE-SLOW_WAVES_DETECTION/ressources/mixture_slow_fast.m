

function [mix, f_sep] = mixture_slow_fast(fi,fmin_max)

    %%%%%%%%%%%%%%%%%%%%%%%
    % fM = min(4,max(fi));%
    % fm = min(fi);       %
    %%%%%%%%%%%%%%%%%%%%%%%
    fm = fmin_max(1);
    fM = fmin_max(2);
    f_samp = linspace(fm,fM,150);
    % Mixture:
    GG = fitgmdist(fi,2,'options',statset('MaxIter',1500','TolFun',1e-5));
    % gauss1
    mu1 = GG.mu(1);
    si1 = sqrt(GG.Sigma(1));
    p1 = round(100*GG.ComponentProportion(1));
    A1 = GG.ComponentProportion(1)/sqrt(2*pi)/si1;
    g1 = A1*exp(-0.5*(f_samp-mu1).^2/si1^2);
    %gauss2
    mu2 = GG.mu(2);
    si2 = sqrt(GG.Sigma(2));
    p2 = round(100*GG.ComponentProportion(2));
    A2 = GG.ComponentProportion(2)/sqrt(2*pi)/si2;
    g2 = A2*exp(-0.5*(f_samp-mu2).^2/si2^2);
    
    % Separation: %%%%%%%%%%%%%%%%%%%%%%%
    [~,m1] = find(g1>g2,1,'first');     %
    [~,M1] = find(g1>g2,1,'last');      %
    [~,m2] = find(g2>g1,1,'first');     %
    [~,M2] = find(g2>g1,1,'last');      %
    sep_n = min(max(m1,M1),max(m2,M2)); %
    f_sep = f_samp(sep_n);              %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    % en sortie:
    mix.G1.fsamp = f_samp;
    mix.G1.p1 = p1;
    mix.G1.A1 = A1;
    mix.G1.g1 = g1;
    mix.G2.fsamp = f_samp;
    mix.G2.p1 = p2;
    mix.G2.A1 = A2;
    mix.G2.g1 = g2;
    
end


