function ssw = phase_of_SSW(signals,ssw,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% ssw contains the markers of the ssw for one subject (for each trials)
% ssw{a}.n_zc_critere_temporel
%       .PaP
% (pour le trial a)
% OPTIONS_SLEEP contains the parameters for the SSW detection

% output:
% phase of the ssw

fs   = signals.fs;
data = signals.data;
[Ntrials,~] = size(data);
% parametres du filtre (SSW)
if     strcmpi(OPTIONS.Sleep.SSW.method,'carrier')
       wn = OPTIONS.Sleep.SSW.Carrier.fmin_max / (fs/2);
elseif strcmpi(OPTIONS.Sleep.SSW.method,'molle')
       wn = OPTIONS.Sleep.SSW.Molle.fmin_max / (fs/2);
else   fprintf('\n!!!! warning: we take Carrier definition (SSW)');
       wn = OPTIONS.Sleep.SSW.Carrier.fmin_max / (fs/2);
end
    
ssw_filter = fir1(150,wn);

% on passe en revue les trials:
for it = 1:Ntrials
    sig = double(data(it,:));
    sso = ssw{it};
    % filtrage dans la bande SSW
    sigf = filtfilt(ssw_filter,1,sig);
    sigh = hilbert(sigf);
    phi  = unwrap(angle(sigh));
    % we look at the ssw segments:
    for i_so = 1:length(sso.PaP)
        d = sso.n_t(i_so,1);
        f = sso.n_t(i_so,2);
        ssw{it}.sigf{i_so} = sigf(d:f);
        ssw{it}.siga{i_so} = abs(sigh(d:f));
        ssw_phase    = phi(d:f);
        % we shift the phase so that phase = 0 at the max of SSW:

        [~,b] = max(ssw{it}.sigf{i_so});
        phase_shift = ssw_phase(b);
        ssw{it}.phase{i_so} = ssw_phase-phase_shift;
        ssw{it}.phase_shift_max = phase_shift;
        [~,b] = min(ssw{it}.sigf{i_so});
        ssw{it}.phase_shift_min = ssw_phase(b); % in case we want to use 
        % the min as the 0 phase (then we consider 
        % ssw{it}.phase{i_so} = ssw{it}.phase{i_so} +
        % ssw{it}.phase_shift_max - ssw{it}.phase_shift_min
    end
end
    
end
