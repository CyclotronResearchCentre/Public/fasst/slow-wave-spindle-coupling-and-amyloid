function newO = set_up_Detector(O)
newO = O;
fprintf('\n');
% Utilise-t-on des seuils adaptes?
        % Seuils standard de Carrier
        for i_gr = 1:numel(O.DBase.groupes)
            newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 75;
            newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 40;
        end
        answ = 'n'; answ = input(' --- Adapted SSW thresholds (Thaina) ? ([n]/y) ','s');
        if strcmpi(answ,'y')
            for i_gr = 1:numel(O.DBase.groupes)            
            if strcmpi(O.DBase.groupes{i_gr},'middle')
            newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 65;
            newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 34.5;
            end
            if strcmpi(O.DBase.groupes{i_gr},'young')
            newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP = 75.5;
            newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg = 40;
            end
            fprintf('\n     New thresholds for SSW detection in %s : PaP = %3.2f and Neg = %3.2f',newO.DBase.groupes{i_gr},newO.DBase.SSW_Adapted_thresholds{i_gr}.PaP,newO.DBase.SSW_Adapted_thresholds{i_gr}.Neg);
            end
        else
        fprintf('\n     Standard thresholds for SSW detection');
        end
        fprintf('\n');pause(1);
end