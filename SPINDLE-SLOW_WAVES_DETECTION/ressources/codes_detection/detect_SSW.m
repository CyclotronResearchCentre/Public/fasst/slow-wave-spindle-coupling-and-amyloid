function [marker,signals,N_SSW] = detect_SSW(signals,OPTIONS)
% signals.data is a Ntrials x Ntime
% signas.fs    is the sampling rate
% OPTIONS.Sleep contains the parameters for the SPINDLE detection
% in particular, the OPTIONS.Sleep.SPL gives the paprameters of the
% detectors. TWO DETECTORS AVAILABLE: 
% OPTIONS.Sleep.SSW.method = 'Carrier' (the detector in Carrier et al. 2011)
% OPTIONS.Sleep.SSW.method = 'Molle' (the detector in Molle et al. 2011)

% output:
% marker------------ .n_zc : table N1(start) N2(end) and as much line than SW 
%                    .PaP : PaP amplitude of each SSW
% ... (Neg, Pos, durations...)

if     strcmpi(OPTIONS.Sleep.SSW.method(1:14),'Carrier0.3_4Hz')||strcmpi(OPTIONS.Sleep.SSW.method(1:15),'Carrier0.16_4Hz')
       [marker,signals,N_SSW] = detect_SSW_Carrier(signals,OPTIONS);
elseif strcmpi(OPTIONS.Sleep.SSW.method,'molle')
       [marker,signals,N_SSW] = detect_SSW_Molle(signals,OPTIONS);
else   fprintf('\n wrong call of the SSW detector, we quit... bye\n'); 
       return;
end
end