function SSW = integrate_SPL_in_SSW(SSW,SSW_SPL)

    for ig = 1:numel(SSW)
        for is = 1:numel(SSW{ig})

            SSW_SPLi = SSW_SPL{ig}{is};
            SSWi = SSW{ig}{is};

            for it = 1:numel(SSWi) % on parcourt les trial
                SSWi{it}.spdl = zeros(1,numel(SSWi{it}.PaP));
                if ~isempty(SSW_SPLi{it}.concordanceSTART)
                   listeOL = SSW_SPLi{it}.concordanceSTART(:,2)';
                   listeFx = SSW_SPLi{it}.concordanceSTART(:,1)';
                   SSWi{it}.spdl(listeOL) = listeFx;
                end    
            end
            SSW{ig}{is} = SSWi;
        end
    end
end

