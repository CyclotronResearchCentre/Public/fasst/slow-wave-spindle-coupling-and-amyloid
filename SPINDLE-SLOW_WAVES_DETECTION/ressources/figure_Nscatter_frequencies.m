function figure_Nscatter_frequencies(INVENTAIRE,ELECTRODES,OPTIONS)

% OPTIONS
% INVENTAIRE{elec}{stade} = [identif P2P NEG frMOY fTRA dureeDOWN dureeUP]
ind_moy = 6;
ind_tra = 7;
N_stade = numel(OPTIONS.DBase.Conditions);
Col = {'k' 'r' 'b'};

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Regroupement des electrodes F,C,P          %
    Nele = numel(ELECTRODES);                    %
    F = []; C = []; P = [];                      %
    for j = 1:Nele                               %
        if strcmpi(ELECTRODES{j}(1),'f')         %
            F = [F,j];                           %
        elseif strcmpi(ELECTRODES{j}(1),'c')     %
            C = [C,j];                           %
        elseif strcmpi(ELECTRODES{j}(1),'p')     %
            P = [P,j];                           %
        end                                      %
    end                                          %
    R = {F C P};                                 %
    name_R = {'Frontal' 'Central' 'Parietal'};   %
    for r = 1:numel(R)                           %
        if numel(R{r})==0                        %
            R(r) = [];                           %
            name_R(r) = '';                      %
        end                                      %
    end                                          %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Pour chaque regroupements: Regroupement                     %
    % des sujets, par stades                                      % 
    for r = 1:numel(R)                                            %
        FRE{r}.Gr = [];                                           %
        FRE{r}.fx = [];                                           %
        FRE{r}.fy = [];                                           %
        FRE{r}.name_Color = {};                                   %
        FRE{r}.Color = {};                                        %
        for st = 1:N_stade                                        %
            fx{st} = []; fy{st} = [];                             %
            for k = R{r}                                          %
            fx{st} = [fx{st} ; INVENTAIRE{k}{st}(:,ind_moy)];     %
            fy{st} = [fy{st} ; INVENTAIRE{k}{st}(:,ind_tra)];     %
            end                                                   %
            gri = cell(1,numel(fx{st}));                          %
            gri(1,:) = {OPTIONS.DBase.Conditions{st}};            %
            FRE{r}.Gr = cat(2,FRE{r}.Gr,gri);                     %
            FRE{r}.fx = cat(1,FRE{r}.fx,fx{st});                  %
            FRE{r}.fy = cat(1,FRE{r}.fy,fy{st});                  %
            FRE{r}.name_Color{st} = OPTIONS.DBase.Conditions{st}; %
            FRE{r}.Color{st} = Col{st};                           %
        end                                                       %
        FRE{r}.name = name_R{r};                                  %
    end                                                           %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    for r = 1:numel(FRE), figure_make_scatter(FRE{r}); end

    
end   
    
function figure_make_scatter(FRE)

fX = FRE.fx;
fY = FRE.fy;
groups = FRE.Gr;
name = FRE.name;
Col = FRE.Color;
nCol = FRE.name_Color;

% FIGURE:
ff = figure('position',[10 10 400 400]);
my_scatterhist(fX,fY, ...
               'Group',groups', ...
               'Nbins',60,...
               'kernel','on',...
               'linestyle',{'-' '-'},...
               'Marker','.','MarkerSize',1,...
               'Legend','off',...
               'Color',FRE.Color); 
    hold on; set(gca,'Color','none');
    xlim([0 4]); ylim([0 4]);
    xlabel('frequency (Hz)','FontSize',14);
    ylabel('transition frequency (Hz)','FontSize',14);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    Offx = -1.5;                        %
    Offy = -0.5;                        %
    Doff = -0.3;                        %
    for i = 1:numel(FRE.Color)          %
        text(Offx,Offy+(i-1)*Doff, ...  %
            FRE.name_Color{i}, ...      %
            'Color',FRE.Color{i}, ...   %
            'fontsize',12);             %
    end                                 %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    title(name,'FontSize',16)
    hold off
end