function [PAC] = calcul_PAC(TRD,MKR)
% TDR = TR.D{std}{ele};
% MKR = SSW.markers{ele}{std};
%
% On utilisse:
%   MKR.markers{epo}
%   TDR.data(epo,:);

% Inventaire des OL:
for i = 1:numel(MKR.markers)
    liste_ssw(i) = numel(MKR.markers{i}.PaP);
end
liste = find(liste_ssw);

    % filtres
    fs = TRD.fs;
    wn_sigma = [10 16] / (fs/2);
    sigma_filter = fir1(round(4/3*fs),wn_sigma);
    wn_delta = [0.3 4] / (fs/2);
    delta_filter = fir1(round(4/3*fs),wn_delta);
    
    
data = TRD.data(liste,:);
[Ntrials,~] = size(data);
data_f_sigma = (filtfilt(sigma_filter,1,data'))';
hilbert_sigma = (hilbert(data_f_sigma'))';
Amp_sigma = abs(hilbert_sigma);

data_f_delta = (filtfilt(delta_filter,1,data'))';
hilbert_delta = (hilbert(data_f_delta'))';
Phs_delta = hilbert_delta ./ abs(hilbert_delta);

k = 1;
for i = 1:numel(liste)
    epo = liste(i);
    for j = 1:liste_ssw(epo)
        mrk = MKR.markers{epo}.n_t(j,:);
        D = mrk(1):mrk(2);
        PAC.pac(k) = sum(Amp_sigma(D) .* Phs_delta(D)) / numel(D);
        [zmean,zsigm] = randomize_PAC(Amp_sigma(D),Phs_delta(D));
        PAC.zPAC(k) = (abs(PAC.pac(k) - zmean))/zsigm;
        PAC.ftr(k) =  MKR.markers{epo}.tfr(j);
        k = k+1;
    end
end

end

function [zmean,zsigm] = randomize_PAC(Amp,Phs)
N = numel(Amp);
for i = 1:500
    u = randperm(N);
    pac(i) = abs(sum(Amp .* Phs(u))) / N;
end
zmean = mean(pac);
zsigm = std(pac);
end



