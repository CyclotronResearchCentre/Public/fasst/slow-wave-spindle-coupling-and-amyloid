function INVENTAIRE23 = fusion23(INVENTAIRE,k2,k3)

    Nele = numel(INVENTAIRE);
    for i = 1:Nele
        INVENTAIRE23{i}{1} = INVENTAIRE{i}{1};
        INVENTAIRE23{i}{2} = [INVENTAIRE{i}{k2} ; ... 
                              INVENTAIRE{i}{k3}];
    end

end