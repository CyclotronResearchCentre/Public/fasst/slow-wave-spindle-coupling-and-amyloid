function [All, ligne] = inventaire_ssw(markers)

% SSW.markers{1}{2}: 
%     markers: {1x76 cell}
%     signals: [1x1 struct]
%     electro: 'f3'
%       stage: 'Stade3'
%     identif: [1 1]
%     densite: 17.5789
%       duree: 38

% markers{trial}: 
%     Thresholds_PaP_Neg: [61 33]
%                    n_t: [5x2 double]
%                    PaP: [79.2222 104.4761 87.4319 61.0696 116.4555]
%                    Neg: [51.6069 51.9487 64.9356 35.4691 55.3789]
%                    tNe: [339.8438 320.3125 300.7812 332.0312 484.3750]
%                    tPo: [562.5000 453.1250 253.9062 464.8438 488.2812]
%                PaP_raw: [118.7822 121.4741 159.3449 103.0282 170.9387]
%                Neg_raw: [63.2122 60.5202 113.0438 58.8990 85.6349]
%                    mfr: [1.0940 1.2736 1.7655 1.2367 1.0159]
%                    tfr: [1.2190 2.0984 1.5422 0.9846 1]

    Ap2p = []; Trfr = []; Mofr = []; D_down = []; D_up = [];
    All.duree = 0; All.Nbr_trials = 0;
	for i_std = 1:numel(markers) % on cumule les stades
        mark = markers{i_std}.markers;
        
        Nbr_trials = numel(mark);
        All.Nbr_Epoques(i_std) = Nbr_trials;
        All.Nbr_trials = All.Nbr_trials + Nbr_trials;
        
        All.duree = All.duree + markers{i_std}.duree;
        for i_tr = 1:Nbr_trials
            Ap2p = cat(2,Ap2p,mark{i_tr}.PaP);
            Trfr = cat(2,Trfr,mark{i_tr}.tfr);
            Mofr = cat(2,Mofr,mark{i_tr}.mfr);
            D_down = cat(2,D_down,mark{i_tr}.tNe);
            D_up = cat(2,D_up,mark{i_tr}.tPo);
        end
    end

        All.Ap2p       = Ap2p;
        All.trans_freq = Trfr;
        All.moy_freq   = Mofr;
        All.mean_Ap2p  = mean(Ap2p);
        All.sigm_Ap2p  = std(Ap2p);
        All.mean_trfr  = mean(Trfr);
        All.sigm_trfr  = std(Trfr);
        All.mean_mofr  = mean(Mofr);
        All.sigm_mofr  = std(Mofr);
        All.mean_Dup   = mean(D_up);
        All.sigm_Dup   = std(D_up);
        All.mean_Ddown = mean(D_down);
        All.sigm_Ddown = std(D_down);
        All.electrode  = markers{1}.electro;
        All.OL_densite = numel(All.Ap2p) / All.duree;
        All.stade      = 'N2N3';
        
% TABLE:
ligne = [All.Nbr_trials , ...
         numel(All.Ap2p) , ...
         All.OL_densite , ...
         All.mean_Ap2p , ...
         All.sigm_Ap2p , ...
         All.mean_mofr , ...
         All.sigm_mofr , ...
         All.mean_trfr , ...
         All.sigm_trfr , ...
         All.mean_Ddown , ...
         All.sigm_Ddown , ...
         All.mean_Dup , ...
         All.sigm_Dup];

end

