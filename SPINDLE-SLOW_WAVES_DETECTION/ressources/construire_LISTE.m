function [LISTE,LISTE_CAL,LISTE_FILE] = construire_LISTE(dd, uu) 

LISTE = {}; % OPTIONS.DBase.listes.sujets; % (defaut)
            % GROUP = OPTIONS.GROUP;
LISTE_CAL = {};
LISTE_FILE = {};

    vv = dir([dd filesep '*liste*.txt']);
    if ~isempty(vv)
        fiD = fopen([dd filesep vv(1).name],'r');
        ll = fgetl(fiD);
        ll = fgetl(fiD); k = 1;
        while ischar(ll)
            LISTE{k} = ll;
            k = k+1;
            ll = fgetl(fiD);
        end
    end
    if ~isempty(LISTE)
        l = 1;
        for i = 1:numel(LISTE)
            n = length(LISTE{i});
            for j = 1:numel(uu)
            nf = regexp(uu(j).name,LISTE{i});
            if ~isempty(nf)
                LISTE_CAL{l} = LISTE{i}; 
                LISTE_FILE{l} = uu(j).name;
                l = l+1; break;
            end
            end
        end
    else
        for j = 1:numel(uu)
            LISTE_CAL{j}  = uu(j).name(1:end-3);
            LISTE_FILE{j} = uu(j).name;
        end
    end
    
end