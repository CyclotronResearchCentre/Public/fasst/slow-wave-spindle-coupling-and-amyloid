function [PAC] = calcul_PAC_002(TRD,MKR)
% TDR = TR.D{std}{ele};
% MKR = SSW.markers{ele}{std};
%
% On utilisse:
%   MKR.markers{epo}
%   TDR.data(epo,:);

% Inventaire des OL:
for i = 1:numel(MKR.markers)
    liste_ssw(i) = numel(MKR.markers{i}.PaP);
end
liste = find(liste_ssw);

    % filtres
    fs = TRD.fs;
    wn_sigma = [10 16] / (fs/2);
    sigma_filter = fir1(round(4/3*fs),wn_sigma);
    wn_delta = [0.3 4] / (fs/2);
    delta_filter = fir1(round(4/3*fs),wn_delta);
    
    
data = TRD.data(liste,:);
% filtre sigma:
data_f_sigma = (filtfilt(sigma_filter,1,data'))';
hilbert_sigma = (hilbert(data_f_sigma'))';
Amp_sigma = abs(hilbert_sigma);
% filtre delta:
data_f_delta = (filtfilt(delta_filter,1,data'))';
hilbert_delta = (hilbert(data_f_delta'))';
Phs_delta = hilbert_delta ./ abs(hilbert_delta);

k = 1;
for i = 1:numel(liste)
    epo = liste(i);
    for j = 1:liste_ssw(epo)
        mrk = MKR.markers{epo}.n_t(j,:);
        D = mrk(1):mrk(2);
        PAC.pac(k) = sum(Amp_sigma(i,D) .* Phs_delta(i,D)) / numel(D);
        [zmean,zsigm] = randomize_PAC(data(i,:),Phs_delta(i,:),D,sigma_filter);
        PAC.zPAC(k) = (abs(PAC.pac(k) - zmean))/zsigm;
        PAC.ftr(k) =  MKR.markers{epo}.tfr(j);
        k = k+1;
    end
end

end

function [zmean,zsigm] = randomize_PAC(data,Phs,D,filter)

N = length(data);
n = numel(D);
randata = zeros(500,N);
for i = 1:500, randata(i,:) = data(randperm(N)); end
    data_f_sigma = (filtfilt(filter,1,randata'))';  
    hilbert_sigma = (hilbert(data_f_sigma'))';
    Amp_sigma = abs(hilbert_sigma);
    pac = abs(sum(Amp_sigma(:,D) .* repmat(Phs(D),500,1) ,2)) / n;
zmean = mean(pac);
zsigm = std(pac);
end



