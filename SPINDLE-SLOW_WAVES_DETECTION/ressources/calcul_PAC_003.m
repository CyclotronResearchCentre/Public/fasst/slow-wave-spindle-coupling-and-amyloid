function [PAC] = calcul_PAC_003(SW,filters)
% SW.signal (original signal)
% SW.fs (sampling frequency)
% SW.mkr (start, end of the slow_wave)

% filters.L (low pass filter)
% filters.H (High pass)


    % filters
    fs = SW.fs;
    wn_sigma = filters.H / (fs/2);
    sigma_filter = fir1(round(4/3*fs),wn_sigma);
    wn_delta = filters.L / (fs/2);
    delta_filter = fir1(round(4/3*fs),wn_delta);
    
    
    data = SW.signal;
    % filtre sigma:
    data_f_sigma = (filtfilt(sigma_filter,1,data'))';
    hilbert_sigma = (hilbert(data_f_sigma'))';
    Amp_sigma = abs(hilbert_sigma);
    % filtre delta:
    data_f_delta = (filtfilt(delta_filter,1,data'))';
    hilbert_delta = (hilbert(data_f_delta'))';
    Phs_delta = hilbert_delta ./ abs(hilbert_delta);
    
    a = 1; b = numel(data);
    if isfield(SW,'mkr'), a = SW.mkr(1); b = SW.mkr(2); end
    PHS_SWa = Phs_delta(a:b);
    AMP_SIg = Amp_sigma(a:b);
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Unbiased PAC:                                         %
    Ma = mean(AMP_SIg);                                     %
    Mp = mean(PHS_SWa);                                     %
    dPAC = abs(1/numel(data)*sum(AMP_SIg.*PHS_SWa) - Ma*Mp); %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    [zmean,zsigm] = randomize_PAC(SW.signal,PHS_SWa,a:b,sigma_filter);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    PAC.dPAC = dPAC;                %
    PAC.zPAC = (dPAC-zmean)/zsigm;  %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

end

% function [zmean,zsigm] = randomize_PAC(data,Phs,D,filter)
% 
% N = length(data);
% n = numel(D);
% data_f_sigma = zeros(300,N);
% % Shuffling:
% for i = 1:300, data_f_sigma(i,:) = ...
%         (filtfilt(filter,1,(data(randperm(N)))'))'; 
% end
%     % PAC (in //):
%     hilbert_sigma = (hilbert(data_f_sigma'))';
%     Amp_sigma = abs(hilbert_sigma);
%     pac = abs(sum( Amp_sigma(:,D) .* repmat(Phs,300,1) ,2)) / n;
% 
%     zmean = mean(pac);
% zsigm = std(pac);
% end

function [zmean,zsigm] = randomize_PAC(data,Phs,D,filter)

N = length(data);
n = numel(D);
hilbert_sigma = zeros(200,n);
rand_offset = randi([1,N-n-1],1,200);
rand_data   = data(randperm(N));
randi_sigma = filtfilt(filter,1,rand_data')';
randi_hilbert = (hilbert(randi_sigma'))';
% Shuffling:
for i = 1:200, hilbert_sigma(i,:) = randi_hilbert(rand_offset(i):rand_offset(i)+n-1); 
end
    % PAC (in //):
    Amp_sigma = abs(hilbert_sigma);
    pac0 = sum( Amp_sigma .* repmat(Phs,200,1) ,2) / n;
    pacA = mean(Amp_sigma,2);
    pacP = mean(Phs);
    pac = abs(pac0 - pacA*pacP);
    zmean = mean(pac);
    zsigm = std(pac);
end

