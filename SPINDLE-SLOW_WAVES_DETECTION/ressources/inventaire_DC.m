function OPTIONS = inventaire(D,d,OPTIONS)

% Channels
Nbre_canaux  = size(d,1);
Nbre_echant  = size(d,2);
liste_canaux = {};
ok = [];
for i = 1:numel(D.channels)
    OPTIONS.Canaux.liste_canaux{i} = D.channels(i).label;
end
fprintf('    ... Recherche des derivations\n');
for k = 1:numel(OPTIONS.Canaux.derivations)
    ok(k) = 0; 
        for i = 1:numel(OPTIONS.Canaux.liste_canaux), 
            if strcmpi(OPTIONS.Canaux.derivations{k},...
                       OPTIONS.Canaux.liste_canaux{i}); 
                   ok(k) = i; break; 
            end; 
        end
    if ok(k), fprintf('    --> derivation %s trouvee\n',...
              OPTIONS.Canaux.derivations{k});
    end
end
OPTIONS.Canaux.derivations_ok = ok;

% Selection des canaux:
d = d(OPTIONS.Canaux.derivations_ok,:);
% Pagination
information = D.other.CRC;
OPTIONS.Signal.page = information.score{3};
fprintf('    ... Page: %d secondes\n',OPTIONS.Signal.page);
OPTIONS.Signal.fs = D.Fsample;
fprintf('    ... Frequence echant.: %d Hz\n',OPTIONS.Signal.fs);
% Scores
liste_scores = D.other.CRC.score{1};
scores_dispo = unique(liste_scores);
fprintf('    ... Scores disponibles: %s, sur chaque page\n',num2str(scores_dispo));
fprintf('        Scores interpretes: 0 -> wake\n');
fprintf('        Scores interpretes: 1 -> N1\n');
fprintf('        Scores interpretes: 2 -> N2\n');
fprintf('        Scores interpretes: 3 -> N3\n');
fprintf('        Scores interpretes: 5 -> REM\n');
fprintf('        Les cycles seront definis par des segments precedents chaque REM\n\n');
OPTIONS.Scores.hypnogramme = liste_scores; 
% Donnees
[~,Nsamp] = size(d);
Nsamp_page = ceil(OPTIONS.Signal.fs * OPTIONS.Signal.page);
K = 1:Nsamp_page:Nsamp;

% FIX: Issue with some subjects as scoring goes until LON, not until end of
% recording
K_scored = K(1:length(liste_scores));
% for i = 1:length(K)-1, OPTIONS.Data.pages(i,1) = liste_scores(i); OPTIONS.Data.pages(i,2) = K(i); OPTIONS.Data.pages(i,3) = K(i+1)-1; end;
for i = 1:length(K_scored)-1, OPTIONS.Data.pages(i,1) = liste_scores(i); OPTIONS.Data.pages(i,2) = K(i); OPTIONS.Data.pages(i,3) = K(i+1)-1; end;


OPTIONS.Data.Comments = 'score,debut(samp),fin(samp)';
% Artefacts
fprintf('    ... Les Artefacts\n');
A = D.other.CRC.score{6};
liste_A = [];
for i = 1:size(A,1)
    if ismember(A(i,3),OPTIONS.Canaux.derivations_ok)
        pg1 = A(i,1)*OPTIONS.Signal.fs;
        pg2 = A(i,2)*OPTIONS.Signal.fs;
        u = find(OPTIONS.Data.pages(:,2)< pg1,1,'last');
        v = find(OPTIONS.Data.pages(:,3)>=pg2,1,'first');
        liste_A = [liste_A u v];
%         fprintf('        Electrode %d: artefact sur trials %d -> %d \n',A(i,3),u,v);
    end
end
liste_A = unique(liste_A);
fprintf('    ... On a trouve %d pages artefactees: ',length(liste_A));
OPTIONS.Data.pages(liste_A,:)=[];

B = D.other.CRC.score{5};
B(:,3) = B(:,2)-B(:,1);
liste_B = [];
for i = 1:size(B,1)
    if B(i,3) > 5
        pg1 = B(i,1)*OPTIONS.Signal.fs;
        pg2 = B(i,2)*OPTIONS.Signal.fs;
        u = find(OPTIONS.Data.pages(:,2)< pg1,1,'last');
        v = find(OPTIONS.Data.pages(:,3)>=pg2,1,'first');
        liste_B = [liste_B u:v];
%         fprintf('        arousal sur trials %d -> %d \n',u,v);
    end
end
liste_B = unique(liste_B);
fprintf('    ... On a trouve %d pages d''arousal : ',length(liste_B));
OPTIONS.Data.pages(liste_B,:)=[];
fprintf('il reste %d pages clean...\n\n\n',size(OPTIONS.Data.pages,1));
end