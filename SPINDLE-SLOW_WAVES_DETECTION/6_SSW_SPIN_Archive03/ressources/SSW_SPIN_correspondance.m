function [ SSW_SPL, Phases_START, Phases_POW] = SSW_SPIN_correspondance( SSW,SPIN)

Nbre_trials     = numel(SPL.delimiters);
SSW_SPL         = cell(1,Nbre_trials);
Phases_START    = [];
Phases_POW      = [];

for it = 1:Nbre_trials
    
    SSW_SPL{it}.comment = '[spdl ssw], phase';
    SSW_SPL{it}.concordanceSTART = [];
    SSW_SPL{it}.concordancePOW   = [];
    SSW_SPL{it}.phase_max0_spdl_start = [];
    SSW_SPL{it}.phase_max0_spdl_pow   = [];
    
    if ~isempty(SPL.delimiters{it})
        spdl_start = SPL.delimiters{it}(:,1);
        spdl_pow   = SPL.maxPower{it}(:,1);
        ssw_interv = SSW{it}.n_t;
        Nbre_spindles = numel(spdl_start);
        Nbre_ssw = size(ssw_interv,1);
        k1 = 1; k2 = 1;
        
        for i = 1:Nbre_spindles
            for j = 1:1:Nbre_ssw
                if (spdl_start(i)>=ssw_interv(j,1))&&(spdl_start(i)<=ssw_interv(j,2));
                    SSW_SPL{it}.concordanceSTART(k1,:) = [i j]; % on marque ici qui avec qui
                    SSW_SPL{it}.phase_spdl_start(k1) = SSW{it}.phase{j}(1+spdl_start(i)-ssw_interv(j,1)); % phase dans l'OL
                    Phases_START = cat(1,Phases_START,SSW_SPL{it}.phase_spdl_start(k1)); % on les recupere toutes ici

                    k1 = k1+1;
                end
                if (spdl_pow(i)>=ssw_interv(j,1))&&(spdl_pow(i)<=ssw_interv(j,2));
                    SSW_SPL{it}.concordancePOW(k2,:) = [i j]; 
                    SSW_SPL{it}.phase_spdl_pow(k2) = SSW{it}.phase{j}(1+spdl_pow(i)-ssw_interv(j,1)); % phase dans l'OL
                    Phases_POW = cat(1,Phases_POW,SSW_SPL{it}.phase_spdl_pow(k2)); % On es recupere toutes ici

                    k2 = k2+1;
                end
                
            end
        end
    end
end
end

