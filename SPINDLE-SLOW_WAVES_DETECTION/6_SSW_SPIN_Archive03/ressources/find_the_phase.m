function [phi,v] = find_the_phase(fsignal,TABLE) 

    a = TABLE(1,6);
    b = TABLE(1,7);

    r = TABLE(2,6) - a;
    s = TABLE(2,7) - a;
    
    type = TABLE(1,8); % 1 = slow %
    type(type==0) = -1;% -1 = fast %
    
    d = b-a;

    v = 0;
    phi.start = 9999;
    phi.power = 9999;

    if r>0 && r<=d
        v = type;
        X = hilbert(fsignal);
        X = X(a:b);
        P = unwrap(angle( X./abs(X) ));
        [~,m] = max(real(X)); 
        P = (P - P(m))/pi;

        phi.start = P(r);
        if s<=d, phi.power = P(s); v = 2*type; end
    end

end