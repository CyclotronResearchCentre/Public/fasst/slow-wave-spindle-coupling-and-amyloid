% function analyse_SSW_SPIN_onset_coupling()

clc
clear
addpath(genpath('./ressources'))
fprintf(' ===== Analyse 003: SPINDLE-SSW Coupling ====\n');
fprintf('       Phase of the spindle over the SSW\n\n');
fprintf(' We consider the coincidence btw ssw and spindles\n')
fprintf(' The coincidence will be defined with the phase\n')
fprintf(' of the SSW, givent that phase=0 corresponds to\n')
fprintf(' the MAXIMUM DEPOLARIZATION of the slow wave. The\n')
fprintf(' phase is in unit of pi.\n\n')

fprintf(' --  Looking for a cohorte XXX_mat\n')
dd = uigetdir('./');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
uu = dir([dd filesep 'Results' filesep '001_inventaire_SWITCHERS*']);     %
if ~isempty(uu)                                                           %
    my_folder = uu(1).folder;                                             %
    my_file = uu(1).name;                                                 %
    if numel(uu)>1                                                        %
        fprintf('     Ambiguity of the INVENTORY, please select\n');      %
        my_file = uigetfile([dd filesep 'Results']);                      %
    end                                                                   %
    fprintf(' --> we read %s (for the frequency thresholds: ',my_file);   %
    load([my_folder filesep my_file],'f_sep0');                           %
    fprintf('%4.2f , %4.3f\n',f_sep0{1},f_sep0{2});                       %
else                                                                      %
    fprintf(' 001_inventaire_SWITCHERS not available. Bye\n');            %
    return;                                                               %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vv = dir([dd filesep 'DBase_SSW' filesep 'SW*']);
if isempty(vv)
    fprintf(' DBase_SSW empty. Bye\n'); 
    return; 
else
    Nsuj = numel(vv);
    fprintf(' --> we found %d subjects in this cohort\n',Nsuj);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dd_ssw_spin = [dd filesep 'DBase_SSW_SPIN'];           %
if isfolder(dd_ssw_spin), rmdir(dd_ssw_spin,'s'); end  %
mkdir(dd_ssw_spin);                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SSW_SPINs = cell(1,Nsuj);
for isuj = 1:Nsuj

    ssw_file = vv(isuj).name;
    A = strsplit(ssw_file,'_');
        fprintf('\n --> we consider the SLOW WAVES of %s\n',A{2}(1:end-3));
        ww = dir([dd filesep 'DBase_SPIN' filesep 'SPIN_' A{2}]);
        if isempty(ww)
            fprintf(' DBsae_SPIN not found.\n'); 
            break; 
        end  
    fprintf(' --> we consider the SPINDLES of %s\n',A{2}(1:end-3));
    spin_file = ww.name;

    fprintf('     we read the SLOW WAVES ...');
    load([vv(1).folder filesep ssw_file],'SSW','O')
    fprintf(' done\n');
    fprintf('     we read the SPINDLES ...');
    load([ww.folder filesep spin_file],'SPIN')
    fprintf(' done\n');

    SSW_SPIN = find_coincidence_SSW_SPIN(SSW,SPIN,f_sep0);
    SSW_SPINs{isuj} = SSW_SPIN;
    save([dd_ssw_spin filesep 'SSW_SPIN_' A{2}],'SSW_SPIN','O');
    LISTE_SUJ{isuj} = A{2}(1:end-4);

end

% SSW_SPIN{suj}{ele}{con}
%          coincidence: {1�35 cell}
%                touch: {1�35 cell}
%     true_coincidence: [2 1 0 1 0 0 .... 2 0]

% SSW_SPIN{1}{ele}{con}.coincidence{coincidence}: table
%   ele con epoq i_ol prime debut fin 1=slow
%   ele con epoq i_fs prime debut fin indice_power




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regroupement d'electrodes:                                                          %
k = 1; l = 1; q = 1; v = 1;                                                           %
regroupement(1).name = 'Central';                                                     %
regroupement(2).name = 'Frontal';                                                     %
% regroupement(3).name = 'Parietal'; DC                                                   %
% regroupement(4).name = 'Temporal';   DC                                                 %
for i = 1:numel(O.DBase.channel)                                                      %
    if     strcmpi(O.DBase.channel{i}(1),'C'), regroupement(1).ele(k) = i; k = k+1;   %
    elseif strcmpi(O.DBase.channel{i}(1),'F'), regroupement(2).ele(l) = i; l = l+1;   %
%     elseif strcmpi(O.DBase.channel{i}(1),'P'), regroupement(3).ele(q) = i; q = q+1;   %
%     elseif strcmpi(O.DBase.channel{i}(1),'T'), regroupement(4).ele(v) = i; v = v+1;   %
    end                                                                               %
end                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

COND   = numel(O.DBase.Conditions);
fprintf('\n\n');

for con = 1:COND %%%%%%%% On visite les conditions %%%%%%%%%%%%%%%%%%%%%%%
% On regroupe les electrodes:
    PHASES = cell(1,numel(regroupement));

    for i = 1:numel(regroupement)
        fprintf(' -->  GROUPE ELECTRODES: %s\n',regroupement(i).name);

        PHASES{i}.all_slow_coin = [];
        PHASES{i}.all_fast_coin = [];
        PHASES{i}.all_slow_deph = [];
        PHASES{i}.all_fast_deph = [];
        
        for j = 1:Nsuj % On regarde sujet par sujet
        
    
%             STAT2BESAVE_PHASES{i}(j,:) = NaN(1,4);
%             STAT2BESAVE_SSW_FX{i}(j,:) = NaN(1,7);
            slow_coin = []; 
            fast_coin = [];
            slow_deph = [];
            fast_deph = [];
            NOL = 0;
            NSW = 0;
            NFS = 0;
            NFX = 0;
            Nco = 0;
            NcS = 0;
            NcF = 0;
            
            
            u = 1; v = 1; r = 1; w = 1; % compteurs (a travers les electrodes)
            
            for ele = 1:numel(regroupement(i).ele) % on integre les electrodes %%%%%%%%%%%
            i_ele = regroupement(i).ele(ele);                                            % 
                                                                                         %
            slow_sw = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence>0);                 %
            fast_sw = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence<0);                 %
            slow_de = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence>1);                 %
            fast_de = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence<-1);                %
                                                                                         %
                    for a = 1:numel(slow_sw)                                             %
                    slow_coin(u) = SSW_SPINs{j}{i_ele}{con}.touch{slow_sw(a)}.start;     %
                    u = u+1;                                                             %
                    end                                                                  %
                                                                                         %
                    for a = 1:numel(fast_sw)                                             %
                    fast_coin(v) = SSW_SPINs{j}{i_ele}{con}.touch{fast_sw(a)}.start;     %
                    v = v+1;                                                             %
                    end                                                                  %
                                                                                         %
                    for a = 1:numel(slow_de)                                             %
                    slow_deph(r) = SSW_SPINs{j}{i_ele}{con}.touch{slow_de(a)}.power - ...%
                        SSW_SPINs{j}{i_ele}{con}.touch{slow_de(a)}.start ;               %
                    r = r+1;                                                             %
                    end                                                                  %
                                                                                         %
                    for a = 1:numel(fast_de)                                             %
                    fast_deph(w) = SSW_SPINs{j}{i_ele}{con}.touch{fast_de(a)}.power - ...%
                        SSW_SPINs{j}{i_ele}{con}.touch{fast_de(a)}.start ;               %
                    w = w+1;                                                             %
                    end                                                                  %
                                                                                         %
                    NOL = NOL + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_OLentes;            %
                    NSW = NSW + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_SlowS;              %
                    NFS = NFS + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_FastS;              %
                    NFX = NFX + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_Spind;              %
                    Nco = Nco + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_coincidences;       %
                    NcS = NcS + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_coincidences_SlowS; %
                    NcF = NcF + SSW_SPINs{j}{i_ele}{con}.STAT.nombre_coincidences_FastS; %
                                                                                         %   
            end                                                                          %
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            %%%%% mode principal des fast/slow %%%
            [a,b] = hist(slow_coin);
                slow_mode = mean(b(a==max(a)));
            [a,b] = hist(fast_coin);
                fast_mode = mean(b(a==max(a)));
            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
            
            PHASES{i}.all_slow_coin = [PHASES{i}.all_slow_coin slow_coin];
            PHASES{i}.all_fast_coin = [PHASES{i}.all_fast_coin fast_coin];
            PHASES{i}.all_slow_deph = [PHASES{i}.all_slow_deph slow_deph];
            PHASES{i}.all_fast_deph = [PHASES{i}.all_fast_deph fast_deph];
            PHASES{i}.slow_mode(j) = slow_mode;
            PHASES{i}.fast_mode(j) = fast_mode;
            PHASES{i}.slow_coin(j) = mean(slow_coin);
            PHASES{i}.fast_coin(j) = mean(fast_coin);
            PHASES{i}.slow_deph(j) = mean(slow_deph);
            PHASES{i}.fast_deph(j) = mean(fast_deph);
            PHASES{i}.COMPTEURS(j,:) = [NOL NSW NFS NFX Nco NcS NcF];
            
        end % sujets
    end % Regroupement electrodes
    
    CONDITION{con}.condition = O.DBase.Conditions{con};
    CONDITION{con}.PHASES = PHASES;
    
end

for con = 1:numel(O.DBase.Conditions)

figure('position',[100 100 720 800])
    subplot(2,2,1) ; hold on; set(gca,'color','none');
    f1 = [0.2 0.2 0.2]; t{2} = 'Central';
    f2 = [0.5 0.5 0.5]; t{1} = 'Frontal';
%     f3 = [0.8 0.8 0.8]; t{3} = 'Parietal';
    
     F = [f1;f2];%[f1;f2;f3];
     Z = {CONDITION{con}.PHASES{2}.all_slow_coin' , ...
          CONDITION{con}.PHASES{1}.all_slow_coin' }; %, ... 
%           CONDITION{con}.PHASES{3}.all_slow_coin'};
    violin(Z,'facecolor',F,'bw',0.1,'mc','-k','medc',[]);
    plot([0.5 3.5], [0 0],'--k'); text(3.7,0,'Dep','fontsize',14,'HorizontalAlignment','Center');
    plot([0.5 3.5], [-pi -pi],'--k'); text(3.7,-pi,'Hyp','fontsize',14,'HorizontalAlignment','Center');
    plot([0.5 3.5], [-pi/2 -pi/2],':k');
    plot([1 1],[-3*pi/2 pi/2],':k');
    plot([2 2],[-3*pi/2 pi/2],':k');
    plot([3 3],[-3*pi/2 pi/2],':k');
    ylim([-3*pi/2 pi/2])
    set(gca,'Xtick',[1 2 3]); set(gca,'XtickLabel',t);
    ylabel('Spindle onset on the Slow Wave phase');
    titre = sprintf('SLOW SWITCHERS (%s)',O.DBase.Conditions{con});
    title(titre,'fontsize',16);
    hold off
    
    subplot(2,2,2) ; hold on; set(gca,'color','none');
    f1 = [0.2 0.2 0.2]; t{2} = 'Central';
    f2 = [0.5 0.5 0.5]; t{1} = 'Frontal';
%     f3 = [0.8 0.8 0.8]; t{3} = 'Parietal';
    
     F = [f1;f2];%[f1;f2;f3];
     Z = {CONDITION{con}.PHASES{2}.all_fast_coin' , ...
          CONDITION{con}.PHASES{1}.all_fast_coin' };%, ... 
%           CONDITION{con}.PHASES{3}.all_fast_coin'};
    violin(Z,'facecolor',F,'bw',0.1,'mc','-k','medc',[]);
    plot([0.5 3.5], [0 0],'--k'); text(3.7,0,'Dep','fontsize',14,'HorizontalAlignment','Center');
    plot([0.5 3.5], [-pi -pi],'--k'); text(3.7,-pi,'Hyp','fontsize',14,'HorizontalAlignment','Center');
    plot([0.5 3.5], [-pi/2 -pi/2],':k');
    plot([1 1],[-3*pi/2 pi/2],':k');
    plot([2 2],[-3*pi/2 pi/2],':k');
    plot([3 3],[-3*pi/2 pi/2],':k');
    ylim([-3*pi/2 pi/2])
    set(gca,'Xtick',[1 2 3]); set(gca,'XtickLabel',t);
    ylabel('Spindle onset on the Slow Wave phase');
    title('FAST SWITCHERS','fontsize',16);
    hold off
    
    subplot(2,2,3) ; hold on; set(gca,'color','none');
    f1 = [0.2 0.2 0.2]; t{2} = 'Central';
    f2 = [0.5 0.5 0.5]; t{1} = 'Frontal';
%     f3 = [0.8 0.8 0.8]; t{3} = 'Parietal';
    
     F = [f1;f2];%[f1;f2;f3];
      Z = {CONDITION{con}.PHASES{2}.all_slow_deph' , ...
           CONDITION{con}.PHASES{1}.all_slow_deph' }%, ... 
%            CONDITION{con}.PHASES{3}.all_slow_deph'};
    violin(Z,'facecolor',F,'bw',0.1,'mc','-k','medc',[]);
    plot([0.5 3.5], [0 0],'--k'); text(3.6,0,'Onset','fontsize',14, ...
        'rotation',90,'HorizontalAlignment','Center');
    plot([0.5 3.5], [pi/2 pi/2],':k');
    plot([1 1],[-pi/4 pi],':k');
    plot([2 2],[-pi/4 pi],':k');
    plot([3 3],[-pi/4 pi],':k');
    ylim([-pi/4 pi])
    set(gca,'Xtick',[1 2 3]); set(gca,'XtickLabel',t);
    ylabel('Phase shift (max spindle power)');
    hold off
    
    subplot(2,2,4) ; hold on; set(gca,'color','none');
    f1 = [0.2 0.2 0.2]; t{2} = 'Central';
    f2 = [0.5 0.5 0.5]; t{1} = 'Frontal';
%     f3 = [0.8 0.8 0.8]; t{3} = 'Parietal';
    
     F = [f1;f2];%[f1;f2;f3];
      Z = {CONDITION{con}.PHASES{2}.all_fast_deph' , ...
           CONDITION{con}.PHASES{1}.all_fast_deph' };%, ... 
%            CONDITION{con}.PHASES{3}.all_fast_deph'};
    violin(Z,'facecolor',F,'bw',0.1,'mc','-k','medc',[]);
    plot([0.5 3.5], [0 0],'--k'); text(3.6,0,'Onset','fontsize',14, ...
        'rotation',90,'HorizontalAlignment','Center');
    plot([0.5 3.5], [pi/2 pi/2],':k');
    plot([1 1],[-pi/4 pi],':k');
    plot([2 2],[-pi/4 pi],':k');
    plot([3 3],[-pi/4 pi],':k');
    ylim([-pi/4 pi])
    set(gca,'Xtick',[1 2 3]); set(gca,'XtickLabel',t);
    ylabel('Phase shift (max spindle power)');
    hold off
    
end
    pause(1);
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Dephasage Centro-Frontal
    DEPHASAGE21_N23slow = [];
    DEPHASAGE21_N23fast = [];
%     DEPHASAGE31_N23slow = [];
%     DEPHASAGE31_N23fast = [];
    
    for con = 1:numel(O.DBase.Conditions)
        
        Dslow = CONDITION{con}.PHASES{2}.slow_mode - ...
                CONDITION{con}.PHASES{1}.slow_mode;
            Dslow = sign(Dslow).*mod(abs(Dslow),2*pi);
        DEPHASAGE21_N23slow = [DEPHASAGE21_N23slow Dslow];
%         Dslow = CONDITION{con}.PHASES{3}.slow_mode - ...
%                 CONDITION{con}.PHASES{1}.slow_mode;
%             Dslow = sign(Dslow).*mod(abs(Dslow),2*pi);
%         DEPHASAGE31_N23slow = [DEPHASAGE31_N23slow Dslow];
        
        
        Dfast = CONDITION{con}.PHASES{2}.fast_mode - ...
                CONDITION{con}.PHASES{1}.fast_mode;
            Dfast = sign(Dfast).*mod(abs(Dfast),2*pi);
        DEPHASAGE21_N23fast = [DEPHASAGE21_N23fast Dfast];
%         Dfast = CONDITION{con}.PHASES{3}.fast_mode - ...
%                 CONDITION{con}.PHASES{1}.fast_mode;
%             Dfast = sign(Dfast).*mod(abs(Dfast),2*pi);
%         DEPHASAGE31_N23fast = [DEPHASAGE31_N23fast Dfast];
        
    end
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%     figure; hold on; ax = gca; set(ax,'Color','none');
%     Xx = [1 2 4 5];
%     %     Yy = [mean(DEPHASAGE21_N23slow) ...
% %            mean(DEPHASAGE31_N23slow) ... 
% %            mean(DEPHASAGE21_N23fast) ...
% %            mean(DEPHASAGE31_N23fast)];
%     Yy = [mean(DEPHASAGE21_N23slow) ...
%           mean(DEPHASAGE21_N23fast)];
% 
% %     Ey = [std(DEPHASAGE21_N23slow) ...
% %           std(DEPHASAGE31_N23slow) ... 
% %           std(DEPHASAGE21_N23fast)] ...
% %           std(DEPHASAGE31_N23fast)];
%     Ey = [std(DEPHASAGE21_N23slow) ...
%           std(DEPHASAGE21_N23fast)];
%       errorbar(Xx,Yy,Ey,'.','markersize',28,'linewidth',2);
%       plot([0 6],[0 0],'--k');
%       xlim([0.5 5.5]);
%       ax.XTick = [1 2 4 5];
%       ax.XTickLabel = {'Fro->Cen' 'Fro->Cen'};%{'Fro->Cen' 'Fro->Tem' 'Fro->Cen' 'Fro->Tem'};
%       ax.FontSize = 14;
%       text(1.5,1,'SLOW','horizontalalignment','center','fontsize',18)
%       text(4.5,1,'FAST','horizontalalignment','center','fontsize',18)
%       ax.YLabel.String = 'Phase shift';
%       hold off
    % on sauvegarde (on prend N2 et N3 ensemble (on
    % distingue slow et fast)
    
    % AFAIRE SI NECESSAIRE
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sauvegarde:
    for con = 1:numel(O.DBase.Conditions)
        cond = O.DBase.Conditions{con};
    TT = [CONDITION{con}.PHASES{2}.slow_coin' , ...
          CONDITION{con}.PHASES{2}.fast_coin' , ...
          CONDITION{con}.PHASES{2}.slow_deph' , ...
          CONDITION{con}.PHASES{2}.fast_deph' , ...
          CONDITION{con}.PHASES{1}.slow_coin' , ...
          CONDITION{con}.PHASES{1}.fast_coin' , ...
          CONDITION{con}.PHASES{1}.slow_deph' , ...
          CONDITION{con}.PHASES{1}.fast_deph']% , ...
%           CONDITION{con}.PHASES{3}.slow_coin' , ...
%           CONDITION{con}.PHASES{3}.fast_coin' , ...
%           CONDITION{con}.PHASES{3}.slow_deph' , ...
%           CONDITION{con}.PHASES{3}.fast_deph'];
    der = {'Fr_' 'Ce_'};%{'Fr_' 'Ce_' 'Pa_'};
        v = {'o_slow' 'o_fast' 'd_slow' 'd_fast'};
        vname = {}; k = 1;
        for i = 1:2%3
            for j = 1:4
                vname{k} = [der{i} v{j}] ; k = k+1;
            end
        end
    Tphases = array2table(TT);
    Tphases.Properties.VariableNames = vname;
    Tphases.Properties.RowNames = LISTE_SUJ';
    
    fprintf('\n ===== CONDITION: %s\n',cond);
    disp(Tphases);
    
    fprintf(' Conventions:\n');
    fprintf('\t Fr: frontal, Ce: Central, Pa: Parietal\n');
    fprintf('\t o_slow: phase moyenne d''arrimage du fuseaux sur les slow switchers\n');
    fprintf('\t o_fast: phase moyenne d''arrimage du fuseaux sur les fast switchers\n');
    fprintf('\t d_slow: dephasage moyen entre arrimage et max de \n\t\t puissance du fuseaux sur les slow switchers\n');
    fprintf('\t d_fast: dephasage moyen entre arrimage et max de \n\t\t puissance du fuseaux sur les fast switchers\n');
    file_Name = [dd filesep 'Results' filesep 'SSW_SPDL_Phases' cond '.xls'];
    writetable(Tphases,file_Name,'WriteRowNames',1);
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    % Sauvegarde:
    for con = 1:numel(O.DBase.Conditions)
        cond = O.DBase.Conditions{con};
    TT = [CONDITION{con}.PHASES{2}.COMPTEURS , ...
          CONDITION{con}.PHASES{1}.COMPTEURS]% , ...
%           CONDITION{con}.PHASES{3}.COMPTEURS];
      
    der = {'Fr_' 'Ce_'};%{'Fr_' 'Ce_' 'Pa_'};
        v = {'NbOL' 'NbSlow' 'NbFast' 'NbSpdl' 'NbCoin' 'NbCoinS' 'NbCoinF'};
        vname = {}; k = 1;
        for i = 1:2%3
            for j = 1:7
                vname{k} = [der{i} v{j}] ; k = k+1;
            end
        end
    TStat = array2table(TT);
    TStat.Properties.VariableNames = vname;
    TStat.Properties.RowNames = LISTE_SUJ';
    
    fprintf('\n ===== CONDITION: %s\n',cond);
    disp(TStat);
    
    fprintf(' Conventions:\n');
    fprintf('\t Fr: frontal, Ce: Central, Pa: Parietal\n');
    fprintf('\t NbOL: nombre d''ondes lentes\n');
    fprintf('\t NbSlow: nombre de slow switchers\n');
    fprintf('\t NbFast: nombre de fast switchers\n');
    fprintf('\t NbSpdl: nombre de fuseaux\n');
    fprintf('\t NbCoin: nombre de fuseaux sur des OL\n');
    fprintf('\t NbCoinS: nombre de fuseaux sur des slow switchers\n');
    fprintf('\t NbCoinF: nombre de fuseaux sur les fast switchers\n');
    file_Name = [dd filesep 'Results' filesep 'SSW_SPDL_STAT_' cond '.xls'];
    writetable(TStat,file_Name,'WriteRowNames',1);
    
    end
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    fprintf('\n\n  Voir les fichiers xls dans Results de la cohorte\n');
    fprintf('XXX_mat. Bye.\n');
    
% end



