function Analyses_PAC_Slow_Fast_sigma01(dd)
% clear
% clc
addpath('./ressources');
fprintf('\n\n\n ====== PAC Slow Wave Analyses 001 -v.01- ====== %s \n\t\t(Statistiques des OL)\n',date);
fprintf('\n\n On cherche une cohorte Resultats_XXX (dans laquelle on trouve DBaseMatlab) \n');
fprintf(' Attention: on doit y trouver aussi le repertoire Dgroup_mat (copie de ce qui se \n');
fprintf(' trouve dans le repertoire XXX_2mat de la cohorte (au niveau des Epoques) \n');
% dd = uigetdir('.');

% ddSSW = [dd filesep 'DBaseMatlab'];
ddSSW = [dd filesep 'DBase_SSW'];
ddTRD = [dd filesep 'Dgroup_mat'];
if ~isfolder(ddSSW), fprintf('-- !!! La cohorte ne contient pas le repertoire d''OL (DBaseMatlab)...\n Bye!\n'); return; end
if ~isfolder(ddTRD), fprintf('-- !!! La cohorte ne contient pas le repertoire d''OL (Dgroup_mat)...\n Bye!\n'); return; end
ddRes = [dd filesep 'Results'];
if ~isfolder(ddRes), fprintf(' -- On doit executer l''Analyses_inventaire_SSW avant.\n   Bye.\n');return; end

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% On demande ici qu'elle est la frequence de separation (ou on la          %
% re-calcule) Recherche de l'inventaire des SSW                            %
uu = dir([ddRes filesep '001_inventaire_SSW_04.mat']);                     %
if numel(uu)~=1                                                            %
    fprintf(' -- Pasd''INVENTAIRE (001).\n   Bye.\n');                     %
    return;                                                                %
else                                                                       %
    load([ddRes filesep uu(1).name],'INVENTAIRE','LISTE','LISTE_file','O');%
end                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
for stg = 1:numel(O.DBase.Conditions)                                     %
        fmin_max = [0.25 4.5];                                            %
        INV_ftr = [];                                                     %
        for ele = 1:numel(O.DBase.channels)                               %
            INV_ftr = cat(1,INV_ftr,INVENTAIRE{ele}{stg}(:,7));           %
        end                                                               %
        [mix0, f0] = mixture_slow_fast(INV_ftr,fmin_max);                 %
        f_sep(stg) = f0;                                                  %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Inventaire des DONNEES:
vv = dir([ddTRD filesep 'Dgr*.mat']); k = 1;
fprintf(' -- On a trouve %d sujets (au niveau des donnees)\n',numel(vv));
for i = 1:numel(vv)
    A = strsplit(vv(i).name,'__');
    B = A{2};
    ddi(k).name = B;
    ddi(k).Dgr = vv(i).name;
    ww = dir([ddSSW filesep 'SW_' B]);
    if numel(ww)==1
        ddi(k).SSW = ww(1).name;
        fprintf('    -> %s trouve dans SSW\n',B);
        k = k+1;
    else
        fprintf('    !! %s pas trouve dans SSW\n',B);
    end
end
fprintf(' -- On a trouve %d sujets (au niveau des SSW)\n\n',numel(ddi));
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
T1fast = table; T2fast = table;
T1slow = table; T2slow = table;
ma_liste = {};


for i = 1:numel(ddi)
    fprintf(' --- Lecture de %s\n',ddi(i).name);
	load([ddTRD filesep ddi(i).Dgr]);
    load([ddSSW filesep ddi(i).SSW]);
    
    ma_liste{i} = SSW.name_Id;
    
    % TR.D{cond}{elect}
    % SSW.markers{ele}{cond}
    for std = 1:2
        ZPAC_SLOW_VALUES = []; 
        ZPAC_FAST_VALUES = []; 
        k = 1;
        for ele = 1:numel(SSW.markers)
            TRD = TR.D{std}{ele};
            MKR = SSW.markers{ele}{std};
            stade = MKR.stage;
            elec{i} = TRD.electrode;
            trode = MKR.electro;
            fprintf('stade %s, %s %s\n',stade,elec{i},trode);
            % couplage (Z-PAC)
            [PAC] = calcul_PAC_000(TRD,MKR);
            Class_slow = find(PAC.ftr<=f_sep(std)); 
            Class_fast = find(PAC.ftr>f_sep(std));
            PAC_slow   = mean(PAC.zPAC(Class_slow));
            PAC_fast   = mean(PAC.zPAC(Class_fast));
            ZPAC_SLOW_VALUES(k) = PAC_slow;
            ZPAC_FAST_VALUES(k) = PAC_fast;
            k=k+1;
        end
        if std==1
        T1slow = [T1slow ; array2table(ZPAC_SLOW_VALUES)];
        T1fast = [T1fast ; array2table(ZPAC_FAST_VALUES)];
        elseif std==2
        T2slow = [T2slow ; array2table(ZPAC_SLOW_VALUES)];
        T2fast = [T2fast ; array2table(ZPAC_FAST_VALUES)];
        end
    end  
end

elec = {'SUJETS_N2'};
for i = 1:numel(O.DBase.channel)
    elec{i+1} = O.DBase.channel{i};
end
TT1slow = [cell2table(ma_liste') T1slow];
TT1fast = [cell2table(ma_liste') T1fast];
TT1fast.Properties.VariableNames = elec;
TT1slow.Properties.VariableNames = elec;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ecriture du fichier EXCEL                                                             %
    fileXLS = ['TABLE_N2_SLOW_zPAC.xls'];                                               %
    fprintf('\n--> Ecriture de %s\n',fileXLS);disp(TT1slow);                            %
    fileXLS = [ddRes filesep fileXLS];                                                  %
    writetable(TT1slow,fileXLS);                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ecriture du fichier EXCEL                                                             %
    fileXLS = ['TABLE_N2_FAST_zPAC.xls'];                                               %
    fprintf('\n--> Ecriture de %s\n',fileXLS);disp(TT1fast);                            %
    fileXLS = [ddRes filesep fileXLS];                                                  %
    writetable(TT1fast,fileXLS);                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

elec = {'SUJETS_N3'};
for i = 1:numel(O.DBase.channel)
    elec{i+1} = O.DBase.channel{i};
end
TT2slow = [cell2table(ma_liste') T2slow];
TT2fast = [cell2table(ma_liste') T2fast];
TT2fast.Properties.VariableNames = elec;
TT2slow.Properties.VariableNames = elec;

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ecriture du fichier EXCEL                                                             %
    fileXLS = ['TABLE_N3_SLOW_zPAC.xls'];                                               %
    fprintf('\n--> Ecriture de %s\n',fileXLS);disp(TT2slow);                            %
    fileXLS = [ddRes filesep fileXLS];                                                  %
    writetable(TT2slow,fileXLS);                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Ecriture du fichier EXCEL                                                             %
    fileXLS = ['TABLE_N3_FAST_zPAC.xls'];                                               %
    fprintf('\n--> Ecriture de %s\n',fileXLS),disp(TT2fast);                            %
    fileXLS = [ddRes filesep fileXLS];                                                  %
    writetable(TT2fast,fileXLS);                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

fprintf('\n\n -- Fin. Bye.\n\n')
end