function SSW_SPIN = find_coincidence_SSW_SPIN(SSW,SPIN,fsep)

    fprintf(' --> Coincidences SSW-SPINDLES:\n')
    U = primes(600); Uol = U(U<300); Ufs = U(U>300);
    
    for ele = 1:numel(SSW.markers)
        for con = 1:numel(SSW.markers{ele})
            
            TABLE_OL = []; k = 1;
            TABLE_FX = []; m = 1;
            
            SSW_SPIN{ele}{con}.coincidence = {};
            SSW_SPIN{ele}{con}.touch       = {};
            SSW_SPIN{ele}{con}.true_coincidence = [];
            
            Nt   = length(SSW.markers{ele}{con}.signals.Time);
            Nepo = numel(SSW.markers{ele}{con}.markers);
            
            if Nepo>0
            
            sequence_OL = zeros(Nepo,Nt);
            sequence_FX = zeros(Nepo,Nt);
            for epo = 1:Nepo

                ssw_markers  = SSW.markers{ele}{con}.markers{epo};
                spin_markers = SPIN.markers{ele}{con}.markers{epo};

                if ~isempty(ssw_markers.PaP)
                    for i = 1:numel(ssw_markers.PaP)
                        TABLE_OL(k,1) = ele;
                        TABLE_OL(k,2) = con;
                        TABLE_OL(k,3) = epo;
                        TABLE_OL(k,4) = i;
                        TABLE_OL(k,5) = Uol(i);
                        TABLE_OL(k,6) = ssw_markers.n_t(i,1);
                        TABLE_OL(k,7) = ssw_markers.n_t(i,2);
                        TABLE_OL(k,8) = ssw_markers.tfr(i)<fsep{con};  
                        sequence_OL(epo,TABLE_OL(k,6):TABLE_OL(k,7)) = Uol(i);
                        k = k+1;
                    end
                end

                if ~isempty(spin_markers.fr)
                    for i = 1:numel(spin_markers.fr)
                        TABLE_FX(m,1) = ele;
                        TABLE_FX(m,2) = con;
                        TABLE_FX(m,3) = epo;
                        TABLE_FX(m,4) = i;
                        TABLE_FX(m,5) = Ufs(i);
                        TABLE_FX(m,6) = spin_markers.n_t(i,1);
                        TABLE_FX(m,7) = spin_markers.n_t(i,2);
                        TABLE_FX(m,8) = spin_markers.maxPower(i,1);
                        sequence_FX(epo,TABLE_FX(m,6):TABLE_FX(m,7)) = Ufs(i);
                        m = m+1;
                    end
                end 
            end

            coincidence = sequence_FX .* sequence_OL;
            R = find(sum(coincidence,2));
            
            k = 1;
            for i = 1:numel(R)
                L = unique(coincidence(R(i),:));
                L = L(L>0);
                for j = 1:numel(L)
                    FF = factor(L(j));
                    SSW_SPIN{ele}{con}.coincidence{k} = round(...
                        [TABLE_OL(TABLE_OL(:,5)==FF(1) & TABLE_OL(:,3)==R(i),:) ; ...
                         TABLE_FX(TABLE_FX(:,5)==FF(2) & TABLE_FX(:,3)==R(i),:)]);
                     
                    ssw_f_signal = SSW.markers{ele}{con}.signals.data_f_sw(R(i),:);
                    
                    [SSW_SPIN{ele}{con}.touch{k},SSW_SPIN{ele}{con}.true_coincidence(k)] = ...
                        find_the_phase(ssw_f_signal,...
                                       SSW_SPIN{ele}{con}.coincidence{k});
                    k = k+1;
                end
            end
            end
            
            K = numel(find(SSW_SPIN{ele}{con}.true_coincidence~=0));
            L = numel(find(SSW_SPIN{ele}{con}.true_coincidence>0));
            fprintf('     %s: %d coincidences, %d%% are Slow Switchers (%s)\n', ...
                SSW.markers{ele}{con}.electro, ...
                K, ...
                round(L/K*100), ...
                SSW.markers{ele}{con}.stage)
        end
    end

end