function analyse_SSW_SPIN_onset_coupling()

% clc
% clear
addpath(genpath('./ressources'))
fprintf(' ===== Analyse 003: SPINDLE-SSW Coupling ====\n');
fprintf('       Phase of the spindle over the SSW\n\n');
fprintf(' We consider the coincidence btw ssw and spindles\n')
fprintf(' The coincidence will be defined with the phase\n')
fprintf(' of the SSW, givent that phase=0 corresponds to\n')
fprintf(' the MAXIMUM DEPOLARIZATION of the slow wave. The\n')
fprintf(' phase is in unit of pi.\n\n')

fprintf(' --  Looking for a cohorte XXX_mat\n')
 dd = uigetdir('./');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
uu = dir([dd filesep 'Results' filesep '001_inventaire_SWITCHERS*']);     %
if ~isempty(uu)                                                           %
    my_folder = uu(1).folder;                                             %
    my_file = uu(1).name;                                                 %
    if numel(uu)>1                                                        %
        fprintf('     Ambiguity of the INVENTORY, please select\n');      %
        my_file = uigetfile([dd filesep 'Results']);                      %
    end                                                                   %
    fprintf(' --> we read %s (for the frequency thresholds: ',my_file);   %
    load([my_folder filesep my_file],'f_sep0');                           %
    fprintf('%4.2f , %4.3f\n',f_sep0{1},f_sep0{2});                       %
else                                                                      %
    fprintf(' 001_inventaire_SWITCHERS not available. Bye\n');            %
    return;                                                               %
end                                                                       %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
vv = dir([dd filesep 'DBase_SSW' filesep 'SW*']);
if isempty(vv)
    fprintf(' DBsae_SSW empty. Bye\n'); 
    return; 
else
    Nsuj = numel(vv);
    fprintf(' --> we found %d subjects in this cohort\n',Nsuj);
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dd_ssw_spin = [dd filesep 'DBase_SSW_SPIN'];           %
if isfolder(dd_ssw_spin), rmdir(dd_ssw_spin,'s'); end  %
mkdir(dd_ssw_spin);                                    %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

SSW_SPINs = cell(1,Nsuj);
for isuj = 1:Nsuj

    ssw_file = vv(isuj).name;
    A = strsplit(ssw_file,'_');
        fprintf('\n --> we consider the SLOW WAVES of %s\n',A{2}(1:end-3));
        ww = dir([dd filesep 'DBase_SPIN' filesep 'SPIN_' A{2}]);
        if isempty(ww)
            fprintf(' DBsae_SPIN not found.\n'); 
            break; 
        end  
    fprintf(' --> we consider the SPINDLES of %s\n',A{2}(1:end-3));
    spin_file = ww.name;

    fprintf('     we read the SLOW WAVES ...');
    load([vv(1).folder filesep ssw_file],'SSW','O')
    fprintf(' done\n');
    fprintf('     we read the SPINDLES ...');
    load([ww.folder filesep spin_file],'SPIN')
    fprintf(' done\n');

    SSW_SPIN = find_coincidence_SSW_SPIN(SSW,SPIN,f_sep0);
    SSW_SPINs{isuj} = SSW_SPIN;
    save([dd_ssw_spin filesep 'SSW_SPIN_' A{2}],'SSW_SPIN','O');

end

% SSW_SPIN{suj}{ele}{con}
%          coincidence: {1�35 cell}
%                touch: {1�35 cell}
%     true_coincidence: [2 1 0 1 0 0 .... 2 0]
% 
% SSW_SPIN{1}{ele}{con}.coincidence{coincidence}: table
%   ele con epoq i_ol prime debut fin 1=slow
%   ele con epoq i_fs prime debut fin indice_power




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Regroupement d'electrodes:                                                          %
k = 1; l = 1; q = 1; v = 1;                                                           %
regroupement(1).name = 'Central';                                                     %
regroupement(2).name = 'Frontal';                                                     %
regroupement(3).name = 'Parietal';                                                    %
regroupement(4).name = 'Temporal';                                                    %
for i = 1:numel(O.DBase.channel)                                                      %
    if     strcmpi(O.DBase.channel{i}(1),'C'), regroupement(1).ele(k) = i; k = k+1;   %
    elseif strcmpi(O.DBase.channel{i}(1),'F'), regroupement(2).ele(l) = i; l = l+1;   %
    elseif strcmpi(O.DBase.channel{i}(1),'P'), regroupement(3).ele(q) = i; q = q+1;   %
    elseif strcmpi(O.DBase.channel{i}(1),'T'), regroupement(4).ele(v) = i; v = v+1;   %
    end                                                                               %
end                                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

PHASES = cell(1,numel(regroupement));
for i = 1:numel(regroupement)
    u = 1; v = 1;
    fprintf(' -->  GROUPE ELECTRODES: %s\n',regroupement(i).name);
    for ele = 1:numel(regroupement(i).ele)
        i_ele = regroupement(i).ele(ele);
        for j = 1:Nsuj
            for con = 1:numel(SSW_SPINs{j}{i_ele})
                slow_sw = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence>0);
                fast_sw = find(SSW_SPINs{j}{i_ele}{con}.true_coincidence<0);
                for a = 1:numel(slow_sw)
                    PHASES{i}.slow_coin(u) = SSW_SPINs{j}{i_ele}{con}.touch{slow_sw(a)}.start;
                    u = u+1;
                end
                for a = 1:numel(fast_sw)
                    PHASES{i}.fast_coin(v) = SSW_SPINs{j}{i_ele}{con}.touch{fast_sw(a)}.start;
                    v = v+1;
                end
            end
        end
    end
end

figure('position',[100 100 900 400])
    ppx = -3/2:0.005:0.5; %ppx = ppx(2:end-1);
    C = {'b' 'r' 'm' 'g'};
    subplot(1,2,1); hold on; 
    ax = gca; set(ax,'color','none');
    for i = 1:numel(PHASES)
        if ~isempty(PHASES{i})
            ppp = PHASES{i}.slow_coin;
            ppp(ppp<-1.5)=[];
            ppp(ppp>0.5) = [];
            [w,pp] = hist(ppp,45);
            pp = [-1.5 pp 0.5];
            w  = [0 w 0];
            w = w/sum(w)/(pp(4)-pp(3));
            wwx = spline(pp,w,ppx);
            plot(ppx,wwx,'linewidth',2,'color',C{i});
            text(0.1,1-i*0.08,regroupement(i).name,'fontsize',12,'color',C{i});
        end
    end
    plot([0 0],      [0 0.7],'--k'); text(0,0.75,'Dep','fontsize',14,'HorizontalAlignment','Center');
    plot([-0.5 -0.5],[0 1],'--k')
    plot([-1 -1],    [0 0.7],'--k'); text(-1,0.75,'Hyp','fontsize',14,'HorizontalAlignment','Center');
    xlabel('PHASES (onset of the spindle)','fontsize',14);
    title('SLOW SWITCHERS - SPINDLES','fontsize',14)
    
    
    
    ppx = -3/2:0.01:0.5; ppx = ppx(2:end-1);
    subplot(1,2,2);  hold on; 
    ax = gca; set(ax,'color','none');
    for i = 1:numel(PHASES)
        if ~isempty(PHASES{i})
            ppp = PHASES{i}.fast_coin;
            ppp(ppp<-1.5)= [];
            ppp(ppp>0.5) = [];
            [w,pp] = hist(ppp,45);
            pp = [-1.5 pp 0.5];
            w  = [0 w 0];
            w = w/sum(w)/(pp(4)-pp(3));
            wwx = spline(pp,w,ppx);
            plot(ppx,wwx,'linewidth',2,'color',C{i});
            text(0.1,1.5-i*0.08,regroupement(i).name,'fontsize',12,'color',C{i});
        end
    end
    plot([0 0],      [0 1],'--k'); text(0,1.1,'Dep','fontsize',14,'HorizontalAlignment','Center');
    plot([-0.5 -0.5],[0 1.5],'--k')
    plot([-1 -1],    [0 1],'--k'); text(-1,1.1,'Hyp','fontsize',14,'HorizontalAlignment','Center');
    xlabel('PHASES (onset of the spindle)','fontsize',14);
    title('FAST SWITCHERS - SPINDLES','fontsize',14)
    
    
%end



