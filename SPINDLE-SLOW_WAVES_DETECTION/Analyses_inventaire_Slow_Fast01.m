function Analyses_inventaire_Slow_Fast01(dd)
% clear
% clc
addpath('./ressources');
fprintf('\n\n\n ====== Slow Wave Analyses 001 -v.01- ====== %s \n\t\t(Statistiques des OL)\n',date);
fprintf('\n\n On cherche une cohorte XXX_2mat (dans laquelle on trouve DBase_SSW) : \n');
% dd = uigetdir('.');

ddSSW = [dd filesep 'DBase_SSW'];
if ~isfolder(ddSSW), fprintf('-- !!! La cohorte ne contient pas le repertoire d''OL (DBase_SSW)...\n Bye!\n'); return; end

ddRes = [dd filesep 'Results'];
if ~isfolder(ddRes), fprintf(' -- On doit executer l''Analyses_inventaire_SSW avant.\n   Bye.\n');return; end

% Recherche de l'inventaire des SSW %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
uu = dir([ddRes filesep '001_inventaire_SSW*.mat']);                       %
if numel(uu)~=1                                                            %
    fprintf(' -- Ambiguite ou absence d''INVENTAIRE (001).\n   Bye.\n');   %
    return;                                                                %
else                                                                       %
    load([ddRes filesep uu(1).name],'INVENTAIRE','LISTE','LISTE_file','O');%
end                                                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Structure INVENTAIRE{electrode}{stade}
%   [i nbepocs densSSW PaP Neg mfr tfr tNe tPo ele stade epoque]
% 1) Pour chaque sujet: Calcul de la frequence de separation par 
%    stade (toutes electrodes confondues)
%

for stg = 1:numel(O.DBase.Conditions)
        fmin_max = [0.25 4.5];
        INV = [];
        for ele = 1:numel(O.DBase.channels)
            INV = cat(1,INV,INVENTAIRE{ele}{stg}(:,[1 7]));
        end
        index_suj = unique(INV(:,1)); 
        for i = 1:numel(index_suj)
            INV_i = INV(INV(:,1)==index_suj(i),:);
            [mix(i), f_sep(i)] = mixture_slow_fast(INV_i(:,2),fmin_max);
        end
        [mix0, f_sep0{stg}] = mixture_slow_fast(INV(:,2),fmin_max);

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Figure:                                                      %
        figure('position',[10 100 800,400]); ax = gca;                 %
        set(ax,'Color','none');                                        %
        hold on                                                        %
        for i = 1:numel(index_suj)                                     %
            plot(mix(i).G1.fsamp,mix(i).G1.g1,'Color',[0.3 0.3 0.3]);  %
            plot(mix(i).G2.fsamp,mix(i).G2.g1,'Color',[0.7 0.7 0.7]);  %
        end                                                            %
        plot(mix0.G1.fsamp,mix0.G1.g1,'-r','linewidth',3);             %
        plot(mix0.G2.fsamp,mix0.G2.g1,'-b','linewidth',3);             %
        plot([f_sep0{stg} f_sep0{stg}],[0 0.7],'--k','linewidth',3);   %
        mess = sprintf('%3.2f Hz',f_sep0{stg});                        %
        text(f_sep0{stg},0.95,mess,...                                 %
                          'HorizontalAlignment','center',...           %
                          'fontsize',16);                              %
        mess = sprintf('%3.2f\n[%3.2f]', mean(f_sep),std(f_sep));      %
        text(f_sep0{stg},0.85,mess,...                                 %
                          'HorizontalAlignment','center',...           %
                          'fontsize',14);                              %
        % les donnees: %%%%%%%%%%%%%%%%%%%%                            %
        [a,b] = hist(INV(:,2),60);        %                            %
        a  = a /sum(a) /(b(2)-b(1));      %                            %
        ds = b(1):0.01:b(end);            %                            %
        d  = spline(b,a,ds);              %                            %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                            %
        plot(ds,d,'-','Color',[0.5 0.5 0.5],'linewidth',2);            %
        xlabel('transition frequency (Hz)','fontsize',14);             %
        title(O.DBase.Conditions{stg},'fontsize',14);                  %
        hold off                                                       %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                             SWITCHERS                                    % 
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf(' -- On a les frequence de separation suivante:\n');
disp(f_sep0);
% Inventaire des slow et fast switchers: On procede par stade, par electrode
% et par sujet. On construit in INVENTAIRE similaire aux OL 
% par stade
% Duree d'une page: On suppose que c'est la meme pour tout le monde
a = INVENTAIRE{1}{1}(INVENTAIRE{1}{1}(:,1)==1,2:3);
duree_page = size(a,1)/a(1,1)/a(1,2); %%% EN MINUTES %%%

    % Construction des SLOW:
    for stg = 1:numel(O.DBase.Conditions)
        for ele = 1:numel(O.DBase.channels)
            INVENTAIRE_SLOW{ele}{stg} = ...
                INVENTAIRE{ele}{stg}(INVENTAIRE{ele}{stg}(:,7)< f_sep0{stg},:);
        end
    end
    % Construction des SLOW:
    for stg = 1:numel(O.DBase.Conditions)
        for ele = 1:numel(O.DBase.channels)
            INVENTAIRE_FAST{ele}{stg} = ...
                INVENTAIRE{ele}{stg}(INVENTAIRE{ele}{stg}(:,7)>= f_sep0{stg},:);
        end
    end
    
INVENTAIRE = INVENTAIRE_SLOW; 
TYPE       = 'SLOW';

for Switcher = 1:2    
            %%% TABLES DE SORTIE (SWITCHERS)
            % On construit une table pour la cohorte

            varNames1 = {'NbrEpoc' 'Dens'}; NVar1 = numel(varNames1);
            varNames2 = {'P2P' 'Neg' 'fre' 'tra' 'Hyp' 'Dep'}; NVar2 = numel(varNames2);

            varTypes = strings(1,14);
            varTypes(:,:) = 'double';
                          % Colonne de gauche
                          for stg = 1:numel(O.DBase.Conditions)
                          TABLE(stg).full_Table = table('Size',[numel(LISTE) 1], ...
                              'VariableTypes',"string", ...
                              'VariableNames',{'SUJETS'}, ...
                              'RowNames',LISTE);
                          TABLE(stg).full_Table.SUJETS = LISTE';
                          end

                for ele = 1:numel(O.DBase.channels)
                    for stg = 1:numel(O.DBase.Conditions)
                    El = O.DBase.channels{ele};
                    St = O.DBase.Conditions{stg};
                    for k = 1:numel(varNames1), varNames_k{k} = [El '_' St '_' varNames1{k}]; end
                    for k = 1:numel(varNames2), varNames_k{k+NVar1} = [El '_' St '_' varNames2{k} '_M']; end
                    for k = 1:numel(varNames2), varNames_k{k+NVar1+NVar2} = [El '_' St '_' varNames2{k} '_S']; end

                    for k = 1:numel(LISTE_file)
                        INV_k = INVENTAIRE{ele}{stg}(INVENTAIRE{ele}{stg}(:,1)==k,1:9);
                        petite_table = INV_k;
                        Nbr_switchers = size(INV_k,1);
                        % test si petite table vide %%%%%%%%%%%%%%%%%%%%%%%
                        if Nbr_switchers <2                               %
                            ligne_sujetsM = NaN(1,9);                     %
                            ligne_sujetsS = NaN(1,9);                     %
                        else                                              %
                            ligne_sujetsM = mean(petite_table);           %
                            ligne_sujetsS = sqrt(var(petite_table,[]));   %
                            ligne_sujetsM(3) = Nbr_switchers/ ...         %
                                               ligne_sujetsM(2) / ...     %
                                               duree_page;                %
                        end                                               %
                        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                        numtable(k,:) = [ligne_sujetsM(2:end) ligne_sujetsS(4:end)];
                    end
                    T = table('Size',[numel(LISTE) 14], ...
                              'VariableTypes',varTypes, ...
                              'VariableNames',varNames_k, ...
                              'RowNames',LISTE);
                    for k = 1:numel(varNames_k)
                    T.(varNames_k{k}) = numtable(:,k);
                    end
                    disp(T);
                    TABLE(stg).full_Table = [TABLE(stg).full_Table T];
                    end
                end

            %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%    
            % Ecriture du fichier EXCEL
            for stg = 1:numel(O.DBase.Conditions)
                fileXLS = [ddRes filesep 'TABLE_' TYPE '_' O.DBase.Conditions{stg} '.xls'];
                Tstag = TABLE(stg).full_Table;
                writetable(Tstag,fileXLS);
            end
INVENTAIRE = INVENTAIRE_FAST;
TYPE       = 'FAST';
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
fprintf('***********************************************************************\n')
fprintf('VARIABLES ARCHIVES DANS LES XLS:\n');
fprintf('\tNbr d''epoques\n');
fprintf('\tDensite (/minute)\n');
fprintf('\tAmplitude PaP (moyenne)\n');
fprintf('\tfrequence (moyenne)\n');
fprintf('\tfrequence transition (moyenne)\n');
fprintf('\tAmplitude DOWN (moyenne)\n');
fprintf('\tAmplitude UP (moyenne)\n');
fprintf('\tsuivi de :Amplitude PaP, frequences, DOMN et UP (eccart-type)\n');
fprintf('***********************************************************************\n\n')

save([ddRes filesep '001_inventaire_SWITCHERS_04'], ...
    'INVENTAIRE_SLOW',...
    'INVENTAIRE_FAST',...
    'LISTE',...
    'LISTE_file',...
    'O',...
    'f_sep0');


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
% ON MELANGE ICI LES STADES 2 et 3:  CAS PARTICULIER (on melange les deux
% dernieres conditions
% TABLES DE SORTIE
% On construit une table pour la cohorte a condition de trouver les deux 
% conditions '2' et '3'
k2 = find(contains(O.DBase.Conditions,'2'));
k3 = find(contains(O.DBase.Conditions,'3'));
if numel(k2)~=1 || numel(k3)~=1
    fprintf('\n\n -- Pas de stade 2 et 3 a fusionner\n    Fin. Bye.\n\n');
    return; 
else
    fprintf('\n -- Fusion des stades 2 et 3\n');
end
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

INVENTAIRE = INVENTAIRE_SLOW; 
TYPE       = 'SLOW';
% Fusion des INVENTAIRE{}{k2} et INVENTAIRE{}{k3} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
[INVENTAIRE23_SLOW] = fusion23(INVENTAIRE,k2,k3);                                  %
save([ddRes filesep '001_inventaire_SWITCHERS_04'],'INVENTAIRE23_SLOW','-append'); %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

for Switcher = 1:2    

        varNames1 = {'NbrEpoc' 'Dens'}; NVar1 = numel(varNames1);
        varNames2 = {'P2P' 'Neg' 'fre' 'tra' 'Hyp' 'Dep'}; NVar2 = numel(varNames2);

        varTypes = strings(1,14);
        varTypes(:,:) = 'double';
                      % Colonne de gauche
                      TABLE23.full_Table = table('Size',[numel(LISTE) 1], ...
                          'VariableTypes',"string", ...
                          'VariableNames',{'SUJETS'}, ...
                          'RowNames',LISTE);
                      TABLE23.full_Table.SUJETS = LISTE';             

            for ele = 1:numel(O.DBase.channels)
                El = O.DBase.channels{ele};
                for k = 1:numel(varNames1), varNames_k{k} = [El '_23_' varNames1{k}]; end
                for k = 1:numel(varNames2), varNames_k{k+NVar1} = [El '_23_' varNames2{k} '_M']; end
                for k = 1:numel(varNames2), varNames_k{k+NVar1+NVar2} = [El '_23_' varNames2{k} '_S']; end

                for k = 1:numel(LISTE_file)
                    INV2_k = INVENTAIRE{ele}{k2}(INVENTAIRE{ele}{k2}(:,1)==k,1:9);
                    INV3_k = INVENTAIRE{ele}{k3}(INVENTAIRE{ele}{k3}(:,1)==k,1:9);
                    petite_table = [INV2_k; INV3_k];
                    Nbr_switchers = size(petite_table,1);
                    % test si petite table vide %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                    if size(petite_table,1)<2                               %
                        ligne_sujetsM = NaN(1,9);                           %
                        ligne_sujetsS = NaN(1,9);                           %
                    else                                                    %
                        ligne_sujetsM = mean(petite_table);                 %
                        ligne_sujetsS = sqrt(var(petite_table,[]));         %
                        % Ajustement du nombre d'epoques et densite:        %
                        Nb2 = 0; if ~isempty(INV2_k),Nb2 = INV2_k(1,2); end % 
                        Nb3 = 0; if ~isempty(INV3_k),Nb3 = INV3_k(1,2); end %
                        De2 = 0; if ~isempty(INV2_k),De2 = INV2_k(1,3); end %
                        De3 = 0; if ~isempty(INV3_k),De2 = INV3_k(1,3); end %
                        ligne_sujetsM(2) = Nb2+Nb3;                         %
                        ligne_sujetsM(3) = Nbr_switchers/ ...               %
                                           ligne_sujetsM(2) / ...           %
                                           duree_page;                      %
                    end                                                     %
                    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                    
                    numtable(k,:) = [ligne_sujetsM(2:end) ligne_sujetsS(4:end)];
                end
                T = table('Size',[numel(LISTE) 14], ...
                          'VariableTypes',varTypes, ...
                          'VariableNames',varNames_k, ...
                          'RowNames',LISTE);
                for k = 1:numel(varNames_k)
                T.(varNames_k{k}) = numtable(:,k);
                end
                disp(T);
                TABLE23.full_Table = [TABLE23.full_Table T];
            end

        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Ecriture du fichier EXCEL                                                             %
            fileXLS = ['TABLE_' TYPE '_Stade23.xls'];                                           %
            fprintf('\n--> Ecriture de %s\n',fileXLS)                                           %
            fprintf('    Ce fichier contient les stat des %s (Stade 2+3)\n\n',TYPE);            %
            fileXLS = [ddRes filesep fileXLS];                                                  %
            Tstag = TABLE23.full_Table;                                                         %
            writetable(Tstag,fileXLS);                                                          %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        INVENTAIRE = INVENTAIRE_FAST;
        TYPE       = 'FAST';
        % Fusion des INVENTAIRE{}{k2} et INVENTAIRE{}{k3} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        [INVENTAIRE23_FAST] = fusion23(INVENTAIRE,k2,k3);                                  %
        save([ddRes filesep '001_inventaire_SWITCHERS_04'],'INVENTAIRE23_FAST','-append'); %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
end
        %%% MIXTURE POUR LA FUSION 2+3 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        load([ddRes filesep uu(1).name],'INVENTAIRE23');                        % 
        for stg = 2 % STADE FUSION 2+3                                          % 
        fmin_max = [0.25 4.5];                                                  %  
        INV = [];                                                               % 
        for ele = 1:numel(O.DBase.channels)                                     % 
            INV = cat(1,INV,INVENTAIRE23{ele}{stg}(:,[1 7]));                   % 
        end                                                                     % 
        index_suj = unique(INV(:,1));                                           % 
        for i = 1:numel(index_suj)                                              % 
            INV_i = INV(INV(:,1)==index_suj(i),:);                              % 
            [mix(i), f_sep(i)] = mixture_slow_fast(INV_i(:,2),fmin_max);        % 
        end                                                                     % 
        [mix0, f_sep0{stg}] = mixture_slow_fast(INV(:,2),fmin_max);             % 
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        % Figure:                                                      %
        figure('position',[10 100 800,400]); ax = gca;                 %
        set(ax,'Color','none');                                        %
        hold on                                                        %
        for i = 1:numel(index_suj)                                     %
            plot(mix(i).G1.fsamp,mix(i).G1.g1,'Color',[0.3 0.3 0.3]);  %
            plot(mix(i).G2.fsamp,mix(i).G2.g1,'Color',[0.7 0.7 0.7]);  %
        end                                                            %
        plot(mix0.G1.fsamp,mix0.G1.g1,'-r','linewidth',3);             %
        plot(mix0.G2.fsamp,mix0.G2.g1,'-b','linewidth',3);             %
        plot([f_sep0{stg} f_sep0{stg}],[0 0.7],'--k','linewidth',3);   %
        mess = sprintf('%3.2f Hz',f_sep0{stg});                        %
        text(f_sep0{stg},0.95,mess,...                                 %
                          'HorizontalAlignment','center',...           %
                          'fontsize',16);                              %
        mess = sprintf('%3.2f\n[%3.2f]', mean(f_sep),std(f_sep));      %
        text(f_sep0{stg},0.85,mess,...                                 %
                          'HorizontalAlignment','center',...           %
                          'fontsize',14);                              %
        % les donnees: %%%%%%%%%%%%%%%%%%%%                            %
        [a,b] = hist(INV(:,2),60);        %                            %
        a  = a /sum(a) /(b(2)-b(1));      %                            %
        ds = b(1):0.01:b(end);            %                            %
        d  = spline(b,a,ds);              %                            %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%                            %
        plot(ds,d,'-','Color',[0.5 0.5 0.5],'linewidth',2);            %
        xlabel('transition frequency (Hz)','fontsize',14);             %
        title('NREM 2+3','fontsize',14);                               %
        hold off                                                       %
        %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
        end

fprintf('\n\n -- Fin. Bye.\n\n')
end